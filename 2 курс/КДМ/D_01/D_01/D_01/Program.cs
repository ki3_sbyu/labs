﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace D_01
{
    class Program
    {
        static void Main(string[] args)
        {
            int firstElems, secondElems;
            bool check;
            do
            {
                WriteLine("Введите к-ство елементов");
                do
                {
                    WriteLine("в первом множестве =>");
                    check = int.TryParse(ReadLine(), out firstElems);
                } while (!check || firstElems <= 0);
                do
                {
                    WriteLine("во втором множестве =>");
                    check = int.TryParse(ReadLine(), out secondElems);
                } while (!check || secondElems <= 0);
                int[] firstArea = new int[firstElems],
                    secondArea = new int[secondElems],
                    resultArea = new int[firstElems + secondElems];
                firstArea = ReadElements(firstArea);
                secondArea = ReadElements(secondArea);
                do
                {
                    check = true;
                    WriteLine("Выбор операции:\n1.Сложение\n2.Вычитание\n3.Сечение\n4.Симметрическое вычитание");
                    switch (ReadLine())
                    {
                        case "1":
                            resultArea = SummAreas(firstArea, secondArea);
                            break;
                        case "2":
                            resultArea = SubAreas(firstArea, secondArea);
                            break;
                        case "3":
                            resultArea = SecArea(firstArea, secondArea);
                            break;
                        case "4":
                            resultArea = SimmetricSecArea(firstArea, secondArea);
                            break;
                        default:
                            WriteLine("Ошибка ввода!");
                            check = false;
                            break;
                    }
                } while (!check);
                PrintArray(resultArea);
                WriteLine("Выход из программы => 0");
            } while (ReadLine() != "0");
           
        }

        static int[] SummAreas(int[] firstArea, int[] secondArea)
        {
            int[] results = new int[firstArea.Length + secondArea.Length];
            results = Clear(results);
            int k = 0; bool check = false;
            for (k = 0; k < firstArea.Length; k++)
            {
                results[k] = firstArea[k];
            }
            for (int i = 0; i < secondArea.Length; i++, k++)
            {
                check = true;
                for (int j = 0; j < firstArea.Length; j++)
                {
                    if (check = secondArea[i] == results[j])
                    {
                        k--;
                        break;
                    }
                }
                if (!check)
                    results[k] = secondArea[i];
            }
            CutArray(ref results);
            return results;
        }

        static int[] SubAreas (int[] firstArea, int[] secondArea)
        {
            int[] results = new int[firstArea.Length + secondArea.Length];
            results = Clear(results);
            bool check = false;
            for(int i=0, k=0;i<firstArea.Length;i++, k++)
            {
                check = false;
                for(int j=0;j<secondArea.Length;j++)
                {
                    if(check = firstArea[i] == secondArea[j])
                    {
                        k--;
                        break;
                    }
                }
                if(!check)
                {
                    results[k] = firstArea[i];
                }
            }
            CutArray(ref results);
            return results;
        }

        static int[] SecArea (int[] firstArea, int[] secondArea)
        {
            int[] results = new int[firstArea.Length + secondArea.Length];
            results = Clear(results);
            for (int i = 0, k = 0; i < firstArea.Length; i++)
            {
                for (int j = 0; j < secondArea.Length; j++)
                {
                    if (firstArea[i] == secondArea[j])
                    {
                        results[k] = firstArea[i];
                        k++;
                        break;
                    }
                }
            }
            CutArray(ref results);
            return results;
        }

        static int[] SimmetricSecArea(int[] firstArea, int[] secondArea)
        {
            int[] results = new int[firstArea.Length + secondArea.Length];
            results = Clear(results);
            bool check; int k = 0;
            for (int i = 0; i < firstArea.Length; i++, k++)
            {
                check = false;
                for (int j = 0; j < secondArea.Length; j++)
                {
                    if (check = firstArea[i] == secondArea[j])
                    {
                        k--;
                        break;
                    }
                }
                if (!check)
                    results[k] = firstArea[i];
            }
            for (int i = 0; i < secondArea.Length; i++, k++)
            {
                check = false;
                for (int j = 0; j < firstArea.Length; j++)
                {
                    if (check = secondArea[i] == firstArea[j])
                    {
                        k--;
                        break;
                    }
                }
                if (!check)
                    results[k] = secondArea[i];
            }
            CutArray(ref results);
            return results;
        }

        static int[] Clear(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = int.MinValue;
            }
            return array;
        }

        static int[] ReadElements(int[] array)
        {
            bool check;
            WriteLine("Начните ввод значений:");
            for (int i = 0; i < array.Length; i++)
            {
                do
                {
                    Write($"{i} элемент == >");
                    check = int.TryParse(ReadLine(), out array[i]);
                } while (!check);
            }
            return array;
        }

        static void CutArray(ref int[] array)
        {
            int elems = 0;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] != int.MinValue)
                    elems++;
            }
            int[] secondArr = new int[elems];
            for (int i = 0; i < secondArr.Length; i++)
            {
                secondArr[i] = array[i];
            }
            array = secondArr;
        }

        static void PrintArray(int[] array)
        {
            Write("Результат = { ");
            foreach (int i in array)
            {
                Write($"{i} ");
            }
            Write("}\n");
        }
    }
}
