﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Console;

namespace D_04
{
    class Program
    {
        private const int e_size = 3;
        static void Main(string[] args)
        {
            OutputEncoding = Encoding.UTF8;
            InputEncoding = Encoding.UTF8;
            int tree_weight = 0, tmp;
            int v, e;
            Write("Кількість вершин:"); v = Convert.ToInt32(ReadLine());
            Write("Кількість ребер:"); e = Convert.ToInt32(ReadLine());
            List<int[]> SpanTree = new List<int[]>();
            int[] Comp = new int[v];
            for (int i = 0; i < v; i++)
            {
                Comp[i] = i + 1;
            }
            int[,] gr = ReadGraph(e);
            Sort(ref gr);
            for (int i = 0; i < e; i++)
            {
                if (Comp[gr[i, 0] - 1] != Comp[gr[i, 1] - 1])
                {
                    int[] arr = { gr[i, 0], gr[i, 1], gr[i, 2] };
                    SpanTree.Add(arr);
                    tree_weight += gr[i, 2];
                    tmp = Comp[gr[i, 1] - 1];
                    for (int j = 0; j < v; j++)
                    {
                        if (Comp[j] == tmp)
                        {
                            Comp[j] = Comp[gr[i, 0] - 1];
                        }
                    }
                }
            }
            foreach (int[] item in SpanTree)
            {
                foreach (int i in item)
                {
                    Write(i + " ");
                }
                WriteLine();
            }
            WriteLine("Вага остового дерева: " + tree_weight);
        }

        static void Sort(ref int[,] arr)
        {
            for (int j = 0; j < 17; j++)
            {
                for (int i = 0; i < 17; i++)
                {
                    if (arr[i, 2] > arr[i + 1, 2])
                    {
                        for(int m=0; m < e_size; m++)
                        {
                            int temp = arr[i, m];
                            arr[i, m] = arr[i + 1, m];
                            arr[i + 1, m] = temp;
                        }
                    }
                }
            }
        }

        static int [,] ReadGraph(int e)
        {
            int[,] graph = new int[e, e_size];
            for(int i=0; i<graph.GetLength(0); i++)
            {
                WriteLine($"Введіть дані ребра {i+1}:");
                string temp;
                for(int j=0; j < graph.GetLength(1); j++)
                {
                    switch(j)
                    {
                        case 0:
                        case 1: Write($"Вершина {j+1}:"); break;
                        case 2: Write($"Вага ребра:"); break;
                    }
                    temp = ReadLine();
                    graph[i, j] = Convert.ToInt32(temp);
                }
            }
            return graph;
        }
    }
}