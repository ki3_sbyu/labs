﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;
using static BinaryMatrix.Utils;

namespace BinaryMatrix
{
    public static class Utils
    {

        public static void Pause()
        {
            WriteLine("Нажмите любую кнопку для выхода...");
            ReadKey(true);
        }
        public static int[] SetInput(string stop)
        {
            int item;
            bool valid;
            string buffer;
            List<int> set = new List<int>();
            #region MESSAGES
            Write(stop);
            WriteLine(" подтвердить текущие элементы");
            Write("[enter]");
            WriteLine(" добавить элемент");
            #endregion
            do
            {
                buffer = ReadLine();
                if (string.Compare(buffer, stop) == 0)
                    break;
                valid = int.TryParse(buffer, out item);
                if (!valid)
                {
                    WriteLine("Ошибка при считывании элемента, ввод будет проигнорирован.");
                }
                else
                    set.Add(item);
            } while (true);
            return set.ToArray();
        }
        public static void WriteSet(int[] set, string name)
        {
            Write(name + " = {" + $"{set[0]}");
            for (int i = 1; i < set.Length; i++)
                Write($", {set[i]}");
            WriteLine("}");
        }

        public delegate bool Condition(int item1, int item2);
        public static void WriteMatrix(bool[,] binaryMatrix)
        {
            WriteLine("");
            for (int i = 0; i < binaryMatrix.GetLength(0); i++)
            {
                Write(binaryMatrix[i, 0] == true ? "1" : "0");
                for (int j = 1; j < binaryMatrix.GetLength(1); j++)
                    Write(binaryMatrix[i, j] == true ? " 1" : " 0");
                WriteLine("");
            }
        }
        public static bool[,] GetBinaryMatrix(int[] set1, int[] set2, Condition rule)
        {
            int cols = set1.Length;
            int rows = set2.Length;
            bool[,] matrix = new bool[rows, cols];
            for (int a = 0; a < cols; a++)
                for (int b = 0; b < rows; b++)
                {
                    if (rule(set1[a], set2[b]) == true)
                        matrix[b, a] = true;
                    else
                        matrix[b, a] = false;
                }
            return matrix;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            const string STOP_SIGN = "#stop";
            int[] set1 = SetInput(STOP_SIGN); Clear();
            int[] set2 = SetInput(STOP_SIGN); Clear();
            Clear();
            WriteSet(set1, "A");
            WriteSet(set2, "B");
            Condition rule = Check;
            bool[,] bin = GetBinaryMatrix(set1, set2, rule);
            Write("p = {(a,b):aєA & bєB & |a-b| > 1}");
            WriteMatrix(bin);
            Pause();
        }
        static bool Check(int a, int b)
        {
            if (a + b + 1 != 0 && Math.Abs(a - b) > 1)
                return true;
            else
                return false;
        }
    }
}
