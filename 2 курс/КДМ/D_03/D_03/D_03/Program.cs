﻿using System;
using static System.Console;
using System.Collections.Generic;
namespace D_03
{
    class Program
    {
        static void Main(string[] args)
        {
            Int16 menu, n, m;
            string str;
            do
            {
                PrintMenu();
                menu = Convert.ToInt16(ReadLine());
                switch (menu)
                {
                    case 1:
                        Write("\nПерестановка без повторений\nВведите количество объектов(n):");
                        n = Convert.ToInt16(ReadLine());
                        Write("Количество возможных способов: " + Fact(n) + "\n\n");
                        break;
                    case 2:
                        Write("\nПерестановка c повторением\nВведите слово для обработки:");
                        str = ReadLine();
                        Write("Количество возможных способов: " + Word(str) + "\n\n");
                        break;
                    case 3:
                        Write("\nРазмещение без повторений\nВведите общее количество объектов(n):");
                        n = Convert.ToInt16(ReadLine());
                        Write("Введите количество объектов для размещения(m):");
                        m = Convert.ToInt16(ReadLine());
                        Write("Количество возможных способов: " + Fact(n) / Fact(n - m) + "\n\n");
                        break;
                    case 4:
                        Write("\nРазмещение c повторением\nВведите общее количество объектов(n):");
                        n = Convert.ToInt16(ReadLine());
                        Write("Введите количество объектов для размещения(m):");
                        m = Convert.ToInt16(ReadLine());
                        Write("Количество возможных способов: " + Math.Pow(n, m * 1.0) + "\n\n");
                        break;
                    case 5:
                        Write("\nСочетание без повторений\nВведите общее количество объектов(n):");
                        n = Convert.ToInt16(ReadLine());
                        Write("Введите количество объектов для размещения(m):");
                        m = Convert.ToInt16(ReadLine());
                        Write("Количество возможных способов: " + Fact(n) / (Fact(n - m) * Fact(m)) + "\n\n");
                        break;
                    case 6:
                        Write("\nСочетание с повторением\nВведите общее количество объектов(n):");
                        n = Convert.ToInt16(ReadLine());
                        Write("Введите количество объектов для размещения(m):");
                        m = Convert.ToInt16(ReadLine());
                        Write("Количество возможных способов: " + Fact(n + m - 1) / (Fact(m) * Fact(n - 1)) + "\n\n");
                        break;
                    case 7:
                        Write("Введите количество элементов (#stop для остановки):");
                        int k;
                        k = Convert.ToInt16(ReadLine()); k = Fact(k);
                        string temp = "";
                        do
                        {
                            Write("Введите количество элементов для перестановки:");
                            temp = ReadLine();
                            if (temp == "#stop") break;
                            int temp_int = Convert.ToInt32(temp);
                            
                            k /= Fact(temp_int);
                        } while (temp != "#stop");
                        WriteLine("Количество возможных способов: " + k + "\n\n");
                        break;
                    case 0:
                        break;
                    default:
                        break;
                }
            } while (menu != 0);
        }

        static void PrintMenu()
        {
            Write("\tМеню\n" +
                "1) Перестановка без повторений\n" +
                "2) Перестановка с повторением\n" +
                "3) Размещение без повторений\n" +
                "4) Размещение с повторением\n" +
                "5) Сочетание без повторений\n" +
                "6) Сочетание с повторением\n" +
                "7) Перестановка с условием\n"+
                "0) Выход\n-> ");
        }

        static int Fact(int n)
        {
            if (n == 1) return n;
            return n * Fact(n - 1);
        }

        static int Word(string s)
        {
            List<char> chars = new List<char>();
            for (int i = 0; i < s.Length; i++)
            {
                chars.Add(s[i]);
            }
            int count = 0, res = 1, k = s.Length;
            for (int i = 0; i < k; i++)
            {
                for (int j = 0; j < k; j++)
                {
                    if (j == 0) count++;
                    if (j != 0)
                    {
                        if (chars[j] == chars[i] && i != j)
                        {
                            chars.RemoveAt(j);
                            k--;
                            count++;
                        }
                    }
                }
                res *= Fact(count);
                count = 0;
            }
            return Fact(s.Length) / res;
        }
    }
}

