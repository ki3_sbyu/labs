'use strict';
const express = require('express');
const bodyParser = require('body-parser');
const multer = require('multer');
const cookieParser = require('cookie-parser');
const session = require('cookie-session');
const fs = require('fs');
const app = express();
const upload = {
    images: multer({ dest: 'upload/images' }),
}
const defaultLang = 'ukr';

//������������ JSON
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
//session
app.use(cookieParser('somesecretkey'));
app.use(session({ secret: 'somesecretkey123' }));
//���������� ������ PUG
app.set('view engine', 'pug');
//���������� ���������� �������
app.use(express.static('upload'));
app.use(express.static('content'));

app.get("/index", function (req, res) {
    if (!req.session.data) {
        req.session.data = {};
    }
    res.render('index', { lang: req.session.keywords, data: req.session.data });
});

app.post('/form', upload.images.single('photo'), function (req, res) {
    req.session.data = req.body;
    let file = req.file;
    req.session.photo = file;
    if (file) {
        req.body.photo = `/images/${file.filename}`;
    }
    req.body.about = nl2br(req.body.about, false);
    res.render('form', { lang: req.session.keywords, data: req.body });
});

app.use(function (req, res) {
    if (!req.session.lang)
        req.session.lang = defaultLang;
    if (req.query.lang) {
        req.session.lang = req.query.lang;
    }
    let selectedLanguageJSON =
        fs.readFileSync(`${__dirname}/lang/${req.session.lang}.json`, 'utf8');
    selectedLanguageJSON = selectedLanguageJSON.trim();
    req.session.keywords = JSON.parse(selectedLanguageJSON);
    res.redirect(302, '/index');
});

app.listen(1337);

function nl2br(str, is_xhtml) {
    if (typeof str === 'undefined' || str === null) {
        return '';
    }
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}