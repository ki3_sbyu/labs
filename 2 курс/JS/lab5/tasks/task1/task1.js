let inputs = document.querySelectorAll('input');

function convertFromFarenheitToCelsius() {
  inputs[0].value = 5 / 9 * (+inputs[1].value - 32);
}

function convertFromCelsiusToFarenheit(value) {
  inputs[1].value = 9 / 5 * +inputs[0].value + 32;
}

inputs[0].addEventListener('keyup', convertFromCelsiusToFarenheit);
inputs[1].addEventListener('keyup', convertFromFarenheitToCelsius);
