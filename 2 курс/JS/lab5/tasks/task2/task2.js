let totalQuestions = 0;
let correctAnswers = 0;

let resultLabel = document.querySelector('.result');
let nextTask = document.querySelector('.next');
let setTask = document.querySelector('.question');
let answer = document.querySelector('.answer');
let check = document.querySelector('.check');
let info = document.querySelector('.info');
let isPressed = false;

function getRandomInt(min, max)
{
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

let firstOperand = 0;
let secondOperand = 0;

function setInfo() {
  info.innerText = "Загальний рахунок " +
      Math.round(+correctAnswers / (totalQuestions == 0? 1: totalQuestions) * 100) +
      '%' + ' ('+correctAnswers + " правильних відповідей з " + totalQuestions + ')';
}

function setQuestion() {
  generateOperands();
  setTask.innerText = firstOperand + ' × ' + secondOperand;
}

function generateOperands() {
  firstOperand = getRandomInt(0, 10);
  secondOperand = getRandomInt(0, 20);
}

setQuestion();
setInfo();
totalQuestions++;

check.addEventListener('click', function() {
  if(!isPressed) {
    let result = firstOperand * secondOperand;
    if(result == +answer.value) {
      correctAnswers++;
      resultLabel.innerText = 'Правильно!';
    }
    else {
      resultLabel.innerText = "Неправильно, правильна відповідь: " + result;
    }
    isPressed = true;
  }
  else {
    resultLabel.innerText = "Ви вже дали відповідь на це питання!";
  }
});

nextTask.addEventListener('click', function() {
  setQuestion();
  setInfo();
  totalQuestions++;
  isPressed = false;
  resultLabel. innerText = "";
  answer.value = "";
});
