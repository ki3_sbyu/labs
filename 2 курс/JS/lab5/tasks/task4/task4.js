let imgArr = [
  {
    path: 'images/aperture.jpg',
    title: 'Aperture Laboratories',
    description: 'Aperture Inc. Logo'
  },
  {
    path: 'images/blackmesa.png',
    title: 'Black Mesa',
    description: 'Research facility'
  },
  {
    path: 'images/citadel.jpg',
    title: 'The Citadel',
    description: 'The Citadel is the headquarters from which the Combine govern the Earth'
  }
];

function initPhotoRotator(id, imagesArray) {
  let block = document.getElementById(id);
  let divBack = document.createElement('div');
  let divNext = document.createElement('div');
  let hrefBack = document.createElement('a');
  let hrefNext = document.createElement('a');
  let labelCurrentPhoto = document.createElement('label');
  let labelTitle = document.createElement('strong');
  let labelDescription = document.createElement('label');
  let container = document.createElement('div');
  let divImage = document.createElement('div');
  let image = document.createElement('img');
  let divHeader = document.createElement('div');
  let divFooter = document.createElement('div');

  block.style.display = "flex";
  divBack.style.display = "flex";
  divBack.style.alignItems = "center";
  divNext.style.display = "flex";
  divNext.style.alignItems = "center";
  labelTitle.style.display = "block";
  divHeader.style.textAlign = "center";
  divFooter.style.textAlign = "center";
  divBack.style.border = "1px solid lightgrey";
  divNext.style.border = "1px solid lightgrey";
  divImage.style.border = "1px solid lightgrey";
  divFooter.style.border = "1px solid lightgrey";
  divHeader.style.border = "1px solid lightgrey";
  divBack.style.padding = "5px";
  divNext.style.padding = "5px";
  divImage.style.padding = "5px";
  divFooter.style.padding = "5px";
  divHeader.style.padding = "5px";
  divImage.style.width = "700px";
  divImage.style.height = '300px';
  divImage.style.display = "grid";
  image.style.maxWidth = "700px";
  image.style.maxHeight = "300px";
  divImage.style.alignItems = "center";
  divImage.style.justifyContent = "center";

  hrefBack.innerText = "Назад";
  hrefNext.innerText = "Вперед";
  hrefBack.setAttribute('href', '#');
  hrefNext.setAttribute('href', '#');
  divBack.appendChild(hrefBack);
  divHeader.appendChild(labelCurrentPhoto);
  divImage.appendChild(image);
  divFooter.appendChild(labelTitle);
  divFooter.appendChild(labelDescription);
  container.appendChild(divHeader);
  container.appendChild(divImage);
  container.appendChild(divFooter);
  divNext.appendChild(hrefNext);
  block.appendChild(divBack);
  block.appendChild(container);
  block.appendChild(divNext);
  
  labelCurrentPhoto.innerText = "Фотографія " + 1 + ' з ' + imagesArray.length;
  labelTitle.innerText = imagesArray[0].title;
  labelDescription.innerText = imagesArray[0].description;
  image.setAttribute('src', imagesArray[0].path);
  image.value = 0;

  function setText(index) {
    labelTitle.innerText = imagesArray[index].title;
    labelDescription.innerText = imagesArray[index].description;
    image.setAttribute('src', imagesArray[index].path);
  };

  hrefBack.addEventListener('click', function() {
    let index = image.value == 0? imagesArray.length - 1: +image.value - 1;
    image.value = index;
    labelCurrentPhoto.innerText = "Фотографія " + (index + 1) + ' з ' + imagesArray.length;
    setText(index);
  });

  hrefNext.addEventListener('click', function() {
    let index = image.value == imagesArray.length - 1? 0: +image.value + 1;
    image.value = index;
    labelCurrentPhoto.innerText = "Фотографія " + (index + 1) + ' з ' + imagesArray.length;
    setText(index);
  });
}

initPhotoRotator('rotator', imgArr);
