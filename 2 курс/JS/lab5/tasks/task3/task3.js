let headerLabel = document.createElement('label');
let nextTaskButton = document.createElement('button');
let questionLabel = document.createElement('label');
let radioButtons = [];
let radioLabels = [];
let resultLabel = document.createElement('label');
let answer = document.createTextNode('Hi');
let div = document.createElement('div');
let answers = [];
let firstOperand, secondOperand;
let isPressed = false;
let correctAnswers = 0;
let totalQuestions = 0;

function setInfo() {
  headerLabel.innerText = "Загальний рахунок " +
      Math.round(+correctAnswers / (totalQuestions == 0? 1: totalQuestions) * 100) +
      '%' + ' ('+correctAnswers + " правильних відповідей з " + totalQuestions + ')';
}

function setElements() {
  document.body.appendChild(headerLabel);
  nextTaskButton.innerText = "Наступне завдання!";
  document.body.appendChild(nextTaskButton);
  document.body.appendChild(questionLabel);
  setRadioButtons();
  document.body.appendChild(resultLabel);
}

function setRadioButtons() {
  for(let i = 0; i < 4; i++) {
    radioButtons.push(document.createElement('input'));
    radioButtons[i].setAttribute('type', 'radio');
    radioButtons[i].setAttribute('name', 'answer');
    radioLabels.push(document.createElement('label'));
    radioLabels[i].appendChild(radioButtons[i]);
    answers.push(answer.cloneNode(false));
    answers[i].data = "Hi" + i;
    radioLabels[i].appendChild(answers[i]);
    div.appendChild(radioLabels[i]);
  }
  document.body.appendChild(div);
}

function removeRadio() {
  document.body.removeChild(div);
}

function getRandomInt(min, max)
{
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function generateOperands() {
  firstOperand = getRandomInt(0, 10);
  secondOperand = getRandomInt(0, 20);
}

function setQuestion() {
  setInfo();
  let radio = document.querySelector('input[name="answer"]:checked');
  if(radio != undefined) radio.removeAttribute('checked');
  generateOperands();
  questionLabel.innerText = firstOperand + ' × ' + secondOperand;
  generateAnswers();
  resultLabel.innerText = "";
}

function generateAnswers() {
  let result = firstOperand * secondOperand;
  let temp;
  let position = getRandomInt(0, 3);
  isPressed = false;
  for(let i = 0; i < 4; i++) {
    temp = getRandomInt(0, 10) * getRandomInt(0, 20);
    if(i != position) {
      if(result == temp) {
        i--;
        continue;
      }
      answers[i].data = temp;
      radioButtons[i].value = temp;
    }
    else {
      answers[i].data = result;
      radioButtons[i].value = result;
    }
    radioButtons[i].checked = false;
  }
  totalQuestions++;
}

function checkAnswer() {
  let result = firstOperand * secondOperand;
  let value = document.querySelector('input[name="answer"]:checked').value;
  if(!isPressed) {
    if(+value == result) {
      resultLabel.innerText = "Правильно!";
      correctAnswers++;
    }
    else {
      resultLabel.innerText = "Неправильно, правильна відповідь: " + result;
    }
  }
  else {
    resultLabel.innerText = "Ви вже відповіли на це питання!";
  }
  isPressed = true;
}

nextTaskButton.addEventListener('click', function() {
  setQuestion();
});

setElements();
setQuestion();
for(let i = 0; i < 4; i++) {
  radioButtons[i].addEventListener('click', checkAnswer);
}
