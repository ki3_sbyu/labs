﻿const infoArr = [
  {
    num: 0,
    map: [
      [1, 1, 1, 1],
      [1, 0, 0, 1],
      [1, 0, 0, 1],
      [1, 0, 0, 1],
      [1, 1, 1, 1]
    ]
  },
  {
    num: 1,
    map: [
      [0, 0, 0, 1],
      [0, 0, 1, 1],
      [0, 0, 0, 1],
      [0, 0, 0, 1],
      [0, 0, 0, 1]
    ]
  },
  {
    num: 2,
    map: [
      [0, 1, 1, 0],
      [1, 0, 0, 1],
      [0, 0, 1, 0],
      [0, 1, 0, 0],
      [1, 1, 1, 1]
    ]
  },
  {
    num: 3,
    map: [
      [1, 1, 1, 1],
      [0, 0, 0, 1],
      [0, 1, 1, 0],
      [0, 0, 0, 1],
      [1, 1, 1, 1]
    ]
  },
  {
    num: 4,
    map: [
      [1, 0, 0, 1],
      [1, 0, 0, 1],
      [0, 1, 1, 1],
      [0, 0, 0, 1],
      [0, 0, 0, 1]
    ]
  },
  {
    num: 5,
    map: [
      [1, 1, 1, 1],
      [1, 0, 0, 0],
      [1, 1, 1, 0],
      [0, 0, 0, 1],
      [1, 1, 1, 0]
    ]
  },
  {
    num: 6,
    map: [
      [0, 1, 1, 1],
      [1, 0, 0, 0],
      [1, 1, 1, 1],
      [1, 0, 0, 1],
      [0, 1, 1, 1]
    ]
  },
  {
    num: 7,
    map: [
      [1, 1, 1, 1],
      [1, 0, 0, 1],
      [0, 0, 1, 0],
      [0, 1, 0, 0],
      [0, 1, 0, 0]
    ]
  },
  {
    num: 8,
    map: [
      [0, 1, 1, 0],
      [1, 0, 0, 1],
      [0, 1, 1, 0],
      [1, 0, 0, 1],
      [0, 1, 1, 0]
    ]
  },
  {
    num: 9,
    map: [
      [0, 1, 1, 0],
      [1, 0, 0, 1],
      [0, 1, 1, 1],
      [0, 0, 0, 1],
      [1, 1, 1, 0]
    ]
  },
];

let div = document.querySelector('div');
let input = document.querySelector('input');
let button = document.querySelector('button');
let result = getRandomInt(1000, 9999);

generateCapcha(result, div);
button.innerText = "Перевірити!";

function getRandomInt(min, max)
{
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function generateCapcha(number, block) {
  let str = number + '';
  let generatedArray = [];
  let temp = [];
  for(let i = 0; i < 5; i++) {
    for(let j = 0; j < str.length; j++) {
      temp = temp.concat(infoArr[+str[j]].map[i], 0);
    }
    generatedArray.push(temp);
    temp = [];
  }
  for(let i = 0; i < generatedArray.length; i++) {
    let tempBlock = document.createElement('div');
    for(let j = 0; j < generatedArray[0].length; j++) {
      let span = document.createElement('span');
      span.style.height = "5px";
      span.style.width = "5px";
      span.style.display = "block";
      if(generatedArray[i][j] == 1) {
        span.style.backgroundColor = "red";
      }
      tempBlock.appendChild(span);
    }
    tempBlock.style.display = "flex";
    block.appendChild(tempBlock);
  }
}

button.addEventListener('click', function() {
  let answer = +input.value;
  if(answer == result) alert('Вірно!');
  else alert('Невірно!');
});
