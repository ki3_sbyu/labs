const person = {
  masha: { name: "Маша", age: 18 },
  vasya: { name: "Вася", age: 23 },
  vovochka: { name: "Вовочка", age: 6 }
};
const examples = {
  className: "name my namett is namehh helloWorld red",
  cssProperty: "-webkit-transition",
  intArray: [1, 2, 3, 5, -2, 2, 10, -100],
  strArray: ["HTML", "CSS", "JavaScript"],
  people: [person.vasya, person.masha, person.vovochka],
  strings: ["C++", "C#", "C++", "C#", "C", "C++", "JavaScript", "C++", "JavaScript"]
};

var list = { value: 1 };
list.next = { value: 2 };
list.next.next = { value: 3 };
list.next.next.next = { value: 4 };
list.next.next.next.next = { value: 5 };
list.next.next.next.next.next = { value: 6 };

let obj = {className: "hello world"};

console.log("К классу " + obj.className + " добавить new");
addClass(obj, "new");
console.log("Результат: " + obj.className);
console.log("Из " + examples.cssProperty + " сделано " + camelize(examples.cssProperty));
console.log("Из " + examples.className + " удалить все name");
removeClass(examples, "name");
console.log("Результат: " + examples.className);
console.log("Из " + examples.intArray.join(' ') + " взять только те которые находятся в диапазоне [-3; 3]");
filterRangeInPlace(examples.intArray, -3, 3);
console.log("Результат: " + examples.intArray.join(' '));
console.log("Массив [" + examples.intArray.join(', ') + "] отсортировать по убыванию");
reverseSort(examples.intArray);
console.log("Результат: [" + examples.intArray.join(', ') + "]");
console.log(examples.strArray.join(", ") + " сортирован в " + arraySort(examples.strArray).join(', '));
for(let i = 0; i < 5; i++) {
  console.log("Случайная сортировка массива: " + examples.intArray.sort(randomize).join(', '));
}
console.log("Сортировка обьектов по полю age:");
console.log([person.vasya, person.masha, person.vovochka]);
sortObjectsByAge(examples.people);
console.log(examples.people);
console.log("Односвязный список:");
console.log("Рекурсия, сначала");
printListRec(list);
console.log("Цикл, сначала");
printList(list);
console.log("Рекурсия, реверс");
printReverseListRec(list);
console.log("Цикл, реверс");
printReverseList(list);
console.log("Оставить элементы из массива [" + examples.strings.join(', ') + "] в одном экземпляре:");
console.log(unique(examples.strings));
