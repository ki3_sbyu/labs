function unique(arr) {
  let array = arr.slice();
  for(let i = 0; i < array.length; i++) {
    let j = array.lastIndexOf(array[i]);
    if(j != i && j >= 0) {
      array.splice(j, 1);
      i--;
    }
  }
  return array;
}

function printReverseList(list) {
  let temp = list;
  let arr = [];

  while(temp != undefined) {
    arr.push(temp.value);
    temp = temp.next;
  }
  for(let i = arr.length - 1; i >= 0; i--) {
    console.log(arr[i]);
  }
}

function printReverseListRec(list) {
  if(list.next != undefined)
    printReverseListRec(list.next);
  console.log(list.value);
}

function printListRec(obj) {
  console.log(obj.value);
  if(obj.next != undefined) {
    printListRec(obj.next);
  }
}

function printList(list) {
  let temp = list;
  while (temp != undefined) {
    console.log(temp.value);
    temp = temp.next;
  }
}

//sort functions
function sortObjectsByAge(arr) {
  return arr.sort(compareAges);
}

function arraySort(arr) {
  return arr.sort();
}

function reverseSort(arr) {
  return arr.sort().reverse();
}

//compares
function randomize() {
  return Math.floor(Math.random() * 3 ) - 1;
}

function compareAges(a, b) {
  if(a.age < b.age) return -1;
  if(a.age > b.age) return 1;
  return 0;
}

//other functions
function camelize(str) {
  let symbol;
  for(let i = 0; str.indexOf('-') != -1; i++) {
    symbol = str[str.indexOf('-') + 1].toUpperCase();
    str = str.substring(0, str.indexOf('-')) + symbol +
     str.substring(str.indexOf('-') + 2, str.length);
  }
  return str;
}

function addClass (object, className) {
  let array = object.className.split(' ');
  if(array.indexOf(className) < 0) {
    array.push(className);
    object.className = array.join(' ').trim();
  }
}

function removeClass(object, className) {
  let array = object.className.split(' ');
  for(let i = 0; i < array.length; i++) {
    if(array[i] === className) {
      array.splice(i, 1);
      i--;
    }
  }
  object.className = array.join(' ');
}

function filterRangeInPlace(arr, a, b) {
  for(let i=0; i < arr.length; i++) {
    if(arr[i] < a || arr[i] > b) {
      arr.splice(i, 1);
      i--;
    }
  }
}
