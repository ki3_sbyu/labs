/*jshint esversion: 6*/
let fps = 15;
let cellsInWidth = 10;
let reserveCellsCount = 4;
let cellsInHeight = 20 + reserveCellsCount;
let block = document.createElement('div');
let canvas = document.createElement('canvas');
let ctx = canvas.getContext('2d');
let strColor = 'white';
let strWidth = 2;
let cellSize = 30;
let canvasWidth = cellSize * cellsInWidth * 2;
let canvasHeight = cellSize * 22;
let gameFieldPadding = 5;
let cellColor = 'white';
let gameMatrix = [];
let up = 1;
let left = 2;
let right = 3;
let down = 4;
let logicTimer;
let drawTimer;
/*fill game matrix by zeros*/
for (let i = 0; i < cellsInHeight; i++) {
  gameMatrix.push(Array(cellsInWidth));
  gameMatrix[i].fill(0);
}
