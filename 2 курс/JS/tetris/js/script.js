/*jshint esversion: 6*/
function __init__() {
  canvas.innerText = 'Ваш браузер не підтримує технологію Canvas';
  block.appendChild(canvas);
  document.body.appendChild(block);
  canvas.setAttribute('width', canvasWidth);
  canvas.setAttribute('height', (cellsInHeight - reserveCellsCount) * cellSize);
  document.addEventListener('keydown', control);
  info.nextFigure = figuresMaps.list[getRandomInt(figuresMaps.list.length)];
  drawTimer = setInterval(renderFrame, 1000 / fps);
  mainLoop = setInterval(main, 1000 / info.level);
  spawnNewFigure();
  info.gameCheck = 0;
}

function main() {
  if (isCanMove(down)) {
    moveFigure(down);
    info.gameCheck++;
  } else {
    if (info.gameCheck == 0) {
      clearInterval(mainLoop);
      alert('Game Over!');
    }

    info.score += 10 * info.level;
    if (info.dropped) {
      info.dropped = false;
      clearInterval(mainLoop);
      mainLoop = setInterval(main, 1000 / info.level);
    }

    for (let i = 0; i < gameMatrix.length; i++) {
      if (!~gameMatrix[i].indexOf(0)) {
        moveLinesFrom(i);
        info.cleans++;
        info.score += 100 * info.level;
        if (info.cleans % 50 == 0) {
          info.level++;
          clearInterval(mainLoop);
          mainLoop = setInterval(main, 1000 / info.level);
        }

        for (let j = 0; j < gameMatrix.length; j++) {
          for (let k = 0; k < gameMatrix[0].length; k++) {
            if (gameMatrix[j][k] != 0)
              gameMatrix[j][k].set(k, j);
          }
        }
      }
    }

    spawnNewFigure();
    info.nextFigure = figuresMaps.list[getRandomInt(figuresMaps.list.length)];
    info.gameCheck = 0;
  }
}

function spawnNewFigure() {
  let figure = info.nextFigure;
  currentFigure = [];
  currentFigure.alpha = figure;
  let color = '#' + getRandomInt(10) + getRandomInt(10) + getRandomInt(10);
  for (let i = reserveCellsCount - 2, k = 0; i < reserveCellsCount + 2; i++, k++) {
    for (let j = cellsInWidth / 2 - 2, m = 0; j < cellsInWidth / 2 + 2; j++, m++) {
      if (figuresMaps[figure][k][m] == 1) {
        gameMatrix[i][j] = createFigure(j, i, color, strColor);
        currentFigure.push(gameMatrix[i][j]);
      }
    }
  }
}

function renderFrame() {
  ctx.clearRect(0, 0, canvasWidth, canvasHeight);
  drawGameField();
  renderMatrix();
  drawInfo();
}

function createFigure(x, y, cellClr, bdrClr) {
  return new Square(x, y, cellClr, bdrClr);
}

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function moveLinesFrom(index) {
  gameMatrix.splice(index, 1);
  gameMatrix.unshift([]);
  for (let i = 0; i < cellsInWidth; i++) {
    gameMatrix[0].push(0);
  }
}

function moveFigure(vector) {
  for (let i = 0; i < currentFigure.length; i++) {
    let x = currentFigure[i].mX;
    let y = currentFigure[i].mY;
    switch (vector) {
      case down:
        currentFigure[i].set(x, y + 1);
        gameMatrix[y + 1][x] = currentFigure[i];
        break;
      case left:
        currentFigure[i].set(x - 1, y);
        gameMatrix[y][x - 1] = currentFigure[i];
        break;
      case right:
        currentFigure[i].set(x + 1, y);
        gameMatrix[y][x + 1] = currentFigure[i];
        break;
    }
    if (!isCurrentFigure(x, y, i))
      gameMatrix[y][x] = 0;
  }
}

function isCurrentFigure(x, y, num) {
  for (let i = 0; i < 4; i++) {
    if (i != num) {
      let cx = currentFigure[i].mX;
      let cy = currentFigure[i].mY;
      if (cx == x && cy == y) {
        return true;
      }
    }
  }

  return false;
}

function isCanMove(vector) {
  for (let i = 0; i < 4; i++) {
    let cx = currentFigure[i].mX;
    let cy = currentFigure[i].mY;
    switch (vector) {
      case down:
        if (cy + 1 >= cellsInHeight ||
            (gameMatrix[cy + 1][cx] != 0 && !isCurrentFigure(cx, cy + 1, i))) {
          return false;
        }

        break;
      case left:
        if (cx - 1 < 0 ||
            (gameMatrix[cy][cx - 1] != 0 && !isCurrentFigure(cx - 1, cy, i))) {
          return false;
        }

        break;
      case right:
        if (cx + 1 >= cellsInWidth ||
          (gameMatrix[cy][cx + 1] != 0 && !isCurrentFigure(cx + 1, cy, i))) {
          return false;
        }

        break;
    }
  }

  return true;
}

function checkRotate() {
  if (currentFigure.alpha == 'q') return false;
  let a = [];
  for (let i = 0; i < figuresMaps[currentFigure.alpha].length; i++) {
    a.push(figuresMaps[currentFigure.alpha].slice());
  }

  for (let i = 0; i < a.length / 2; i++) {
    for (let j = 0; j < a.length / 2; j++) {
      let tmp = a[i][j];
      let n = a.length;
      a[i][j] = a[j][n - 1 - i];
      a[j][n - 1 - i] = a[n - 1 - i][n - 1 - j];
      a[n - 1 - i][n - 1 - j] = a[n - 1 - j][i];
      a[n - 1 - j][i]  = tmp;
    }

  }

  let x = currentFigure[0].mX; //
  let y = currentFigure[0].mY; //
  for (let i = y - 2, k = 0; k < a.length; i++, k++) {
    for (let j = x - 2, m = 0; m < a[0].length; j++, m++) {
      if (a[k][m] == 1 && gameMatrix[i][j] != 0 && !isCurrentFigure(j, i) ||
          i >= cellsInHeight || j >= cellsInWidth + 1 || i < reserveCellsCount - 2 ||
          j < 0 || i < reserveCellsCount) {
        return false;
      }
    }
  }

  return true;
}

function rotate() {

  let a = figuresMaps[currentFigure.alpha];
  for (let i = 0; i < a.length / 2; i++) {
    for (let j = 0; j < a.length / 2; j++) {
      let tmp = a[i][j];
      let n = a.length;
      a[i][j] = a[j][n - 1 - i];
      a[j][n - 1 - i] = a[n - 1 - i][n - 1 - j];
      a[n - 1 - i][n - 1 - j] = a[n - 1 - j][i];
      a[n - 1 - j][i]  = tmp;
    }
  }

  for (let i = 0; i < 4; i++) {
    let cx = currentFigure[i].mX;
    let cy = currentFigure[i].mY;
    gameMatrix[cy][cx] = 0;
  }

  let x = currentFigure[2].mX; //
  let y = currentFigure[2].mY; //
  let alpha = currentFigure.alpha;
  currentFigure = [];
  currentFigure.alpha = alpha;
  for (let i = y - 2, k = 0; k < a.length; i++, k++) {
    for (let j = x - 2, m = 0; m < a[0].length; j++, m++) {
      if (a[k][m] == 1) {
        gameMatrix[i][j] = createFigure(j, i, cellColor, strColor);
        currentFigure.push(gameMatrix[i][j]);
      }
    }
  }
}

function control(e) {
  switch (e.keyCode) {
    case 87: case 38: //up
      if (checkRotate())
        rotate();
      break;
    case 68: case 39: //right
      if (isCanMove(right)) {
        moveFigure(right);
      }

      break;
    case 65: case 37: //left
      if (isCanMove(left)) {
        moveFigure(left);
      }

      break;
    case 83: case 40: //down
      if (isCanMove(down)) {
        moveFigure(down);
      }

      break;
    case 32:
      clearInterval(mainLoop);
      mainLoop = setInterval(main, 1);
      info.dropped = true;
      break;
  }
}
