/*jshint esversion: 6*/
function drawGameField() {
  ctx.save();
  ctx.strokeStyle = strColor;
  ctx.lineWidth = strWidth;
  ctx.beginPath();
  ctx.moveTo(cellsInWidth * cellSize + strWidth / 2, 0);
  ctx.lineTo(cellsInWidth * cellSize + strWidth / 2,
      (cellsInHeight - reserveCellsCount) * cellSize);
  ctx.stroke();
  ctx.restore();
}

function drawInfo() {
  ctx.save();
  let marginLeft = cellsInWidth * cellSize + strWidth / 2 + 10;
  ctx.font = '30px consolas';
  ctx.fillStyle = 'white';
  ctx.textAlign = 'left';
  ctx.fillText('score: ' + info.score, marginLeft, 50);
  ctx.fillText('cleans: ' + info.cleans, marginLeft, 100);
  ctx.fillText('level: ' + info.level, marginLeft, 150);
  ctx.lineWidth = 3;
  ctx.strokeStyle = 'white';
  ctx.strokeRect(marginLeft + (canvasWidth - marginLeft) / 2 - 3 * cellSize,
    350, 5 * cellSize, 6 * cellSize);
  let alpha = info.nextFigure;
  for (let i = 0, y = 350 + cellSize; i < figuresMaps[alpha].length; i++, y += cellSize) {
    for (let j = 0, x = marginLeft + cellSize * 2; j < figuresMaps[alpha][0].length; j++, x += cellSize) {
      if (figuresMaps[alpha][i][j] == 1) {
        ctx.save();
        ctx.strokeStyle = 'white';
        ctx.fillStyle = 'grey';
        ctx.fillRect(x + 4, y + 4, cellSize - 8, cellSize - 8);
        ctx.strokeRect(x + 2, y + 2, cellSize - 4, cellSize - 4);
        ctx.restore();
      }
    }
  }

  ctx.restore();
}

function renderMatrix() {
  for (let i = 0; i < cellsInHeight; i++) {
    for (let j = 0; j < cellsInWidth; j++) {
      if (gameMatrix[i][j] != 0)
        gameMatrix[i][j].render();
    }
  }
}
