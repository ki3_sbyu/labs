/*jshint esversion: 6*/
function coord(x, y) {
  let crd = {};
  crd.x = x;
  crd.y = y;
  return crd;
}

function Square(x, y, cellClr, borderClr) {
  this.mX = x;
  this.mY = y;
  this.x = this.mX * cellSize;
  this.y = (this.mY - reserveCellsCount) * cellSize;
  this.render = function () {
    ctx.save();
    ctx.strokeStyle = borderClr;
    ctx.fillStyle = cellClr;
    ctx.fillRect(this.x + 4, this.y + 4, cellSize - 8, cellSize - 8);
    ctx.strokeRect(this.x + 2, this.y + 2, cellSize - 4, cellSize - 4);
    ctx.restore();
  };

  this.set = function (x, y) {
    this.mX = x;
    this.mY = y;
    this.x = x * cellSize;
    this.y = (y - reserveCellsCount) * cellSize;
  },

  this.move = function (vector) {
    switch (vector) {
      case up:
        this.mY--;
        this.y -= cellSize;
        break;
      case down:
        this.mY++;
        this.y += cellSize;
        break;
      case left:
        this.mX--;
        this.x -= cellSize;
        break;
      case right:
        this.mX++;
        this.x += cellSize;
        break;
    }
  };
}

var figuresMaps = {
  list: 'qtizsjl',
  q: [
      [0, 0, 0, 0],
      [0, 1, 1, 0],
      [0, 1, 1, 0],
      [0, 0, 0, 0],
     ],
  t: [
      [0, 0, 0, 0],
      [0, 1, 1, 1],
      [0, 0, 1, 0],
      [0, 0, 0, 0],
     ],
  i: [
      [0, 0, 1, 0],
      [0, 0, 1, 0],
      [0, 0, 1, 0],
      [0, 0, 1, 0],
    ],
  z: [
      [0, 0, 0, 0],
      [0, 1, 1, 0],
      [0, 0, 1, 1],
      [0, 0, 0, 0],
    ],
  s: [
      [0, 0, 0, 0],
      [0, 1, 1, 0],
      [1, 1, 0, 0],
      [0, 0, 0, 0],
    ],
  j: [
      [0, 0, 1, 0],
      [0, 0, 1, 0],
      [0, 1, 1, 0],
      [0, 0, 0, 0],
    ],
  l: [
      [0, 1, 0, 0],
      [0, 1, 0, 0],
      [0, 1, 1, 0],
      [0, 0, 0, 0],
    ],
};

var info = {
  dropped: false,
  score: 0,
  level: 1,
  currentFigure: '',
  nextFigure: '',
  control: '',
  cleans: 0,
};

var currentFigure = [];
