/*jshint esversion: 6*/

class Hamburger {
  constructor(size, stuff) {
    if (Object.is(size, Hamburger.SIZE_LARGE) || Object.is(size, Hamburger.SIZE_SMALL)) {
      this.size = size;
    } else {
      throw new Error('Incorrect size item!');
    }

    if (Object.is(stuff, Hamburger.STUFF_CHEESE) ||
        Object.is(stuff, Hamburger.STUFF_POTATO) ||
        Object.is(stuff, Hamburger.STUFF_SALAD)) {
      this.stuff = stuff;
    } else {
      throw new Error('Incorrect stuff item!');
    }

    this.topping = [];
  }

  addTopping(item) {
    if ((Object.is(item, Hamburger.TOPPING_MAYO) ||
        Object.is(item, Hamburger.TOPPING_SPICE)) &&
        !~this.topping.indexOf(item)) {
      this.topping.push(item);
    } else {
      throw new Error('Incorrect topping item!');
    }
  }

  removeTopping(item) {
    if ((Object.is(item, Hamburger.TOPPING_MAYO) ||
        Object.is(item, Hamburger.TOPPING_SPICE)) &&
        ~this.topping.indexOf(item)) {
      this.topping.splice(this.topping.indexOf(item), 1);
    } else {
      throw new Error('Incorrect topping item!');
    }
  }

  getToppings() {
    let list = [];
    for (let i = 0; i < this.topping.length; i++) {
      list.push(this.topping[i].name);
    }

    return list;
  }

  getSize() {
    return this.size.name;
  }

  getStuffing() {
    return this.stuff.name;
  }

  calculatePrice() {
    let price = 0;
    price += this.size.price;
    price += this.stuff.price;
    for (let i = 0; i < this.topping.length; i++) {
      price += this.topping[i].price;
    }

    return price;
  }

  calculateCalories() {
    let calories = 0;
    calories += this.size.calories;
    calories += this.stuff.calories;
    for (let i = 0; i < this.topping.length; i++) {
      calories += this.topping[i].calories;
    }

    return calories;
  }
}

Hamburger.SIZE_SMALL = { price: 50, calories: 20, name: 'small' };
Hamburger.SIZE_LARGE = { price: 100, calories: 40, name: 'large' };
Hamburger.STUFF_CHEESE = { price: 10, calories: 20, name: 'cheese' };
Hamburger.STUFF_SALAD = { price: 20, calories: 10, name: 'salad' };
Hamburger.STUFF_POTATO = { price: 15, calories: 10, name: 'potato' };
Hamburger.TOPPING_MAYO = { price: 20, calories: 5, name: 'mayo' };
Hamburger.TOPPING_SPICE = { price: 15, calories: 0, name: 'spice' };
