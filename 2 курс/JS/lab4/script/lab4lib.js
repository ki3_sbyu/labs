const days = ['неділя', 'понеділок', 'вівторок', 'середа', 'четвер',
 "п'ятниця", 'субота'];
const months = ['січня', 'лютого', 'березня', 'квітня', 'травня',
 'червня', 'липня', 'серпня', 'вересня', 'жовтня', 'листопада', 'грудня'];

function getCurrentDate() {
  let date = new Date();
  let result = "Дата: " + date.getDate() + " " + months[date.getMonth()] + " " +
   date.getFullYear() + " року\n";
  let minutes = date.getMinutes();

  minutes = minutes < 10? '0' + minutes: minutes;
  result += "День тижня: " + days[date.getDay()] + "\n";
  result += "Час: " + date.getHours() + ":" + minutes;
  return result;
}

function getDayOfWeekObject(date) {
  let obj = {dayNumber: 0, dayName: ""};

  obj.dayNumber = date.getDay();
  obj.dayName = days[date.getDay()];
  return obj;
}

function getLastDayOfMonth(year, month) {
  let date = new Date(year, month, 0);
  return date.getDate();
}

function getSeconds() {
  let res = {
    secondsFromStartDay: 0,
    secondsToStartNewDay: 0
  };
  let currentDate = new Date();
  let todayZeroDate = new Date(currentDate.getFullYear(),
    currentDate.getMonth(), currentDate.getDate());
  let tomorrowZeroDate = new Date(currentDate.getFullYear(),
    currentDate.getMonth(), currentDate.getDate() + 1);

  res.secondsFromStartDay = Math.round((currentDate - todayZeroDate)/1000);
  res.secondsToStartNewDay = Math.round((tomorrowZeroDate - currentDate)/1000);
  return res;
}

function toString(date) {
  let month = date.getMonth() + 1;
  let year = date.getFullYear();
  let day = date.getDate();
  return (day < 10? '0': day) + '.' + (month < 10? '0' + month : month) + '.' +
    year;
}

function getDateDifference(a, b) {
  let secondsDiff = Math.round(Math.abs(a - b) / 1000);
  let result = {
    days: Math.floor(secondsDiff / (3600 * 24)),
    hours: Math.floor(secondsDiff % (3600 * 24) / 3600),
    minutes: Math.floor(secondsDiff % (3600 * 24) % 3600 / 60),
    seconds: secondsDiff % (3600 * 24) % 3600 % 60,
  };
  return result;
}

function formatDate(date) {
  let diff = new Date() - date;
  let hours = date.getHours();
  let minutes = date.getMinutes();

  if(diff < 1000) {
     return "тільки що";
  }
  if(diff >= 1000 && diff < 60 * 1000) {
    return Math.floor(diff / 1000) + " сек. назад";
  }
  if(diff >= 60 * 1000 && diff < 3600 * 1000) {
        return Math.floor(diff / 60 / 1000) + " хв. назад";
  }
  return toString(date) + " " + (hours < 10? '0' + hours : hours) + ':' +
    (minutes < 10 ? '0' + minutes: minutes);
}

function getDate(date, code) {
  let options = {
    era: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    weekday: 'long',
    timezone: 'UTC',
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric'
  };
  return date.toLocaleString(code, options);
}

function getDaysAgo(days) {
  let newDate = new Date();
  newDate.setDate(newDate.getDate() - days);
  return toString(newDate);
}

function parseDate(date) {
  let dateFormats = [/(?:(\d{4})-(0[1-9]|1[0-2])-(3[01]|[12]\d|0[1-9]))/i,
                     /(?:(3[01]|[12]\d|0[1-9])\.(1[0-2]|0[1-9])\.(\d{4}))/i];
  let match = null, result = null;

  if(dateFormats[0].test(date)) {
    match = date.match(dateFormats[0]);
    result = new Date(+match[1], +match[2] - 1, +match[3]);
  }
  if(dateFormats[1].test(date)) {
    match = date.match(dateFormats[1]);
    result = new Date(+match[3], +match[2] - 1, +match[1]);
  }
  return result;
}
