let task1 = document.getElementById('getCurrentDateTimeButton');
task1.addEventListener('click', function() {
  let result = document.getElementById('getCurrentDateTimeResult');
  result.innerText = getCurrentDate();
});

let task2 = document.getElementById('getCurrentWeekDayButton');
task2.addEventListener('click', function() {
  let result = document.getElementById('getCurrentWeekDayResult');
  result.innerText = "Номер дня: " + getDayOfWeekObject(new Date()).dayNumber +
   '\n' + "Назва дня: " + getDayOfWeekObject(new Date()).dayName;
});

let task3 = document.getElementById('getDaysAgoButton');
task3.addEventListener('click', function() {
  let value = +document.getElementById('getDaysAgoText').value;
  let result = document.getElementById('getDaysAgoResult');
  result.innerText = getDaysAgo(value);
});

let task4 = document.getElementById('getLastDayButton');
task4.addEventListener('click', function() {
  let year = +document.getElementById('getLastDayYear').value;
  let month = +document.getElementById('getLastDayMonth').value;
  let result = document.getElementById('getLastDayResult');
  result.innerText = getLastDayOfMonth(year, month);
});

let task5 = document.getElementById('getSecondsButton');
task5.addEventListener('click', function() {
  let result = document.getElementById('secondsButtonResult');
  result.innerText = getSeconds().secondsFromStartDay + " с с начала дня\n" +
   getSeconds().secondsToStartNewDay + " с до начала следующего дня";
});

let task9 = document.getElementById('parseDateButton');
task9.addEventListener('click', function() {
  let result = document.getElementById('parseDateResult');
  let date = document.getElementById('parseDateText').value;
  result.innerText = parseDate(date);
});
