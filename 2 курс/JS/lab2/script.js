const regexp = {
  ip: /\b(?:(?:(25[0-5])|(2[0-4]\d)|(1?\d\d)|(\d))\.){3}(?:(25[0-5])|(2[0-4]\d)|(1?\d\d)|(\d))\b/i,
  rgba: /rgba\((25[0-5]|2[0-4]\d|1?\d\d|\d)\, *(25[0-5]|2[0-4]\d|1?\d\d|\d)\, *(25[0-5]|2[0-4]\d|1?\d\d|\d)\, *(1|0(?:\.\d+)?)\)/gi,
  hex: /\#(?:([\dA-F]{6})|([\dA-F]{3}))\b/gi,
  num: /(-?\d+)/gi,
  date: /\d{4}-(?:0[1-9]|1[0-2])-(?:3[01]|[12]\d|0[1-9])\b/gi
};

function findDates(text) {
  return text.match(regexp.date);
}

function findTags(text, tag) {
  let regexp = new RegExp("\<" + tag + ".*\>" + ".*" + "\<\/" + tag + "\>", "gi");
  return text.match(regexp);
}

function findPosNum(text) {
  let arr = text.match(regexp.num);
  for(let i = 0; i < arr.length; i++) {
    if(Number(arr[i]) < 0) {
      arr.splice(i, 1);
      i--;
    }
  }
  return arr.length > 0? arr: null;
}

function isIPAddress(ip) {
  return regexp.ip.test(ip);
}

function findRGBA(text) {
  return text.match(regexp.rgba);
}

function findHexColor(text) {
  return text.match(regexp.hex);
}

function isInteger(num) {
  if(num % 1 != 0) return false;
  return true;
}

function findPrimes(a, b) {
  let primes = [];

  for(let i = a; i <= b; i++) {
    if(i == 1 || i == 2) {
      primes.push(i);
      continue;
    }
    for(let j = 2; j <= parseInt(i / 2 + 1); j++) {
      if(i % j == 0) {
        break;
      }
      if(j == parseInt(i / 2 + 1)) {
        primes.push(i);
      }
    }
  }
  return primes;
}
