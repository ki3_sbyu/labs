﻿onmessage = function (event) {
    let primes = findPrimes(event.data.from, event.data.to);
    postMessage({messageType: 'primeList', data: primes });
}

function findPrimes(fromNumber, toNumber) {
    let prevProgress = 0;
    let list = [];
    for (let i = fromNumber; i <= toNumber; i++) {
        if (i > 1) list.push(i);
    }

    let maxDiv = Math.round(Math.sqrt(toNumber));
    let primes = [];

    for (let i = 0; i < list.length; i++) {
        let time = performance.now();
        for (let j = 2; j <= maxDiv; j++) {
            if ((list[i] != j) && (list[i] % j == 0)) {
                time = 0;
                break;
            }
            if (j == maxDiv) {
                primes.push('' + list[i] + ' - ' + (performance.now() - time).toFixed(3) + ' мс');
                time = 0;
            }
        }

        let progress = (i / list.length * 100).toFixed(2);

        if (progress != prevProgress) {
            postMessage({ messageType: 'progress', data: progress });
            prevProgress = progress;
        }
    }

    return primes;
}