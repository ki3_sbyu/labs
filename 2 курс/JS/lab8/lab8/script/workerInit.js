﻿
let start = document.querySelector('#searchButton');
let stop = document.querySelector('#stop');
let status = document.getElementById('status');
let fromNumberText = document.getElementById('from');
let toNumberText = document.getElementById('to');
let container = document.querySelector('#primeContainer');
let progress = document.querySelector('progress');
let notificationTimeOut;
let notifIsSended = false;
let worker;
let map;

window.onload = function () {
    navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationFailure);

    function geolocationSuccess(position) {
        console.log(`${position.coords.latitude} : ${position.coords.longitude}`);
  
    }

    function geolocationFailure(positionError) {
        console.error(positionError.message);
    }
}

function pushNotification(message, body) {
    let newNot = new Notification(message, body);
}

document.addEventListener('visibilitychange', function () {
    if (document.hidden && !notifIsSended) {
        notificationTimeOut = setTimeout(checkInterval, 5 * 1000);
        notifIsSended = true;
    } else if (!document.hidden) {
        clearTimeout(notificationTimeOut);
        notifIsSended = false;
    }

});

function checkInterval() {
    if (document.hidden) {
        pushNotification("Эй, ты куда ушел?");
        return true;
    }
    return false;
}

if (localStorage['from'] != null) {
    fromNumberText.value = localStorage['from'];
}

if (localStorage['to'] != null) {
    toNumberText.value = localStorage['to'];
}

if (localStorage['primes'] != null) {
    container.innerText = localStorage['primes'];
}

progress.max = 100;
start.addEventListener('click', doSearch);
stop.addEventListener('click', stopSearch);
stop.disabled = true;

function doSearch() {
    let fromNumber = fromNumberText.value;
    let toNumber = toNumberText.value;

    container.innerText = '';
    worker = new Worker('worker.js');
    stop.disabled = false;
    worker.onmessage = receivedMessage;
    worker.postMessage({ from: fromNumber, to: toNumber });

    status.innerText = `Ищем простые числа от ${fromNumber} до ${toNumber}...`;
    start.disabled = true;
}

function receivedMessage(event) {
    let message = event.data;
    switch (message.messageType) {
        case 'primeList':
            let primes = message.data;
            container.innerText = primes.join('\n');
            start.disabled = false;
            stop.disabled = true;
            localStorage['from'] = fromNumberText.value;
            localStorage['to'] = toNumberText.value;
            localStorage['primes'] = primes.join('\n');
            progress.value = 100;
            status.innerText = `Поиск завершён, количество найденых чисел равно ${primes.length}.`;
            switch (Notification.permission.toLowerCase()) {
                case 'granted':
                    pushNotification('Поиск простых чисел', {
                        tag: "success",
                        body: "Успешно завершил свою работу!",
                        image: "/success.png"
                    });
                    
                    break;
                case 'denied':
                    break;
                case 'default': default:
                    Notification.requestPermission();
                    break;
            }
            break;
        case 'progress':
            progress.value = message.data;
            break;
        default:
            stopSearch();
            status.innerText += '\nНеизвестная ошибка!';
            break;
    }

}

function workerError(error) {
    status.innerText = `Ошибка. Файл: ${error.filename}; строка: ${error.lineno}, ${error.message}`;
}

function stopSearch() {
    worker.terminate();
    worker = null;
    status.innerText = 'Поток остановлен!';
    progress.removeAttribute('value');
    start.disabled = false;
    stop.disabled = true;
}
