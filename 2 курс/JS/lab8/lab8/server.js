'use strict';
var express = require('express');
var app = express();
var port = process.env.PORT || 1337;

app.use(express.static('script'));
app.use(express.static('manifest'));
app.use(express.static('images'));

app.get('/*', function (req, res) {
    res.sendFile(__dirname + '/views/workers.html');
});


app.listen(port);