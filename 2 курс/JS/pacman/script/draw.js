function drawLevelMap(level) {
  let type = info.drawMethod;
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  if (type == drawType.sprite) {
    ctx.drawImage(levelMapSprite, 0, 0, info.currentLevel[0].length * cellSize,
      info.currentLevel.length * cellSize);
  }

  for (let i = 0, k = 0; i < level.length; i++, k += cellSize) {
    for (let j = 0, m = 0; j < level[0].length; j++, m += cellSize) {
      ctx.save();
      switch (level[i][j]) {
        case code.gate: {
          if (type == drawType.blocks) {
            ctx.strokeStyle = gateColor;
            ctx.moveTo(m, k + cellSize / 2);
            ctx.lineTo(m + cellSize, k + cellSize / 2);
            ctx.stroke();
          }

          break;
        }

        case code.wall: {
          if (type == drawType.blocks) {
            ctx.fillStyle = levelColor;
            ctx.fillRect(m, k, cellSize, cellSize);
          }

          break;
        }

        case code.empty: {
          if (type == drawType.blocks) {
            ctx.clearRect(m, k, cellSize, cellSize);
          } else {
            ctx.fillStyle = levelColor;
            ctx.fillRect(m, k, cellSize, cellSize);
          }

          break;
        }

        case code.dot: {
          dot.render(m, k);
          break;
        }

        case code.energizer: {
          energizer.render(m, k);
          break;
        }

        case code.cherry: {
          cherry.render(m, k);
          break;
        }

        case code.lemon: {
          lemon.render(m, k);
          break;
        }

        case code.strawberry: {
          strawberry.render(m, k);
          break;
        }

        case code.pear: {
          pear.render(m, k);
          break;
        }
      }
      ctx.restore();
    }
  }
}

function drawFruits() {
  let ctx = livesAndFruitCTX;
  ctx.clearRect(0, 0, livesAndFruit.width / 2, livesAndFruit.height);
  for (let i = 0, j = 0; i < info.eatedFruit.length; i++, j += cellSize) {
    ctx.drawImage(spriteImage, info.eatedFruit[i].x * spriteSize, info.eatedFruit[i].y * spriteSize,
       spriteSize, spriteSize, j, 0, cellSize, cellSize);
  }
}

function drawLives() {
  let ctx = livesAndFruitCTX;
  for (let i = 0, j = livesAndFruit.width - cellSize; i < info.lives; i++, j -= cellSize) {
    ctx.clearRect(j, 0, cellSize, cellSize);
    ctx.drawImage(spriteImage, 3 * spriteSize, pacman.skin, spriteSize, spriteSize,
        j, 0, cellSize, cellSize);
  }
}

function drawGrid(level) {
  ctx.strokeStyle = 'darkgreen';
  ctx.font = '10px Arial';
  ctx.fillStyle = 'white';
  let canvasWidth = level[0].length * cellSize;
  let canvasHeight = level.length * cellSize;
  for (let i = 0, j = 0; i < canvasWidth; i += cellSize, j++) {
    ctx.beginPath();
    ctx.moveTo(i, 0);
    ctx.lineTo(i, canvasHeight);
    ctx.stroke();
    ctx.fillText(j, i, cellSize);
  }

  for (let i = 0, j = 1; i < canvasHeight; i += cellSize, j++) {
    ctx.beginPath();
    ctx.moveTo(0, i);
    ctx.lineTo(canvasWidth, i);
    ctx.stroke();
    ctx.fillText(j, 0, i + cellSize * 2);
  }
}
