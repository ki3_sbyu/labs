/*jshint esversion: 6 */
function init() {
  info.currentLevel = levels[0];
  info.drawMethod = drawType.sprite;
  info.graph = getGraphFromMatrix(info.currentLevel);
  info.highscore = 0;
  info.lives = 3;
  info.time = 0;
  info.eatedFruit = [];
  info.started = false;
  info.seconds = 0;
  info.behavior = behavior.scatter;
  if (info.drawMethod == drawType.sprite) {
    levelColor = '#000';
  }

  document.body.style.backgroundColor = levelColor;

  blinky.defTargetX = info.currentLevel[0].length - 4;
  blinky.defTargetY = 1;

  inky.defTargetX = info.currentLevel[0].length - 4;
  inky.defTargetY = info.currentLevel.length - 2;
  inky.spriteRow = 6;

  pinky.defTargetX = 3;
  pinky.defTargetY = 1;
  pinky.spriteRow = 7;

  clyde.defTargetX = 3;
  clyde.defTargetY = info.currentLevel.length - 2;
  clyde.spriteRow = 5;

  document.body.appendChild(scoreBlock);
  document.body.appendChild(canvas);
  document.body.appendChild(livesAndFruit);
  scoreBlock.appendChild(highscoreLabel);
  scoreBlock.appendChild(scoreLabel);
  document.body.appendChild(sidebar);
  canvas.style.width = '900px';
  canvas.style.height = '1020px';

  sidebar.style.color = levelColor;
  sidebar.className = sidebarClass;
  scoreBlock.className = labelBlockClass;
  scoreBlock.style.width = 900 + 'px';
  livesAndFruit.width = '900';
  livesAndFruit.height = cellSize;
  livesAndFruit.className = livesAndFruitClass;
  canvas.width = info.currentLevel[0].length * cellSize;
  canvas.height = info.currentLevel.length * cellSize;
  highscoreLabel.innerText = 'HIGHSCORE:';
  scoreLabel.innerText = info.highscore;

  document.addEventListener('keydown', control);

  mainTimer = setInterval(main, 1000 / fps, info.currentLevel);
  frameTimer = setInterval(drawFrame, 1000 / fps, info.currentLevel);
  secondTimer = setInterval(timer, 1000);
}

function control(event) {
  switch (event.keyCode) {
    case 87: case 38:
      pacman.nextVector = up;
      break;
    case 68: case 39:
      pacman.nextVector = right;
      break;
    case 83: case 40:
      pacman.nextVector = down;
      break;
    case 65: case 37:
      pacman.nextVector = left;
      break;
    case 78:
      location.reload();
      break;
  }
}

function timer() {
  sidebar.innerText = info.seconds;
  info.seconds++;
  if (info.seconds > 59) {
    info.behavior = behavior.chase;
    return;
  }

  if (info.seconds > 54) {
    info.behavior = behavior.scatter;
    return;
  }

  if (info.seconds > 34) {
    info.behavior = behavior.chase;
    return;
  }

  if (info.seconds > 27) {
    info.behavior = behavior.scatter;
    return;
  }

  if (info.seconds > 7) {
    info.behavior = behavior.chase;
    return;
  }
}

function drawFrame(level) {
  let crd = getCurrentCell(pacman);
  if (!info.started) {
    drawLives();
    info.started = true;
  }

  drawLevelMap(level);
  let isStopped = false;
  switch (pacman.currentVector) {
    case up:
      if (isWall(crd.x, crd.y - 1, level)) {
        isStopped = true;
      }

      break;
    case down:
      if (isWall(crd.x, crd.y + 1, level)) {
        isStopped = true;
      }

      break;
    case left:
      if (isWall(crd.x - 1, crd.y, level)) {
        isStopped = true;
      }

      break;
    case right:
      if (isWall(crd.x + 1, crd.y, level)) {
        isStopped = true;
      }

      break;
  }
  pacman.render(isStopped);
  blinky.render();
  pinky.render();
  inky.render();
  clyde.render();
  if (info.highscore != +scoreLabel.innerText) {
    scoreLabel.innerText = info.highscore;
  }
}

function main(level) {
  let crd = getCurrentCell(pacman);
  pacman.move(level);
  blinky.move();
  inky.move();
  pinky.move();
  clyde.move();
  switch (level[crd.y][crd.x]) {
    case code.dot:
      info.highscore += dot.score;
      break;
    case code.energizer:
      info.highscore += energizer.score;
      /*TURN ON SCARED MODE FOR GHOSTS*/
      /*
      clyde.scared = true;
      clyde.speed = scaredSpeed;
      inky.scared = true;
      inky.speed = scaredSpeed;
      pinky.scared = true;
      pinky.speed = scaredSpeed;
      blinky.scared = true;
      blinky.speed = scaredSpeed;
      */
      break;
    case code.cherry:
      info.highscore += cherry.score;
      info.eatedFruit.push(cherry);
      drawFruits();
      break;
    case code.lemon:
      info.highscore += lemon.score;
      info.eatedFruit.push(lemon);
      drawFruits();
      break;
    case code.strawberry:
      info.highscore += strawberry.score;
      info.eatedFruit.push(strawberry);
      drawFruits();
      break;
    case code.pear:
      info.highscore += pear.score;
      info.eatedFruit.push(pear);
      drawFruits();
      break;
  }
  if (inTouch(blinky, pacman) || inTouch(inky, pacman) ||
      inTouch(pinky, pacman) || inTouch(clyde, pacman)) {
    clearInterval(mainTimer);
    clearInterval(frameTimer);
    clearInterval(secondTimer);
  }

  level[crd.y][crd.x] = code.empty;
}

function getNeighb(crd, matrix) {
  let list = [];
  let x = Number(crd.substring(0, crd.indexOf(separator)));
  let y = Number(crd.substring(crd.indexOf(separator) + 1));
  if (x + 1 < matrix[0].length) {
    if (matrix[y][x + 1] != code.wall) {
      list.push(x + 1 + separator + y);
    }
  }

  if (x - 1 >= 0) {
    if (matrix[y][x - 1] != code.wall) {
      list.push(x - 1 + separator + y);
    }
  }

  if (y + 1 < matrix.length) {
    if (matrix[y + 1][x] != code.wall) {
      list.push(x + separator + (y + 1));
    }
  }

  if (y - 1 >= 0) {
    if (matrix[y - 1][x] != code.wall) {
      list.push(x + separator + (y - 1));
    }
  }

  return list;
}

function getGraphFromMatrix(matrix) {
  let graph = [];
  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < matrix[0].length; j++) {
      if (!isWall(j, i, info.currentLevel, mode.ghost)) {
        graph[j + separator + i] = getNeighb(j + separator + i, matrix);
      }
    }
  }

  return graph;
}

function searchPath(start, target, graph, matrix) {
  let searchQueue = [];
  searchQueue.push(start);
  graph[start].node = 'none';
  let searched = [];
  while (searchQueue.length > 0) {
    let person = searchQueue.shift();
    if (searched.indexOf(person) < 0) {
      if (person == target) {
        let path = [];
        let father = target;
        do {
          path.push(father);
          father = graph[father].node;
        } while (father != graph[start].node);
        path.reverse();
        clearNodes(graph, matrix);
        return path;
      } else {
        let a = graph[person];
        for (let i = 0; i < a.length; i++) {
          if (graph[a[i]].node == undefined)
            graph[a[i]].node = person;
        }

        searchQueue = searchQueue.concat(a);
        searched.push(person);
      }
    }
  }

  clearNodes(graph, matrix);
}

function clearNodes(graph, matrix) {
  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < matrix[0].length; j++) {
      if (matrix[i][j] != code.wall && graph[j + separator + i].node != undefined) {
        graph[j + separator + i].node = undefined;
      }
    }
  }
}

init();
