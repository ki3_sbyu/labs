/*jshint esversion: 6 */
function clone(obj) {
  if (obj == null || typeof obj != 'object') return obj;
  var copy = obj.constructor();
  for (var attr in obj) {
    if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
  }

  return copy;
}

function isWall(x, y, level, mod) {
  if (x < 0 || y < 0) return false;
  if (x > level[0].length || y > level.length) return false;
  switch (mod) {
    default:
      return ~[code.wall, code.gate].indexOf(level[y][x]);
    case mode.ghost:
      return level[y][x] == code.wall;
  }
}

function inTouch(obj, trg) {
  let halfCell = cellSize / 2;
  return obj.x <= trg.x + halfCell && obj.x >= trg.x - halfCell &&
         obj.y <= trg.y + halfCell && obj.y >= trg.y - halfCell;
}

function itInCenter(object) {
  switch (object.currentVector) {
    case up: case down:
      return object.y % cellSize == 0;
    case left: case right:
      return object.x % cellSize == 0;
  }
}

function getCurrentCell(object) {
  let x;
  let y;
  switch (object.currentVector) {
    case up:
      y = Math.ceil(object.y / cellSize);
      x = (object.x - object.x % cellSize) / cellSize;
      break;
    case down:
      y = Math.floor(object.y / cellSize);
      x = (object.x - object.x % cellSize) / cellSize;
      break;
    case left:
      x = Math.ceil(object.x / cellSize);
      y = (object.y - object.y % cellSize) / cellSize;
      break;
    case right:
      x = Math.floor(object.x / cellSize);
      y = (object.y - object.y % cellSize) / cellSize;
      break;
  }
  return { x, y };
}

function getSolution(object) {
  object.setTarget();
  let crd = getCurrentCell(object);
  let selfCrd = crd.x + separator + crd.y;
  let targetCrd = object.targetX + separator + object.targetY;
  let temp;
  let vector = object.currentVector;
  switch (vector) {
    case up:
      temp = info.currentLevel[crd.y + 1][crd.x];
      info.currentLevel[crd.y + 1][crd.x] = code.wall;
      break;
    case down:
      temp = info.currentLevel[crd.y - 1][crd.x];
      info.currentLevel[crd.y - 1][crd.x] = code.wall;
      break;
    case left:
      temp = info.currentLevel[crd.y][crd.x + 1];
      info.currentLevel[crd.y][crd.x + 1] = code.wall;
      break;
    case right:
      temp = info.currentLevel[crd.y][crd.x - 1];
      info.currentLevel[crd.y][crd.x - 1] = code.wall;
      break;
  }
  let newGraph = getGraphFromMatrix(info.currentLevel);
  let path = searchPath(selfCrd, targetCrd, newGraph, info.currentLevel);
  object.currentVector = getWay(path, object);
  switch (vector) {
    case up:
      info.currentLevel[crd.y + 1][crd.x] = temp;
      break;
    case down:
      info.currentLevel[crd.y - 1][crd.x] = temp;
      break;
    case left:
      info.currentLevel[crd.y][crd.x + 1] = temp;
      break;
    case right:
      info.currentLevel[crd.y][crd.x - 1] = temp;
      break;
  }
}

function getWay(path, object) {
  if (Array.isArray(path)) {
    path.shift();
    let crd = getCurrentCell(object);
    if (!object.scared) {
      let trgX = Number(path[0].substring(0, path[0].indexOf(separator)));
      let trgY = Number(path[0].substring(path[0].indexOf(separator) + 1));
      return getVector(crd, trgX, trgY);
    } else if (object.scared) {
      let neighb = getNeighb(crd.x + separator + crd.y, info.currentLevel);
      let target;
      let trgX;
      let trgY;
      do {
        target = neighb[getRandomInt(0, neighb.length)];
        trgX = Number(target.substring(0, target.indexOf(separator)));
        trgY = Number(target.substring(target.indexOf(separator) + 1));
        console.warn({ target, neighb });
      } while (isWall(trgX, trgY, info.currentLevel, mode.ghost));
      return getVector(crd, trgX, trgY);
    } else return -1;
  }
}

function getVector(crd, trgX, trgY) {
  if (crd.x < trgX) {
    return right;
  }

  if (crd.x > trgX) {
    return left;
  }

  if (crd.y > trgY) {
    return up;
  }

  if (crd.y < trgY) {
    return down;
  }
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}
