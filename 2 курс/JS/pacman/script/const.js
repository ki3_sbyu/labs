//jshint esversion: 6
const canvas = document.createElement('canvas');
const ctx = canvas.getContext('2d');
const labelBlockClass = 'highscore';
const sidebarClass = 'sidebar';
const highscoreLabel = document.createElement('label');
const scoreLabel = document.createElement('label');
const scoreBlock = document.createElement('div');
const livesAndFruit = document.createElement('canvas');
const livesAndFruitClass = 'livesAndFruit';
const livesAndFruitCTX = livesAndFruit.getContext('2d');
const sidebar = document.createElement('aside');
const cellSize = 40;
const spriteSize = 128;
const spritePath = 'content/sprite/pacman_characters.png';
const levelSpritePath = 'content/sprite/level.png';
const up = 0;
const right = 1;
const down = 2;
const left = 3;
const scaredSpeed = 2;
const defSpeed = 4;
const drawType = {
  sprite: 1,
  blocks: 2,
};
const mode = {
  pacman: undefined,
  ghost: 1,
};
const behavior = {
  chase: 0,
  scatter: 1,
};
const ghostAddSize = 4;
let levelColor = '#639';
let gateColor = '#FFFF00';
const edgeWeight = 10;
const separator = ':';
const code = {
  ghostWall: -3,
  gate: -2,
  wall: -1,
  empty: 0,
  dot: 1,
  energizer: 2,
  cherry: 3,
  lemon: 4,
  strawberry: 5,
  pear: 6,
};
const spriteImage = new Image();
spriteImage.src = spritePath;
const levelMapSprite = new Image();
levelMapSprite.src = levelSpritePath;
const fps = 30;
