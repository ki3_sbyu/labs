var actionButton = document.getElementById("summAction"),
    firstNumber = document.getElementById("firstNumber"),
    secondNumber = document.getElementById("secondNumber"),
    resultText = document.getElementById("resultText");

actionButton.addEventListener("click", function() {
  let a = +firstNumber.value,
      b = +secondNumber.value;
    resultText.value = a + b;
});
