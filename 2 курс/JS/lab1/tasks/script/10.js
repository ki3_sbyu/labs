var maxLength = 10;

function truncate(str, maxLength) {
    if(str.length > maxLength)
      return str.substring(0, maxLength) + "...";
    else return str;
}

let stroke = prompt("Введите строку (макс. " + maxLength + " символов):");

alert(truncate(stroke, maxLength));
