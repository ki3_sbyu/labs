try:
    file = open('input.txt', 'r')
    number = int(file.readline())
    file.close()
    if number > 9 or number < 1:
        raise ValueError
    file = open('output.txt', 'a')
    file.write(str(number) + '9' + str(9 - number) + '\n')
except ValueError:
    print('Ошибка ввода!')
