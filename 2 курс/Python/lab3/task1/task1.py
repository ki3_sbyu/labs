try:
    file = open('input.txt', 'r')
    num = file.readline()
    file.close()
    if num.isdigit() and num[-1] == '5' and int(num) < 4e+5 and int(num) > 0:
        num = int(num[:len(num) - 1])
    else:
        raise ValueError
except ValueError:
    print('Ошибка. Неверные данные!')
else:
    num = str(num * (num + 1)) + '25\n'
    file = open('output.txt', 'w')
    file.write(num)
