def createFile(path):
    choise = input('Считать новые значения?\nY/N ')
    if choise == 'N':
        return
    intArray = []
    i = 0
    file = None
    while True:
        try:
            intArray.append(int(input(str(i) + ' : ')))
        except ValueError:
            print('Ошибка ввода!')
        else:
            if intArray[-1] != 0:
                i += 1
            else:
                intArray.pop()
                break
    try:
        file = open(path, 'r')
        print('Файл с таким именем уже существует. Перезаписать его?\n Y/N')
    except FileNotFoundError:
        print('Файл не найдено. Создать новый с этим именем?\n Y/N')
    else:
        answer = input()
        if answer.lower() == 'y':
            file = open(path, 'w')
            print('Файл создан!')
        elif answer.lower() == 'n':
            print('штож')
            return
        else:
            print('Неверный ввод!')
    finally:
        file.write(str(len(intArray)) + '\n')
        for i in intArray:
            file.write(str(i) + ' ')
    if file is not None:
        file.close()
        print('Файл успешно закрыт!')
