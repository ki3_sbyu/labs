import lib
inputFileName = 'input.txt'
outputFilename = 'output.txt'
try:
    numArray = []
    evenArr = []
    oddArr = []
    file = open(inputFileName, 'r')
    arrLength = int(file.readline())
    arrStr = file.readline()
    file.close()
    file = open(outputFilename, 'w')
    for i in arrStr.split():
        numArray.append(int(i))

    for i in numArray:
        if i % 2 == 0:
            evenArr.append(i)
        else:
            oddArr.append(i)
    for i in oddArr:
        file.write(str(i) + ' ')
    file.write('\n')
    for i in evenArr:
        file.write(str(i) + ' ')
    file.write('\n')
    if len(oddArr) > len(evenArr):
        file.write('NO')
    else:
        file.write('YES')
    file.close()
except ValueError:
    print('Ошибка ввода!')
