input = 'input.txt'
output = 'output.txt'
alpha = 'abcdefgh'
numbs = '12345678'

def check(alpha, beta):
    x = abs(ord(alpha[0]) - ord(beta[0]))
    y = abs(ord(alpha[1]) - ord(beta[1]))
    if (x == 1 and y == 2) or (x == 2 and y == 1):
        return True
    elif (x > 0 and x < 9) or (y > 0 and y < 8):
        return False
    else:
        return None
try:
    file = open(input, 'r')
    quest = file.readline();
    file.close()
    file = open(output, 'w')
    quest.index('-')
    if len(quest) != 5:
        raise ValueError

    quest = quest.lower()
    arr = quest.split('-')

    alpha.index(arr[0][0])
    alpha.index(arr[1][0])
    numbs.index(arr[0][1])
    numbs.index(arr[1][1])

    result = check(arr[0], arr[1])

    if result is None:
        file.write('ERROR\n')
    elif result:
        file.write('YES\n')
    else:
        file.write('NO\n')
except Exception:
    file.write('ERROR\n')
