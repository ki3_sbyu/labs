from kivy.app import App

from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.label import Widget

from kivy.config import Config
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from math import sin, cos, tan, sqrt

window_width = 600
window_height = 600
max_num_count = 17

Config.set('graphics', 'resizable', 0)
Config.set('graphics', 'width', window_width)
Config.set('graphics', 'height', window_height)

class CalcApp(App):
    def clear_numsys_labels(self):
            self.dec_d.text = 'DEC: '
            self.bin_d.text = 'BIN: '
            self.oct_d.text = 'OCT: '
            self.hex_d.text = 'HEX: '

    def invert_bin(self, bin):
        start = False
        binary = list(bin)
        for i in range(len(binary) + 1):
            if binary[-i] == '1' and not start:
                start = True
                continue
            if start:
                if int(binary[-(i)]) == 1:
                    binary[len(binary) - i] = '0'
                else:
                    binary[len(binary) - i] = '1'
        return "".join(binary)

    def restore_zeros(self, bin, sys):
        binary = str(bin)
        if sys == 8:
            while len(binary) < 63:
                binary = str(0) + binary
        else:
            while len(binary) < 64:
                binary = str(0) + binary
        return binary

    def from_binary(self, number, target_sys):
        result = ''
        if target_sys == 2:
            return number;
        if target_sys == 8:
            for i in range(0, len(number), 3):
                result = result + str(self.oct_bin[int(number[i: i + 3])])
            return result;
        if target_sys == 16:
            for i in range(0, len(number), 4):
                result = result + str(self.hex_bin[int(number[i: i + 4])])
            return result;

    def to_num_sys(self, num, num_sys):
        b = ''
        number = int(num)
        if number >= 0:
            while number > 0:
                if num_sys != 16:
                    b = str(number % num_sys) + b
                else:
                    key = number % num_sys
                    if key > 9:
                        b = self.nums_hex[key] + b
                    else:
                        b = str(number % num_sys) + b
                number = number // num_sys
            return b
        else:
            number = abs(number)
            while number > 0:
                b = str(number % 2) + b
                number = number // 2
            b = self.restore_zeros(b, num_sys)
            b = self.invert_bin(b)
            b = self.from_binary(b, num_sys)
        return b

    def add_sym(self, instance):
        text = instance.text
#        if self.formula.isdigit() and len(self.formula) > max_num_count - 1:
#            return
        if self.formula.isdigit() and text.isdigit() and self.isNewInput:
            self.formula = ''
            self.isNewInput = False
        if len(self.formula) > 1 and not self.formula[-1].isdigit() and not instance.text.isdigit():
            self.formula = self.formula[:-1]
        self.formula += str(text)
        self.update_display()

    def calculate(self, instance):
        try:
            self.formula = self.formula.replace('^', '**')
            self.formula = self.formula.replace('x', '*')
#            if len(self.formula) > max_num_count:
#                raise ValueError
            self.formula = str(eval(self.formula))
            print(self.formula)
            print(int(float(self.formula)))
            self.isNewInput = True
            self.update_display()
            self.update_labels()

        except ValueError:
            self.display.text = "Overflow"
            self.formula = '0'
            self.isNewInput = True
            self.clear_numsys_labels()
        except Exception:
            self.display.text = "ERROR"
            self.formula = '0'
            self.isNewInput = True
            self.clear_numsys_labels()

    def clear(self):
        self.isNewInput = True
        return '0'

    def delete(self):
        text = self.formula
        if len(text) > 1:
            while True:
                if text[-1].isdigit():
                    return text[:-1]
                else:
                    text =  text[:-1]
                    if text[-1].isdigit():
                        return text
        else:
            self.isNewInput = True
            return '0'

    def functions(self, instance):
        geo_funcs = { 'sqrt': sqrt, 'sin': sin, 'cos': cos, 'tan': tan }
        user_funcs = { 'C': self.clear, 'DEL': self.delete }
        name = instance.text
        if name in geo_funcs:
            self.formula = str(geo_funcs[name](eval(self.formula)))
        elif name in user_funcs:
            self.formula = str(user_funcs[name]())
        self.update_display()
        self.update_labels()

    def update_display(self):
        self.display.text = self.formula

    def update_labels(self):
        self.dec_d.text = 'DEC: ' + str(int(float(self.formula)))
        self.bin_d.text = 'BIN: ' + self.to_num_sys(int(float(self.formula)), 2)
        self.oct_d.text = 'OCT: ' + self.to_num_sys(int(float(self.formula)), 8)
        self.hex_d.text = 'HEX: ' + self.to_num_sys(int(float(self.formula)), 16)

    def build(self):
        self.nums_hex = dict({
            10: "A",
            11: "B",
            12: "C",
            13: "D",
            14: "E",
            15: "F"
        })
        self.hex_bin = dict({
            0: 0,
            1: 1,
            10: 2,
            11: 3,
            100: 4,
            101: 5,
            110: 6,
            111: 7,
            1000: 8,
            1001: 9,
            1010: 'A',
            1011: 'B',
            1100: 'C',
            1101: 'D',
            1110: 'E',
            1111: 'F'
        })
        self.oct_bin = dict({
            0: 0,
            1: 1,
            10: 2,
            11: 3,
            100: 4,
            101: 5,
            110: 6,
            111: 7
        })
        self.isNewInput = True
        self.formula = '0'
        gui_box = BoxLayout(orientation = 'vertical')
        sys_table = BoxLayout(orientation = 'vertical')
        display_box = BoxLayout()
        numpad_grid = GridLayout(cols = 4)
        func_n = "sqrt_sin_cos_tan_C_DEL"
        buttons_n = "^_/_7_8_9_x_4_5_6_-_1_2_3_+__0_."
        self.hex_d = Label(text = 'HEX: 0', size_hint = (1, .1), halign = "left", text_size = (window_width, None))
        self.dec_d = Label(text = 'DEC: 0', size_hint = (1, .1), halign = "left", text_size = (window_width, None))
        self.oct_d = Label(text = 'OCT: 0', size_hint = (1, .1), halign = "left", text_size = (window_width, None))
        self.bin_d = Label(text = 'BIN: 0', size_hint = (1, .1), halign = "left", text_size = (window_width, None))
        self.display = Label(text = '0', font_size = 50, halign = "right", valign = "top", text_size = (window_width, window_height * .35))
        sys_table.add_widget(self.hex_d)
        sys_table.add_widget(self.dec_d)
        sys_table.add_widget(self.oct_d)
        sys_table.add_widget(self.bin_d)
        sys_table.add_widget(self.display)

        for i in func_n.split('_'):
            numpad_grid.add_widget(Button(text = i, on_press = self.functions))

        for i in buttons_n.split('_'):
            if i == '':
                numpad_grid.add_widget(Widget())
                continue
            numpad_grid.add_widget(Button(text = i, on_press = self.add_sym))
        numpad_grid.add_widget(Button(text = '=', on_press = self.calculate))
        display_box.add_widget(sys_table)
        gui_box.add_widget(display_box)
        gui_box.add_widget(numpad_grid)
        return gui_box

if __name__ == '__main__':
    CalcApp().run()
