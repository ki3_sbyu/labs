import socket, threading, time
from string import whitespace
#import kivy framework
from kivy.app import App
from kivy.config import Config
#set window size
window_width = 500
window_height = 600
Config.set('graphics', 'resizable', False)
Config.set('graphics', 'width', window_width)
Config.set('graphics', 'height', window_height)

from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.scrollview import ScrollView
from kivy.core.window import Window

from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.label import Label

class MyChatApp(App):
    def add_message(self, message, sender):
        length = len(message)
        step = 50
        for i in range(0, length, step):
            if i == 0:
                self.messages.add_widget(
                    Label(text = sender + ': ' + message[i: i + step], text_size = (Window.width * .9, None)))
            else:
                self.messages.add_widget(
                    Label(text = message[i: i + step], text_size = (Window.width * .9, None)))

    def clear_textbox(self):
        self.textbox.text = ''

    def send_message(self, instance):
        if self.textbox.text.strip(whitespace) != '':
            self.add_message(message = self.textbox.text.strip(whitespace), sender = "Me")
            self.send_to_server(message = self.textbox.text.strip(whitespace))
            self.clear_textbox()

    def login(self, instance):
        if self.textbox.text.strip(whitespace) != '' and len(self.textbox.text) < 10:
            self.nickname = self.textbox.text.strip(whitespace)
            self.send_button.unbind(on_press = self.login)
            self.send_button.bind(on_press = self.send_message)
            self.add_message(message = 'Hello, ' + self.nickname, sender = "System")
            self.clear_textbox()
            self.send_to_server(message = 'has joined the chat!')
        else:
            self.add_message(message = 'Error. Enter correct nickname!', sender = "System")

    def receving(self, name, sock):
        while not self.shutdown:
            try:
                while True:
                    data, addr = sock.recvfrom(1024)
                    data = data.decode('utf-8')
                    nick = data.split(' ')[0]
                    message = data[data.index(' ') + 1:]
                    self.add_message(message = message, sender = nick)
                    time.sleep(0.2)
            except:
                pass

    def send_to_server(self, message):
        message = self.nickname + ' ' + message
        self.s.sendto((message).encode("utf-8"), self.server)

    def build(self):
        self.shutdown = False
        self.join = False
        self.host = socket.gethostbyname("192.168.1.105" or socket.gethostname())
        self.port = 0
        self.server = (self.host, 9090)
        self.s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.s.bind((self.host, self.port))
        self.s.setblocking(0)
        self.rT = threading.Thread(target = self.receving, args = ("RecvThread", self.s))
        self.rT.start()
        self.nickname = ""
        self.textbox = TextInput(text = '', height = 40)
        self.send_button = Button(text = 'Send', size_hint = (.2, 1), on_press = self.login)

        envirLayout = BoxLayout(orientation = 'vertical')
        messDisplay = ScrollView(size_hint = (1, 10))
        inputbox = BoxLayout()

        self.messages = GridLayout(cols = 1, spacing = 40, size_hint_y = None)
        self.messages.bind(minimum_height = self.messages.setter('height'))
        self.add_message(message = "", sender = "")
        self.add_message(message = "Enter your nickname...", sender = "System")

        inputbox.add_widget(self.textbox)
        inputbox.add_widget(self.send_button)
        messDisplay.add_widget(self.messages)
        envirLayout.add_widget(messDisplay)
        envirLayout.add_widget(inputbox)

        return envirLayout

if __name__ == '__main__':
    MyChatApp().run()
