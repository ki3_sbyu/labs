firstNum = int(input())
secondNum = int(input())
thirdNum = int(input())

max = max(firstNum, secondNum, thirdNum)
min = min(firstNum, secondNum, thirdNum)
avg = thirdNum

if firstNum <= max and firstNum >= min:
    avg = firstNum
elif secondNum <= max and secondNum >= min:
    avg = secondNum
print(max, min, avg)
