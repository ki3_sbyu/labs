yearInput = int(input("Введите год: "))

def isLeapYear(year):
    if year >= 1900 and year <= 3000:
        if (year % 4 == 0 and year % 100 != 0) or year % 400 == 0:
            return 1
        return 0
    return -1

result = isLeapYear(yearInput)

if result == 1:
    print(yearInput, "- високосный год", sep=" ")
if result == 0:
    print(yearInput, "- обычный год", sep=" ")
if result == -1:
    print("Ошибка ввода, введите число из диапазона [1900, 3000]")
