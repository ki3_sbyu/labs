input = 'input.txt'
output = 'output.txt'
try:
    file = open(input, 'r')
    numArray = file.readline()
    file.close()
    numArray = numArray.split(' ')
    for i in range(len(numArray)):
        if int(numArray[i]) < 10e100:
            numArray[i] = int(numArray[i])
        else:
            raise ValueError
    file = open(output, 'w')
    file.write(str(max(numArray)))
    file.close();
except Exception:
    print('Ошибка!')
