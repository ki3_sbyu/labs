input = "input.txt"
output = "output.txt"
with open(input, 'r') as inputFile:
    arr = []
    while True:
        string = inputFile.readline()
        if string:
            lst = list(map(lambda x: int(x), string.split(' ')))
            if len(lst) == 4:
                arr.append(lst)
            else:
                arr.append([])
        else:
            break

with open(output, "w") as outputFile:
    for lst in arr:
        if lst:
            a, b, c, d = lst

            for x in range(-100, 101):
                square = a * x ** 3 + b * x * x + c * x + d
                if square == 0:
                    outputFile.write(str(x) + " ")

            outputFile.write("\n")
        else:
            outputFile.write("Error\n")
