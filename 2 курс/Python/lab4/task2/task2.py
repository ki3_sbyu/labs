input = 'input.txt'
output = 'output.txt'
file = None
try:
    file = open(input, 'r')
    str = file.readline()
    file.close()
    numArray = str.split(' ')
    file = open(output, 'w')
    if int(numArray[0]) * int(numArray[1]) == int(numArray[2]):
        file.write('YES')
    else:
        file.write('NO')

except FileNotFoundError:
    print('Ошибка. Файл не найден!')
except Exception:
    print('Ошибка ввода!')
    file.write('ERROR')
finally:
    file.close()
