input = 'input.txt'
output = 'output.txt'
try:
    with open(input, 'r') as file:
        elemCount = int(file.readline())
        elements = file.readline().split(' ')
        sum = 0
        mul = 1
        for i in range(len(elements)):
            elements[i] = int(elements[i])

        firstIndex = elements.index(max(elements))
        secondIndex = elements.index(min(elements))
        if firstIndex < secondIndex:
            firstIndex, secondIndex = secondIndex, firstIndex
        for i in elements:
            if i > 0:
                sum += i
        for i in range(secondIndex + 1, firstIndex):
            mul *= elements[i]
        if abs(mul) > 3e4 or abs(sum) > 3e4:
            raise ValueError
    with open(output, 'w') as outputFile:
        outputFile.write(str(sum) + ' ' + str(mul))
except FileNotFoundError:
    print('Ошибка! Файл не найден!')
except ValueError:
    print('Ошибка! Неверные входные данные!')
except Exception:
    print('Неизвестная ошибка!')
