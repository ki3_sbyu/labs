import math
PI = math.pi
triangle = 'треугольник'
ring = 'круг'
rectangle = 'прямоугольник'
try:
    figure = input('Введите нужный тип фигуры: ')
    if figure == triangle:
        a = int(input('a = '))
        b = int(input('b = '))
        c = int(input('c = '))
        p = (a + b + c) / 2
        result = math.sqrt(p * (p - a) * (p - b) * (p - c))
    elif figure == ring:
        r = int(input('r = '))
        result = PI * pow(r, 2)
    elif figure == rectangle:
        a = int(input('a = '))
        b = int(input('b = '))
        result = a * b
    else:
        raise ValueError
except ValueError:
    print('Ошибка ввода!')
except Exception:
    print('Неизвестная ошибка!')
else:
    print('Результат: ',result)
