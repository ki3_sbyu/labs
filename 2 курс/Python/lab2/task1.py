isGood = False
operations = ['+', '-', '/', '*', 'mod', 'pow', 'div']
result = ''
while not isGood:
    try:
        a = float(input('Введите первое число: '))
        b = float(input('Введите следующее число: '))
        op = input('Выберите операцию: ')
        operations.index(op)
        if op == '/':
                result = a / b
        elif op == 'div':
                result = a // b
        elif op == 'mod':
                result = a % b
        elif op == '+':
            result = a + b
        elif op == '-':
            result = a - b
        elif op == '*':
            result = a * b
        elif op == 'pow':
            result = a ** b
    except ValueError:
        print('Ошибка ввода!\n')
    except ZeroDivisionError:
        print('Ошибка! Попытка поделить на ноль!\n')
    except Exception:
        print('Неизвестная ошибка!')
    else:
        isGood = True
        print('Результат:', result)
