try:
    number = input('Введіть номер квитка: ')
    if not number.isdigit() or len(number) != 6:
        raise ValueError
except ValueError:
    print('Помилка введення!')
except Exception:
    print('Невідома помилка!')
else:
    a = 0
    b = 0
    for i in range(int(len(number) / 2)):
        a += int(number[i])
    for i in range(int(len(number) / 2), len(number)):
        b += int(number[i])
    if a == b:
        print('Щасливий квиток!')
    else:
        print('Звичайний квиток!')
