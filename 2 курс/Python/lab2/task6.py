def evklid(a, b):
    return b if a % b == 0 else evklid(b, a % b)
try:
    a = int(input('a = '))
    b = int(input('b = '))
    if a <= 0 or b <= 0:
        raise ValueError
except ValueError:
    print('Помилка вхідних данних!')
else:
    result = a * b // evklid(a, b)
    print('Результат:', result)
