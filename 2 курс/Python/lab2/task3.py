try:
    n = int(input('n = '))
    if (n < 0):
        raise ValueError
except ValueError:
    print("Помилка вхідних данних!")
except Exception:
    print("Невідома помилка!")
else:
    if n % 10 > 1 and n % 10 < 5 and (n % 100 < 12 or n % 100 > 21):
        word = "програмісти"
    elif n % 10 == 1 and n % 100 != 11:
        word = "програміст"
    else:
        word = "програмістів"
    print(n, word)
