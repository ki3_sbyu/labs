sum = 0
while True:
    try:
        num = int(input('Введіть число: '))
    except ValueError:
        print('Помилка введення!')
    else:
        if num != 0:
            sum += num
            continue
        print('Результат:', sum)
        break
