from urllib import request
from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
import json

def get_date(str):
    index = 0
    for i in str:
        if i == 'T':
            return str[0:index]
        index += 1


def currency():
    myUrl_currency = 'http://resources.finance.ua/ua/public/currency-cash.json'
    answer_currency = request.urlopen(myUrl_currency)
    body_currency = answer_currency.read()
    fromJson_currency = json.loads(body_currency.decode('utf-8'))

    date_ymd = get_date(fromJson_currency['date'])
    print('\nDate: ' + date_ymd + '\n')

    arr_org = fromJson_currency['organizations']

    for i in arr_org:
        eur, usd, rub = 'EUR', 'USD', 'RUB'
        print('*' * 100)
        print('Bank: ' + i['title'])
        print('Link: ' + i['link'])
        if eur in i['currencies']:
            print('\nCurrent: ' + eur + '\nBuy: ' + i['currencies'][eur]['ask'] +
             '\nSell: ' + i['currencies'][eur]['bid'])
        if usd in i['currencies']:
            print('\nCurrent: ' + usd + '\nBuy: ' + i['currencies'][usd]['ask'] +
             '\nSell: ' + i['currencies'][usd]['bid'])
        if rub in i['currencies']:
            print('\nCurrent: ' + rub + '\nBuy: ' + i['currencies'][rub]['ask'] +
             '\nSell: ' + i['currencies'][rub]['bid'])


def metal():
    myUrl_metal = 'http://resources.finance.ua/ua/public/metal-cash.json'
    answer_metal = request.urlopen(myUrl_metal)
    body_metal = answer_metal.read()
    fromJson_metal = json.loads(body_metal.decode('utf-8'))

    date_ymd = get_date(fromJson_metal['date'])
    print('\nDate: ' + date_ymd + '\n')

    arr_org = fromJson_metal['organizations']

    for i in arr_org:

        xag, xau, m = 'XAG', 'XAU', 'metals'

        print('*' * 100)
        print('Bank: ' + i['title'])
        print('Link: ' + i['link'])

        if xag in i[m]:
            print('\nMetal: Silver\n')
            for id in range(10, 130, 10):
                if str(id) in i[m][xag]:
                    print(str(id) + ':\n' + 'Buy: ' + str(i[m][xag][str(id)]['ask']) +
                     '\nSell: ' + str(i[m][xag][str(id)]['bid']) + '\n')

        if xau in i[m]:
            print('\nMetal: Gold\n')
            for id in range(10, 130, 10):
                if str(id) in i[m][xau]:
                    print(str(id) + ':\n' + 'Buy: ' + str(i[m][xau][str(id)]['ask']) +
                     '\nSell: ' + str(i[m][xau][str(id)]['bid']) + '\n')

currency()
metal()
