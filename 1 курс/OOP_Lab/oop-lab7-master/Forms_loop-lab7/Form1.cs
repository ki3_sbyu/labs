﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FormClassLibrary;

namespace Forms_loop_lab7
{
    public partial class Form1 : Form
    {
        Random rnd = new Random();
        ShapePoint[] figure = new ShapePoint[4];
        ShapePoint[] ShapeArr = new ShapePoint[20];
        public Form1()
        {
            InitializeComponent();
            InitArray(ref figure, ref ShapeArr);
        }

        private void Draw_Button_Click(object sender, EventArgs e) //малювання фігури
        {
            Bitmap cap = new Bitmap(MyPictureBox.Width, MyPictureBox.Height);
            Graphics g = Graphics.FromImage(cap);
            int count_color =rnd.Next(), count_figure=rnd.Next();
            Color _myCol = Color.FromArgb(rnd.Next(0, 255), rnd.Next(0, 255), rnd.Next(0, 255));
            Pen pen = new Pen(_myCol, 5);
            ShapeArr[rnd.Next(1, 20)].Draw(g, pen, _myCol);
            MyPictureBox.Image = cap;
        }
        private void InitArray(ref ShapePoint[] figure, ref ShapePoint[] ShapeArr) //генерація масиву фігур
        {
            figure[0] = new ShapeSection();
            figure[1] = new ShapeRectangle();
            figure[2] = new ShapeRound();
            figure[3] = new ShapeEllipse();
            for (int i = 0; i < 20; i++) ShapeArr[i] = Shape(rnd.Next(1, 5), figure);
        }
        private ShapePoint Shape(int k, ShapePoint[] figures) // повернення k фігури
        {
            k--;
            return figures[k];
        }

        private void Clear_Button_Click(object sender, EventArgs e) //очищення PictureBox
        {
            if (MyPictureBox.Image != null)
            {
                MyPictureBox.Image.Dispose();
                MyPictureBox.Image = null;
            }
        }
    }
}
