﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyClassLibrary;

namespace ConsoleApp_oop_lab7
{
    class Program
    {
        static void Main(string[] args)
        {
            int menu = 1;
            do
            {
                Console.WriteLine("Записать и вывести на экран информацию об:\n" +
                    "1.Абитуриент.\n2.Студент.\n3.Преподаватель.\n4.Пользователь библиотеки.\n0.Выход.");
                for (int i = 0; i != 1; i++)
                    switch (Console.ReadLine())
                    {
                        case "0":
                            menu = 0;
                            break;
                        case "1":
                            Applicant jack = new Applicant();
                            jack.Set();
                            jack.ShowInfo();
                            break;
                        case "2":
                            Student marry = new Student();
                            marry.Set();
                            marry.ShowInfo();
                            break;
                        case "3":
                            Lector harry = new Lector();
                            harry.Set();
                            harry.ShowInfo();
                            break;
                        case "4":
                            LibraryUser antony = new LibraryUser();
                            antony.Set();
                            antony.ShowInfo();
                            break;
                        default:
                            Check.PrintError();
                            break;
                    }
            } while (menu != 0);
        }
    }
}