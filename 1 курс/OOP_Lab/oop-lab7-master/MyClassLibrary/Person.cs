﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyClassLibrary
{
    public class Person
    {
        protected string _firstName = "no_name";
        protected string _lastName = "no_name";
        protected Date _birthDay = new Date();
        public Person(string firstname, string lastname, Date birthday)
        {
            if (Check.Correct(Target.Phrase, firstname, lastname) == true)
            {
                _firstName = firstname;
                _lastName = lastname;
            }
            _birthDay = new Date(birthday);
        }
        public Person(string firstname, string lastname) : this(firstname, lastname, new Date()) { }
        public Person() : this("no_name", "no_name", new Date()) { }    
        public Person(Person obj)
        {
            _firstName = obj._firstName;
            _lastName = obj._lastName;
            _birthDay = new Date(obj._birthDay);
        }

        virtual public void Set()
        {
            string _chFirstName, _chLastName;
            for (int i = 0; i != 1;)
            {
                Console.WriteLine("Имя:"); _chFirstName = Console.ReadLine();
                Console.WriteLine("Фамилия:"); _chLastName = Console.ReadLine();
                if (Check.Correct(Target.Phrase, _chFirstName, _chLastName) == true)
                {
                    _firstName = _chFirstName;
                    _lastName = _chLastName;
                    i = 1;
                }
                else Check.PrintError();
            }
            Console.WriteLine("Дата рождения"); _birthDay.Set();

        }
        virtual public void ShowInfo()
        {
            Console.Write($"Имя:{_firstName} Фамилия:{_lastName} Дата рождения:"); _birthDay.ShowInfo();
        }
    }
}