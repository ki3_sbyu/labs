﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyClassLibrary
{
   public class Lector : Person
    {
        protected string _position;
        protected string _kathedra;
        protected string _university;
        public Lector(string position, string kathedra, string university)
        {
            _position = position; _kathedra = kathedra; _university = university;
        }
        public Lector() : this("no_info", "no_info", "no_info") { }
        public Lector(string position) : this(position, "no_info", "no_info") { }
        public Lector(Lector obj)
        {
            _firstName = obj._firstName;
            _lastName = obj._lastName;
            _birthDay = new Date(obj._birthDay);
            _position = obj._position;
            _kathedra = obj._kathedra;
            _university = obj._university;
        }
        override public void Set()
        {
            base.Set();
                Console.WriteLine("Должность:"); _position = Console.ReadLine();
                Console.WriteLine("Кафедра:");_kathedra = Console.ReadLine();
                Console.WriteLine("ВУЗ:");_university = Console.ReadLine();
        }
        public override void ShowInfo()
        {
            base.ShowInfo();
            Console.Write($"Должность:{_position} Кафедра:{_kathedra} ВУЗ:{_university}");
        }
    }
}