﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyClassLibrary
{
    public class LibraryUser : Person
    {
        protected int _numOfAbonement = 0;
        protected Date _dateOfGive = new Date();
        protected int _monthlyFee = 0;
        public LibraryUser(int numAbon, Date dateGive, int monthFee)
        {
            _numOfAbonement = numAbon;
            _dateOfGive = new Date(dateGive);
            _monthlyFee = monthFee;
        }
        public LibraryUser(int numAbon) : this(numAbon, new Date(), 100) { }
        public LibraryUser() : this(1, new Date(), 100) { }
        public override void Set()
        {
            base.Set();
            for (int i = 1; i < 4; i++)
            {
                switch (i)
                {
                    case 1:
                        Console.WriteLine("Номер абонемента:");
                    bool isOk = Int32.TryParse(Console.ReadLine(), out _numOfAbonement);
                    if (!isOk) { Check.PrintError(); i--; }
                    break;
                    case 2:
                        Console.WriteLine("Дата выдачи ");
                        _dateOfGive.Set();
                        break;
                    case 3:
                        Console.WriteLine("Ежемесячный взнос: ");
                        isOk = Int32.TryParse(Console.ReadLine(), out _monthlyFee);
                            if (!isOk) { Check.PrintError(); i--; }
                        break;
                    default: break;
                }
            }
        }
        public override void ShowInfo()
        {
            base.ShowInfo();
            Console.WriteLine($"Абонемент:{_numOfAbonement} Дата выдачи: ");_dateOfGive.ShowInfo();Console.WriteLine($" Ежемесячный взнос:{_monthlyFee}");
        }
    }
}
