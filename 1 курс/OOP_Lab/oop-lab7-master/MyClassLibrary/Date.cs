﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyClassLibrary
{
   public class Date
    {
        protected int _day = 1;
        protected int _month = 1;
        protected int _year = 1900;
        public Date(string day, string month, string year)
        {
            if(Check.Correct(Target.Digit,day,month,year)==true)
            {
                int[] _chkArr = new int[3];
                _chkArr[0] = Convert.ToInt32(day);
                _chkArr[1] = Convert.ToInt32(month);
                _chkArr[2] = Convert.ToInt32(year);
                if(_chkArr[0]<=31&&_chkArr[0]>=1
                    &&_chkArr[1]<=12&&_chkArr[1]>=1
                    &&_chkArr[2]<=9999&&_chkArr[2]>=1900)
                {
                    _day = _chkArr[0]; _month = _chkArr[1];_year = _chkArr[2];
                }
            }
        }
        public Date() : this("1", "1", "1900") { }
        public Date(string day, string month) : this(day, month, Convert.ToString(DateTime.Now.Year)) { }
        public Date(Date obj)
        {
            _day = obj._day;
            _month = obj._month;
            _year = obj._year;
        }
        public void Set()
        {
            for (int i = 0; i != 1;)
            {
                string day, month, year; int _Day, _Month, _Year;
                Console.WriteLine("День:"); day = Console.ReadLine();
                Console.WriteLine("Месяц:"); month = Console.ReadLine();
                Console.WriteLine("Год"); year = Console.ReadLine();
                if (Check.Correct(Target.Digit, day, month, year) == true)
                {
                    _Day = Convert.ToInt32(day); _Month = Convert.ToInt32(month); _Year = Convert.ToInt32(year);
                    if ((_Day <= 31 && _Day >= 1)
                        && (_Month <= 12 && _Month >= 1)
                        && (_Year <= 9999 && _Year >= 1900))
                    {
                        this._day = _Day;
                        this._month = _Month;
                        this._year = _Year;
                        i = 1;
                    }
                    else Check.PrintError();
                }
                else Check.PrintError();
            }
            
        }
        virtual public void ShowInfo()
        {
            Console.Write($"{ _day, 2}/{_month, 2}/{_year, -4} ");
        }
    }
}