﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyClassLibrary
{
    public enum Target
    {
        Phrase = 1, Digit
    };
    public static class Check
    {
        public static bool Correct(Target obj, params string []_strArr)
        {
            string value = "";
            for (int i = 0; i < _strArr.Length; i++)
                value += _strArr[i];
            char[] _charArr = value.ToCharArray();
            for(int i=0;i<_charArr.Length;i++)
            {
                switch (obj)
                {
                    case Target.Digit:
                        {
                            if (!char.IsDigit(_charArr[i]))
                                return false;
                            break;
                        }
                    case Target.Phrase:
                        {
                            if (!char.IsLetter(_charArr[i]) && _charArr[i] != ' ')
                                return false;
                            break;
                        }
                    default: return false;
                }
            }
            return true;
        }
        public static void PrintError()
        {
            Console.Clear();
            Console.WriteLine("Ошибка. Повторите попытку.\n");
        }
    }
}