﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyClassLibrary
{
    public class Applicant : Person
    {
        protected int[] _pointsESV = { 1, 1, 1 };
        protected int _pointsCertificate = 1;
        protected string _nameOfSchool = "no_name";
        public Applicant(int pointCertificate, int[] pointsESV, string nameOfSchool)
        {
            if (pointCertificate <= 200 && pointCertificate >= 1)
                _pointsCertificate = pointCertificate;
            for (int i = 0; i < 3; i++)
                if (pointsESV[i] >= 1 && pointsESV[i] <= 200)
                    _pointsESV[i] = pointsESV[i];
            if (Check.Correct(Target.Phrase, nameOfSchool) == true)
                _nameOfSchool = nameOfSchool;
        }
        public Applicant(int pointCertificate, int[] pointsESV) : this(pointCertificate, pointsESV, "no_name") { }
        public Applicant() : this(100, new int[] { 100, 100, 100 }, "no_name") { }
        public Applicant(int[] _pointsESV) : this(100, _pointsESV, "no_name") { }
        public Applicant(Applicant obj)
        {
            _firstName = obj._firstName;
            _lastName = obj._lastName;
            _birthDay = new Date(obj._birthDay);
            _nameOfSchool = obj._nameOfSchool;
            _pointsCertificate = obj._pointsCertificate;
            obj._pointsESV.CopyTo(_pointsESV, 0);
        }
        override public void Set()
        {
            string chESV; int _chESV;
            base.Set();
            Console.WriteLine("Баллы ЗНО и сертификата:");
            for (int i = 0; i < _pointsESV.Length + 1; i++)
            {
                chESV = Console.ReadLine(); _chESV = Convert.ToInt32(chESV);
                if (Check.Correct(Target.Digit, chESV) == true && _chESV >= 1 && _chESV <= 200)
                {
                    if (i < 3)
                        _pointsESV[i] = _chESV;
                    else if (i == 3)
                        _pointsCertificate = _chESV;
                }
                else { Check.PrintError(); i--; }
            }
            Console.WriteLine("Название школы:"); _nameOfSchool = Console.ReadLine();
        }
        override public void ShowInfo()
        {
            base.ShowInfo();
            Console.Write($"\nБаллы ЗНО (1п/2п/3п):{_pointsESV[0],-3}/{_pointsESV[1],-3}/{_pointsESV[2],-3} " +
                $"Баллы сертификата: {_pointsCertificate,3} Названия школы: {_nameOfSchool}\n");
        }
    }
}