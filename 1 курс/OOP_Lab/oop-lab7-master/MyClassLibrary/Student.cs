﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyClassLibrary
{
    public class Student : Person
    {
        protected int _course = 1;
        protected string _group = "no_info";
        protected string _faculty = "no_info";
        protected string _university = "no_info";
        public Student(string _name, string _lastname,string _course, string _group, string _faculty,string _university)
        {
            if (Check.Correct(Target.Digit, _course) == true)
                this._course = Convert.ToInt32(_course);
            this._group = _group;
            this._university = _university;
            this._faculty = _faculty;
            if(Check.Correct(Target.Phrase, _name,_lastname)== true)
            {
                _firstName = _name;
                _lastName = _lastname;
            }
        }
        public Student(string name, string lastname, string group, string university) : this(name, lastname, "1", group, "no_info", university) { }
        public Student()
        {
            _firstName = "no_name";
            _lastName = "no_lastname";
            _birthDay = new Date();
        }
        public Student(Student obj)
        {
            _firstName = obj._firstName;
            _lastName = obj._lastName;
            _group = obj._group;
            _university = obj._university;
            _faculty = obj._faculty;
            _course = obj._course;
            _birthDay = new Date(obj._birthDay);
        }
        override public void Set()
        {
            base.Set();
            for (int i = 0; i < 4; i++)
            {
                if (i == 0)
                {
                    Console.WriteLine("Курс:");
                    bool chIs = Int32.TryParse(Console.ReadLine(), out _course);
                    if (!chIs)
                        i--;
                }
                else if (i == 1)
                {
                    Console.WriteLine("Группа:");
                        _group = Console.ReadLine();
                }
                else if (i == 2)
                {
                    Console.WriteLine("Факультет:");
                    _faculty = Console.ReadLine();
                }
                else if (i == 3)
                {
                    Console.WriteLine("ВУЗ:");
                    _university = Console.ReadLine();
                }
                else Check.PrintError();
            }
        }
        public override void ShowInfo()
        {
            base.ShowInfo();
            Console.WriteLine($"Курс:{_course} Группа:{_group} Факультет:{_faculty} ВУЗ:{_university}\n");
        }
    }
}