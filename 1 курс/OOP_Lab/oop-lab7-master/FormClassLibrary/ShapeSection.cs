﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace FormClassLibrary
{
    public class ShapeSection : ShapePoint
    {
        protected int x2;
        public int X2
        {
            get { return x2; }
            set { if (value > 0) x2 = value; else x2 = 200; }
        }
        protected int y2;
        public int Y2
        {
            get { return y2; }
            set { if (value > 0) y2 = value; else y2 = 200; }
        }
        public ShapeSection() : base()
        {
            x2 = 200;
            y2 = 200;
        }
        public ShapeSection(int _x1, int _y1, int _x2, int _y2, Color _color) : base(_x1, _y1, _color)
        {
            x2 = _x2;
            y2 = _y2;
        }
        public ShapeSection(ShapeSection obj, ShapePoint obj_p) : base(obj_p)
        {
            x2 = obj.x2;
            y2 = obj.y2;
        }
        public override void Draw(Graphics obj, Pen pen, Color col)
        {
            obj.DrawLine(pen, 50, 50, 350, 250);
        }
    }
}
