﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace FormClassLibrary
{
    public class ShapeRound : ShapePoint
    {
        protected int r;
        public int R
        {
            get { return r; }
            set { if (value > 0) r = value; else r = 50; }
        }
        public ShapeRound() : base()
        {
            r = 50;
        }
        public ShapeRound(int _x1, int _y1, int _r, Color _color) : base(_x1, _y1, _color)
        {
            r = _r;
        }
        public ShapeRound(ShapeRound obj, ShapePoint obj_p) : base(obj_p)
        {
            r = obj.r;
        }
        public override void Draw(Graphics obj, Pen pen, Color col)
        {
            Brush br = new SolidBrush(col);
            obj.DrawEllipse(pen, 75, 25, 250, 250);
            obj.FillEllipse(br, 75, 25, 250, 250);
        }
    }
}
