﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace FormClassLibrary
{
    public abstract class ShapePoint
    {
        protected int x1;
        public int X1
        {
            get { return x1; }
            set { if (value > 0) x1 = value; else x1 = 100; }
        }
        protected int y1;
        public int Y1
        {
            get { return y1; }
            set { if (value > 0) y1 = value; else y1 = 100; }
        }
        protected Color shapeColor;
        public Color C
        {
            get { return shapeColor; }
            set { shapeColor = value; }
        }
        public ShapePoint()
        {
            x1 = 100;
            y1 = 100;
            shapeColor = Color.Black;
        }
        public ShapePoint(int _x, int _y, Color _color)
        {
            x1 = _x;
            y1 = _y;
            shapeColor = _color;
        }
        public ShapePoint(ShapePoint obj)
        {
            x1 = obj.x1;
            y1 = obj.y1;
            shapeColor = obj.shapeColor;
        }
        public virtual void Draw(Graphics obj, Pen pen, Color col)
        {

        }
    }
}