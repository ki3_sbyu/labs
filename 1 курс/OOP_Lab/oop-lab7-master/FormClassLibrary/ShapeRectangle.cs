﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace FormClassLibrary
{
    public class ShapeRectangle : ShapePoint
    {
        protected int height;
        public int Height
        {
            get { return height; }
            set { if (value > 0) height = value; else value = 200; }
        }
        protected int width;
        public int Width
        {
            get { return width; }
            set { if (value > 0) width = value; else width = 150; }
        }
        public ShapeRectangle() : base()
        {
            height = 200;
            width = 150;
        }
        public ShapeRectangle(int _x1, int _y1, int _height, int _width, Color _color) : base(_x1, _y1, _color)
        {
            height = _height;
            width = _width;
        }
        public ShapeRectangle(ShapeRectangle obj, ShapePoint obj_p) : base(obj_p)
        {
            height = obj.height;
            width = obj.width;
        }
        public override void Draw(Graphics obj, Pen pen, Color col)
        {
            Brush br = new SolidBrush(col);
            obj.DrawRectangle(pen, 50, 50, 300, 200);
            obj.FillRectangle(br, 50, 50, 300, 200);
        }
    }
}
