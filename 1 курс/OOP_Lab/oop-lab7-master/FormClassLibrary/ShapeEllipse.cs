﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace FormClassLibrary
{
    public class ShapeEllipse : ShapeRectangle
    {
        public ShapeEllipse() : base()
        {

        }
        public ShapeEllipse(ShapeEllipse obj, ShapeRectangle obj_r, ShapePoint obj_p) : base(obj_r, obj_p)
        {

        }
        public ShapeEllipse(int _x1, int _y1, int _height, int _width, Color _color) : base(_x1, _y1, _height, _width, _color)
        {

        }
        public override void Draw(Graphics obj, Pen pen, Color col)
        {
            Brush br = new SolidBrush(col);
            obj.DrawEllipse(pen, 50, 50, 300, 180);
            obj.FillEllipse(br, 50, 50, 300, 180);
        }
    }
}
