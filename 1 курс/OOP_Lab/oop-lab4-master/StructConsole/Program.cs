﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructConsole
{
    class Program
    {
        
        static void Main(string[] args)
        {
            Console.Title= "Лабораторна робота №4";
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.OutputEncoding = System.Text.Encoding.Default;
            int Count, menu, big=0, little=0;
            do
                {
                    Console.WriteLine("Введіть кількість нових записів: ");
                    isOk = int.TryParse(Console.ReadLine(), out Count);
                    if (isOk != true)
                    {
                        PrintError();
                    }
                } while (isOk != true);
            Airplane[] airplane = new Airplane[Count];
            do {
                Console.WriteLine("Меню");
                Console.WriteLine("1.Записати дані в пам'ять." +
                    "\n2.Вивести дані на екран.\n3.Вивести літак з найдовшим часом та найкоротшим часом подорожі.\n" +
                    "4.Сортувати дані за спаданням дати відправлення.\n5.Сортувати дані за за зростанням часу подорожі.\n0.Вихід.");
                do
                {
                    Console.WriteLine("==>");
                    isOk = int.TryParse(Console.ReadLine(), out menu);
                    if (menu > 5 || menu < 0)
                    {
                        PrintError();
                        isOk = false;
                    }
                } while (isOk != true);
                switch (menu)
                {
                    case 1:
                        {
                            ReadAirplaneArray(airplane);
                            break;
                        }
                    case 2:
                        {
                            PrintAirplanes(airplane);
                            break;
                        }
                    case 3:
                        {
                            GetAirplaneInfo(airplane, out little,out  big);
                            Console.WriteLine("Мінімальний час:");
                            PrintAirplane(airplane[little]);
                            Console.WriteLine("Максимальний час:");
                            PrintAirplane(airplane[big]);
                            break;
                        }
                    case 4:
                        {
                            SortAirplanesByDate(airplane);
                            Console.Clear();
                            break;
                        }
                    case 5:
                        {
                            SortAirplanesByTotalTime(airplane);
                            Console.Clear();
                            break;
                        }
                }
            } while (menu != 0);
        }
        enum Mon
        {
            Yanuar = 1,
            Februar,
            March,
            April,
            May,
            June,
            July,
            August,
            September,
            October,
            November,
            December
        }
        public struct Airplane
        {
            private string StartCity;
            private string FinishCity;
            private Date StartDate;
            private Date FinishDate;
            public Airplane(string startCity, string finishCity, Date startDate, Date finishDate)
            {
                StartCity = startCity;
                FinishCity = finishCity;
                StartDate = startDate;
                FinishDate = finishDate;
            }
            public int GetTotalTime(Date StartDate, Date FinishDate)
            {
                int resYear = 0, resMonth = 0, resDay = 0, resHours = 0, resMinutes = 0;
                int mulDays = 30;
                if (StartDate.Year <= FinishDate.Year)
                {
                    resYear = FinishDate.Year - StartDate.Year;
                    resMonth = FinishDate.Month - StartDate.Month;
                    resDay = FinishDate.Day - StartDate.Day;
                    resHours = FinishDate.Hours - StartDate.Hours;
                    resMinutes = FinishDate.Minutes - StartDate.Minutes + resHours * 60 + resDay * 24 * 60 + resMonth * mulDays * 24 * 60 + resYear * 12 * mulDays * 24 * 60;
                    return resMinutes;
                }
                else return 0;
            }
            public int IsArrivingToday(Date StartDate, Date FinishDate)
            {
                if (StartDate.Month == FinishDate.Month && StartDate.Year == FinishDate.Year
                    && StartDate.Day == FinishDate.Day)
                {
                    return 1;
                }
                else return 0;
            }
            public string GetDate()
            {
                string result="";
                result += StartDate.Year+StartDate.Month+StartDate.Day;
                return result;
            }

        }
        public struct Date
        {
            public int year;
            public int month;
            public int day;
            public int hours;
            public int minutes;

            public Date(int year, int month, int day, int hours, int minutes)
            {
                this.year = year;
                this.month = month;
                this.day = day;
                this.hours = hours;
                this.minutes = minutes;
            }
        }
        public static string CheckDig(string Elem)
        {
            string No = "-1";
            for(int i=0;i<Elem.Length;i++)
            {
                if(char.IsLetter(Elem, i))
                {
                    PrintError();
                    return No;
                }
            }
            return Elem;
        }
        public static string CheckNotDig(string Elem)
        {
            string No = "-1";
            for (int i = 0; i < Elem.Length; i++)
                if (!char.IsLetter(Elem, i))
            {
                PrintError();
                return No;
            }
            return Elem;
        }
        public static Airplane ReadAirplane()
        {
            Airplane database;
            do
            {
                Console.WriteLine("\nПочаток маршруту:");
                database.StartCity = CheckNotDig(Console.ReadLine());
                isOk = Int32.TryParse(database.StartCity,out number);
            } while (isOk != false);
            do
            {
                Console.WriteLine("\nКінець маршруту:");
                database.FinishCity = CheckNotDig(Console.ReadLine());
                isOk = Int32.TryParse(database.FinishCity, out number);
            }
            while (isOk != false);
            Console.WriteLine("|| Виліт ||");
            do
            {
                Console.WriteLine("Рік:"); 
                database.StartDate.Year = Convert.ToInt32(CheckDig(Console.ReadLine()));//start year
                if (database.StartDate.Year > 9999 || database.StartDate.Year < 1000)
                    database.StartDate.Year = -1;
            } while (database.StartDate.Year == -1);

            do
            {
                Console.WriteLine("Місяць:");
                database.StartDate.Month = Convert.ToInt32(CheckDig(Console.ReadLine()));//start month
                if (database.StartDate.Month < 1 || database.StartDate.Month > 12)
                    database.StartDate.Month = -1;
            } while (database.StartDate.Month == -1);

            do
            {
                Console.WriteLine("День:");
                database.StartDate.Day = Convert.ToInt32(CheckDig(Console.ReadLine()));//start day
                if (database.StartDate.Day > 31 || database.StartDate.Day < 1)
                    database.StartDate.Day = -1;
            } while (database.StartDate.Day == -1);
            do
            {
                Console.WriteLine("Години:");
                database.StartDate.Hours = Convert.ToInt32(CheckDig(Console.ReadLine()));//start hours
                if (database.StartDate.Hours > 24 || database.StartDate.Hours < 0)
                    database.StartDate.Hours = -1;
            } while (database.StartDate.Hours == -1);

            do
            {
                Console.WriteLine("Хвилини:");
                database.StartDate.Minutes = Convert.ToInt32(CheckDig(Console.ReadLine()));// start minutes
                if (database.StartDate.Minutes > 60 || database.StartDate.Minutes < 0)
                    database.StartDate.Minutes = -1;
            } while (database.StartDate.Minutes == -1);
            Console.WriteLine("|| Прибуття ||");
            do {
                Console.WriteLine("Рік:");
                database.FinishDate.Year = Convert.ToInt32(CheckDig(Console.ReadLine()));// finish year
                if (database.FinishDate.Year > 9999 || database.FinishDate.Year < database.StartDate.Year || database.FinishDate.Year < 1000)
                    database.FinishDate.Year = -1;
            } while (database.FinishDate.Year == -1);
            do
            {
                Console.WriteLine("Місяць:");
                database.FinishDate.Month = Convert.ToInt32(CheckDig(Console.ReadLine()));// finish month
                if (database.FinishDate.Month > 12 || database.FinishDate.Month < 1)
                    database.FinishDate.Month = -1;
            } while (database.FinishDate.Month == -1);
            do {
                Console.WriteLine("День:");
                database.FinishDate.Day = Convert.ToInt32(CheckDig(Console.ReadLine()));// finish day
                if (database.FinishDate.Day > 31 || database.FinishDate.Day < 1)
                    database.FinishDate.Day = -1;
            } while (database.FinishDate.Day == -1);

            do {
                Console.WriteLine("Години:");
                database.FinishDate.Hours = Convert.ToInt32(CheckDig(Console.ReadLine()));// finish hours
                if (database.FinishDate.Hours > 24 || database.FinishDate.Hours < 0)
                    database.FinishDate.Hours = -1;
            } while (database.FinishDate.Hours == -1);

            do
            {
                Console.WriteLine("Хвилини:");
                database.FinishDate.Minutes = Convert.ToInt32(CheckDig(Console.ReadLine())); // finish minutes
                if (database.FinishDate.Minutes > 60 || database.FinishDate.Minutes < 0)
                    database.FinishDate.Minutes = -1;
            } while (database.FinishDate.Minutes == -1);
            Console.Clear();

            return database;
        }
        public static void ReadAirplaneArray(Airplane []airplane)
        {
            for(int i=0;i<airplane.Length;i++)
            {
                airplane[i] = ReadAirplane();
            }
        }
        public static void PrintHead()
        {
            Console.WriteLine($"{"Маршрут",-31} {"Дата вил. ",10} {"Час",5} {"Дата приб. ", 10} {"Час", 5}, {"Сьогодні?", 10}, {"Час польоту в хвилинах", 10}");
        }
        public static void PrintAirplane(Airplane airplane)
        {
            string YesNo;
            if (airplane.IsArrivingToday(airplane.StartDate, airplane.FinishDate) == 0) YesNo = "Ні";
            else YesNo = "Так";
            Console.WriteLine($"{airplane.StartCity, 15}-{airplane.FinishCity, -15} {airplane.StartDate.Day,2}.{airplane.StartDate.Month,2}." +
                $"{airplane.StartDate.Year,4} {airplane.StartDate.Hours,2}:{airplane.StartDate.Minutes,2} {airplane.FinishDate.Day,2}.{airplane.FinishDate.Month,2}." +
                $"{airplane.FinishDate.Year,-4}, {airplane.FinishDate.Hours, 2}:{airplane.FinishDate.Minutes, -2}," +
                $" {YesNo,10}, {airplane.GetTotalTime(airplane.StartDate, airplane.FinishDate), 10}");
        }
        public static void PrintAirplanes(Airplane []airplane)
        {
            PrintHead();
            for(int i=0;i<airplane.Length;i++)
            {
                PrintAirplane(airplane[i]);
            }
        }
        public static void PrintError()
        {
            Console.WriteLine("\nПомилка. Спробуйте ввести дані ще раз.\n");
        }
        public static void GetAirplaneInfo(Airplane[] airplane, out int little, out int big)
        {
            little = GetMin(airplane);
            big = GetMax(airplane);
        }
        public static int GetMin(Airplane []airplane)
        {
            int min=0;
            for(int i = airplane.Length - 1; i > 0; i--)
            {
                if (i != 0 && (airplane[i].GetTotalTime(airplane[i].StartDate, airplane[i].FinishDate) <= airplane[i].GetTotalTime(airplane[i - 1].StartDate, airplane[i - 1].FinishDate)))
                {
                    min = i;
                }
            }
            return min;
        }
        public static int GetMax(Airplane[] airplane)
        {
            int max = 0;
            for (int i = airplane.Length-1; i > 0; i--)
            {
                if (i !=0 && (airplane[i].GetTotalTime(airplane[i].StartDate, airplane[i].FinishDate) >= airplane[i].GetTotalTime(airplane[i - 1].StartDate, airplane[i - 1].FinishDate)))
                {
                    max = i;
                }
            }
            return max;
        }
        public static int SortInfoAirplanesByDate(Airplane a, Airplane b)
        {
            int num1 = Convert.ToInt32(a.GetDate()), num2 = Convert.ToInt32(b.GetDate());
            if (num1 > num2)
                return -1;
            if (num1 < num2)
                return 1;
            return 0;
        }
        public static void SortAirplanesByDate(Airplane []airplane)
        {
            Array.Sort(airplane, SortInfoAirplanesByDate);
        }
        public static int SortInfoAirplanesByTotalTime(Airplane a, Airplane b)
        {
            int num1 = a.GetTotalTime(a.StartDate, a.FinishDate), num2 = b.GetTotalTime(b.StartDate, b.FinishDate);
            if (num1 > num2)
                return 1;
            if (num1 < num2)
                return -1;
            return 0;
        }
        public static void SortAirplanesByTotalTime(Airplane [] airplane)
        {
            Array.Sort(airplane, SortInfoAirplanesByTotalTime);
        }
        public static void UnError()
        {
            Console.WriteLine("Невідома помилка...");
        }
        public static bool isOk = false;
        public static int number;
    }
}
