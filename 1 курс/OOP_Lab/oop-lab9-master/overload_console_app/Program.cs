﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lab9_ClassLibrary;

namespace overload_console_app
{
    class Program
    {
        public static void TrueFalse(bool value)
        {
            if (value == true)
                Console.Write("Правда.");
            else Console.Write("Брехня.");
        }
        public static int GetNum(string num)
        {
            int res = 0;
            while (true)
            {
                bool JustTry = Int32.TryParse(num, out res);
                if (JustTry == true) return res;
                else
                {
                    Console.Clear();
                    Console.WriteLine("Допущена помилка. Повторіть спробу:");
                    num = Console.ReadLine();
                }
            }
        }
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            string menu = "1";
            while (menu != "0")
            {
                Console.Clear();
                Console.WriteLine("Меню\n" + "1.Ввести вираз\n" + "0.Вихід");
                menu = Console.ReadLine();
                switch (menu)
                {
                    case "1":
                        {
                            Fraction res = new Fraction();
                            Fraction a = new Fraction();
                            Fraction b = new Fraction();
                            Console.WriteLine("Оберіть операцію:\n");
                            Console.Write("1. +\n2. -\n3. *\n4. /\n5. <\n6. >\n7. ==\n8. !=\n9. <=\n10. >=\n11. ++\n12. --\n");
                            string submenu = Console.ReadLine();
                            if (GetNum(submenu) >= 1 && GetNum(submenu) <= 10)
                            {

                                Console.WriteLine("Введіть чисельник і знаменник першого дробу");
                                int anum, bnum;
                                anum = GetNum(Console.ReadLine()); bnum = GetNum(Console.ReadLine());
                                a = new Fraction(anum, bnum);
                                Console.WriteLine("Другого дробу:");
                                anum = GetNum(Console.ReadLine()); bnum = GetNum(Console.ReadLine());
                                b = new Fraction(anum, bnum);
                            }
                            else
                            {
                                Console.WriteLine("Введіть чисельник і знаменник дробу");
                                int anum, bnum;
                                anum = GetNum(Console.ReadLine()); bnum = GetNum(Console.ReadLine());
                                a = new Fraction(anum, bnum);
                            }
                            Console.Write("Результат: ");
                            switch (GetNum(submenu))
                            {
                                case 1:
                                    res = a + b;
                                    break;
                                case 2:
                                    res = a - b;
                                    break;
                                case 3:
                                    res = a * b;
                                    break;
                                case 4:
                                    res = a / b;
                                    break;
                                case 5:
                                    {
                                        TrueFalse(a < b);
                                        break;
                                    }
                                case 6:
                                    {
                                        TrueFalse(a > b);
                                        break;
                                    }
                                case 10:
                                    {
                                        TrueFalse(a >= b);
                                        break;
                                    }
                                case 9:
                                    {
                                        TrueFalse(a <= b);
                                        break;
                                    }
                                case 7:
                                    {
                                        TrueFalse(a == b);
                                        break;
                                    }
                                case 8:
                                    {
                                        TrueFalse(a != b);
                                        break;
                                    }
                                case 12:
                                    a--;
                                    res = a;
                                    break;
                                case 11:
                                    a++;
                                    res = a;
                                    break;
                                default:
                                    Console.Clear();
                                    break;
                            }

                            if (GetNum(submenu) >= 1 && GetNum(submenu) <= 4 || GetNum(submenu) == 11 || GetNum(submenu) == 12)
                            {
                               
                                Console.WriteLine(Fraction.CutDown(res).ToString() + ", або " + $"{Math.Round(Fraction.GetDouble(res), 3)}");
                            }
                            Console.Read();
                        }
                        break;
                    case "0":break;
                    default:
                        {
                            Console.Clear();
                            Console.WriteLine("Помилка. Спробуйте ще раз.");
                            break;
                        }
                }
            }
        }
    }
}
