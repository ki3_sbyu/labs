﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab9_ClassLibrary
{
    public class Fraction
    {
        protected long Numer;
        protected long Denom;

        public Fraction(long num, long denom)
        {
            Numer = num;
            Denom = denom;
        }
        public Fraction()
        {
            Numer = 1;
            Denom = 1;
        }
        public Fraction(Fraction a)
        {
            this.Denom = a.Denom;
            this.Numer = a.Numer;
        }
        public Fraction(long num)

        {
            Numer = num;
            Denom = num;
        }

        public static Fraction CutDown(Fraction a)
        {
            Fraction res = new Fraction();
            long maxVal = 0;
            if (a.Denom <= a.Numer)
                maxVal = a.Numer;
            else maxVal = a.Denom;
            for (int check =0;check<maxVal/4;check++)
            for(int i=1;i<maxVal/2;i++)
            {
                if(a.Numer%i==0 && a.Denom%i==0)
                {
                    res.Numer = a.Numer / i;
                    res.Denom = a.Denom / i;
                }
            }
            return res;
        }
        public static double GetDouble(Fraction a)
        {
            double res = (double)a.Numer / (double)a.Denom;
            return res;
        }
        public static Fraction operator +(Fraction a, Fraction b)
        {
            Fraction res = new Fraction(a.Numer * b.Denom + a.Denom * b.Numer, a.Denom * b.Denom);
            return res;
        }
        public static Fraction operator -(Fraction a, Fraction b)
        {
            Fraction res = new Fraction(a.Numer * b.Denom - a.Denom * b.Numer, a.Denom * b.Denom);
            return res;
        }
        public static Fraction operator *(Fraction a, Fraction b)
        {
            Fraction res = new Fraction(a.Numer * b.Numer, a.Denom * b.Denom);
            return res;
        }
        public static Fraction operator /(Fraction a, Fraction b)
        {
            Fraction res = new Fraction(a.Numer * b.Denom, b.Numer * a.Denom);
            return res;
        }
        public static Fraction operator ++(Fraction a)
        {
            Fraction res = new Fraction(a.Numer + a.Denom , a.Denom);
            return res;
        }
        public static Fraction operator --(Fraction a)
        {
            Fraction res = new Fraction(a.Numer - a.Denom, a.Denom);
            return res;
        }
        public static bool operator < (Fraction a, Fraction b)
        {
            if (a.Numer*b.Denom<b.Numer*a.Denom)
                return true;
            else return false;
        }
        public static bool operator > (Fraction a, Fraction b)
        {
            if (a.Numer * b.Denom > b.Numer * a.Denom)
                return true;
            else return false;
        }
        public static bool operator == (Fraction a, Fraction b)
        {
            if (a.Numer*b.Denom == b.Numer*a.Denom)
                return true;
            else return false;
        }
        public static bool operator != (Fraction a, Fraction b)
        {
            if (a.Numer * b.Denom != b.Numer * a.Denom) return true;
            else return false;
        }
        public static bool operator <= (Fraction a, Fraction b)
        {
            if (a.Numer * b.Denom <= b.Numer * a.Denom)
                return true;
            else return false;
        }
        public static bool operator >=(Fraction a, Fraction b)
        {
            if (a.Numer * b.Denom >= b.Numer * a.Denom)
                return true;
            else return false;
        }
        public override string ToString()
        {
            string s;
            Fraction a = new Fraction(Numer, Denom);
            a = CutDown(a);
            s = a.Numer + "/" + a.Denom;
            return s;
        }
    }
}
