﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravelForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            comboBoxCountry.SelectedIndex = 0;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            #region Ціни
            int bolSum = 100, bolWin = 150, gerSum = 160, gerWin = 200, polSum = 120, polWin = 180, costGid = 50;
            #endregion
            int days, bilets, mul, check = 1;
            bool isOkD = Int32.TryParse(daysTextBox.Text,out days);
            bool isOkB = Int32.TryParse(textBoxValueOfBilets.Text, out bilets);
            if(!isOkD||days<0)
            {
                MessageBox.Show("Помилка. Неправильно введені дні.");
                textBoxCost.Text = "0";
                daysTextBox.Text = "0";
                check = 0;
            }
            if(!isOkB || bilets < 0)
            {
                MessageBox.Show("Помилка. Неправильно введені путівки.");
                textBoxCost.Text = "0";
                textBoxValueOfBilets.Text = "0";
                check = 0;
            }
            if (check == 1)
            {
                mul = days * bilets;
                if (radioSommer.Checked == true)
                {
                    switch (comboBoxCountry.SelectedIndex)
                    {
                        case 0:
                            {
                                textBoxCost.Text = Convert.ToString(mul * bolSum);
                                break;
                            }
                        case 1:
                            {
                                textBoxCost.Text = Convert.ToString(mul * gerSum);
                                break;
                            }
                        case 2:
                            {
                                textBoxCost.Text = Convert.ToString(mul * polSum);
                                break;
                            }
                    }
                }
                else if (radioWinter.Checked)
                {
                    switch (comboBoxCountry.SelectedIndex)
                    {
                        case 0:
                            {
                                textBoxCost.Text = Convert.ToString(mul * bolWin);
                                break;
                            }
                        case 1:
                            {
                                textBoxCost.Text = Convert.ToString(mul * gerWin);
                                break;
                            }
                        case 2:
                            {
                                textBoxCost.Text = Convert.ToString(mul * polWin);
                                break;
                            }
                    }
                }
                if (buyGid.Checked == true)
                {
                    textBoxCost.Text = Convert.ToString(Convert.ToInt32(textBoxCost.Text)+costGid*mul);
                }
            }
        }
    }
}
