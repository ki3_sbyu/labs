﻿namespace TravelForm
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelDays = new System.Windows.Forms.Label();
            this.daysTextBox = new System.Windows.Forms.TextBox();
            this.countryLabel = new System.Windows.Forms.Label();
            this.comboBoxCountry = new System.Windows.Forms.ComboBox();
            this.radioWinter = new System.Windows.Forms.RadioButton();
            this.radioSommer = new System.Windows.Forms.RadioButton();
            this.buyGid = new System.Windows.Forms.CheckBox();
            this.labelValueOfBilets = new System.Windows.Forms.Label();
            this.textBoxValueOfBilets = new System.Windows.Forms.TextBox();
            this.buttonCalculate = new System.Windows.Forms.Button();
            this.costLabel = new System.Windows.Forms.Label();
            this.textBoxCost = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // labelDays
            // 
            this.labelDays.AutoSize = true;
            this.labelDays.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDays.Location = new System.Drawing.Point(12, 21);
            this.labelDays.Name = "labelDays";
            this.labelDays.Size = new System.Drawing.Size(118, 20);
            this.labelDays.TabIndex = 0;
            this.labelDays.Text = "Кількість днів:";
            // 
            // daysTextBox
            // 
            this.daysTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.daysTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.daysTextBox.Location = new System.Drawing.Point(158, 15);
            this.daysTextBox.Name = "daysTextBox";
            this.daysTextBox.Size = new System.Drawing.Size(112, 26);
            this.daysTextBox.TabIndex = 1;
            this.daysTextBox.Text = "0";
            this.daysTextBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // countryLabel
            // 
            this.countryLabel.AutoSize = true;
            this.countryLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.countryLabel.Location = new System.Drawing.Point(12, 107);
            this.countryLabel.Name = "countryLabel";
            this.countryLabel.Size = new System.Drawing.Size(62, 20);
            this.countryLabel.TabIndex = 2;
            this.countryLabel.Text = "Країна:";
            // 
            // comboBoxCountry
            // 
            this.comboBoxCountry.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBoxCountry.FormattingEnabled = true;
            this.comboBoxCountry.Items.AddRange(new object[] {
            "Болгарія",
            "Німеччина",
            "Польща"});
            this.comboBoxCountry.Location = new System.Drawing.Point(158, 104);
            this.comboBoxCountry.Name = "comboBoxCountry";
            this.comboBoxCountry.Size = new System.Drawing.Size(112, 28);
            this.comboBoxCountry.TabIndex = 3;
            // 
            // radioWinter
            // 
            this.radioWinter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioWinter.AutoSize = true;
            this.radioWinter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.radioWinter.Location = new System.Drawing.Point(298, 64);
            this.radioWinter.Name = "radioWinter";
            this.radioWinter.Size = new System.Drawing.Size(67, 24);
            this.radioWinter.TabIndex = 4;
            this.radioWinter.Text = "Зима";
            this.radioWinter.UseVisualStyleBackColor = true;
            // 
            // radioSommer
            // 
            this.radioSommer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioSommer.AutoSize = true;
            this.radioSommer.Checked = true;
            this.radioSommer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.radioSommer.Location = new System.Drawing.Point(298, 19);
            this.radioSommer.Name = "radioSommer";
            this.radioSommer.Size = new System.Drawing.Size(60, 24);
            this.radioSommer.TabIndex = 5;
            this.radioSommer.TabStop = true;
            this.radioSommer.Text = "Літо";
            this.radioSommer.UseVisualStyleBackColor = true;
            // 
            // buyGid
            // 
            this.buyGid.AccessibleDescription = "";
            this.buyGid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buyGid.AutoSize = true;
            this.buyGid.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buyGid.Location = new System.Drawing.Point(298, 107);
            this.buyGid.Name = "buyGid";
            this.buyGid.Size = new System.Drawing.Size(130, 22);
            this.buyGid.TabIndex = 6;
            this.buyGid.Text = "Замовити гіда";
            this.buyGid.UseVisualStyleBackColor = true;
            // 
            // labelValueOfBilets
            // 
            this.labelValueOfBilets.AutoSize = true;
            this.labelValueOfBilets.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelValueOfBilets.Location = new System.Drawing.Point(12, 66);
            this.labelValueOfBilets.Name = "labelValueOfBilets";
            this.labelValueOfBilets.Size = new System.Drawing.Size(140, 20);
            this.labelValueOfBilets.TabIndex = 7;
            this.labelValueOfBilets.Text = "Кількість путівок:";
            // 
            // textBoxValueOfBilets
            // 
            this.textBoxValueOfBilets.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxValueOfBilets.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxValueOfBilets.Location = new System.Drawing.Point(158, 63);
            this.textBoxValueOfBilets.Name = "textBoxValueOfBilets";
            this.textBoxValueOfBilets.Size = new System.Drawing.Size(112, 26);
            this.textBoxValueOfBilets.TabIndex = 8;
            this.textBoxValueOfBilets.Text = "0";
            // 
            // buttonCalculate
            // 
            this.buttonCalculate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCalculate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonCalculate.Location = new System.Drawing.Point(298, 168);
            this.buttonCalculate.Name = "buttonCalculate";
            this.buttonCalculate.Size = new System.Drawing.Size(130, 32);
            this.buttonCalculate.TabIndex = 9;
            this.buttonCalculate.Text = "Обрахувати";
            this.buttonCalculate.UseVisualStyleBackColor = true;
            this.buttonCalculate.Click += new System.EventHandler(this.buttonCalculate_Click);
            // 
            // costLabel
            // 
            this.costLabel.AutoSize = true;
            this.costLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.costLabel.ForeColor = System.Drawing.Color.Maroon;
            this.costLabel.Location = new System.Drawing.Point(13, 174);
            this.costLabel.Name = "costLabel";
            this.costLabel.Size = new System.Drawing.Size(89, 20);
            this.costLabel.TabIndex = 10;
            this.costLabel.Text = "Вартість:";
            // 
            // textBoxCost
            // 
            this.textBoxCost.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxCost.Location = new System.Drawing.Point(158, 171);
            this.textBoxCost.Name = "textBoxCost";
            this.textBoxCost.ReadOnly = true;
            this.textBoxCost.Size = new System.Drawing.Size(112, 26);
            this.textBoxCost.TabIndex = 11;
            this.textBoxCost.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(434, 211);
            this.Controls.Add(this.textBoxCost);
            this.Controls.Add(this.costLabel);
            this.Controls.Add(this.buttonCalculate);
            this.Controls.Add(this.textBoxValueOfBilets);
            this.Controls.Add(this.labelValueOfBilets);
            this.Controls.Add(this.buyGid);
            this.Controls.Add(this.radioSommer);
            this.Controls.Add(this.radioWinter);
            this.Controls.Add(this.comboBoxCountry);
            this.Controls.Add(this.countryLabel);
            this.Controls.Add(this.daysTextBox);
            this.Controls.Add(this.labelDays);
            this.MaximumSize = new System.Drawing.Size(600, 250);
            this.MinimumSize = new System.Drawing.Size(450, 250);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelDays;
        private System.Windows.Forms.TextBox daysTextBox;
        private System.Windows.Forms.Label countryLabel;
        private System.Windows.Forms.ComboBox comboBoxCountry;
        private System.Windows.Forms.RadioButton radioWinter;
        private System.Windows.Forms.RadioButton radioSommer;
        private System.Windows.Forms.CheckBox buyGid;
        private System.Windows.Forms.Label labelValueOfBilets;
        private System.Windows.Forms.TextBox textBoxValueOfBilets;
        private System.Windows.Forms.Button buttonCalculate;
        private System.Windows.Forms.Label costLabel;
        private System.Windows.Forms.TextBox textBoxCost;
    }
}

