﻿namespace GlazedForms
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.sizeOfWindowLabel = new System.Windows.Forms.Label();
            this.hidthLabel = new System.Windows.Forms.Label();
            this.heightLabel = new System.Windows.Forms.Label();
            this.costlabel = new System.Windows.Forms.Label();
            this.materialLabel = new System.Windows.Forms.Label();
            this.glazedWindowLabel = new System.Windows.Forms.Label();
            this.heightTextBox = new System.Windows.Forms.TextBox();
            this.widthTextBox = new System.Windows.Forms.TextBox();
            this.comboBoxMaterial = new System.Windows.Forms.ComboBox();
            this.costTextBox = new System.Windows.Forms.TextBox();
            this.radioOneChamber = new System.Windows.Forms.RadioButton();
            this.radioTwoChamber = new System.Windows.Forms.RadioButton();
            this.checkWindowsill = new System.Windows.Forms.CheckBox();
            this.buttonCalculate = new System.Windows.Forms.Button();
            this.hrnLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // sizeOfWindowLabel
            // 
            this.sizeOfWindowLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sizeOfWindowLabel.AutoSize = true;
            this.sizeOfWindowLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sizeOfWindowLabel.Location = new System.Drawing.Point(30, 11);
            this.sizeOfWindowLabel.Name = "sizeOfWindowLabel";
            this.sizeOfWindowLabel.Size = new System.Drawing.Size(128, 20);
            this.sizeOfWindowLabel.TabIndex = 0;
            this.sizeOfWindowLabel.Text = "Розміри вікна:";
            this.sizeOfWindowLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // hidthLabel
            // 
            this.hidthLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.hidthLabel.AutoSize = true;
            this.hidthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.hidthLabel.Location = new System.Drawing.Point(30, 42);
            this.hidthLabel.Name = "hidthLabel";
            this.hidthLabel.Size = new System.Drawing.Size(104, 20);
            this.hidthLabel.TabIndex = 1;
            this.hidthLabel.Text = "Ширина (см):";
            // 
            // heightLabel
            // 
            this.heightLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.heightLabel.AutoSize = true;
            this.heightLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.heightLabel.Location = new System.Drawing.Point(30, 78);
            this.heightLabel.Name = "heightLabel";
            this.heightLabel.Size = new System.Drawing.Size(97, 20);
            this.heightLabel.TabIndex = 2;
            this.heightLabel.Text = "Висота(см):";
            // 
            // costlabel
            // 
            this.costlabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.costlabel.AutoSize = true;
            this.costlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.costlabel.Location = new System.Drawing.Point(30, 178);
            this.costlabel.Name = "costlabel";
            this.costlabel.Size = new System.Drawing.Size(89, 20);
            this.costlabel.TabIndex = 3;
            this.costlabel.Text = "Вартість:";
            // 
            // materialLabel
            // 
            this.materialLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.materialLabel.AutoSize = true;
            this.materialLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.materialLabel.Location = new System.Drawing.Point(30, 117);
            this.materialLabel.Name = "materialLabel";
            this.materialLabel.Size = new System.Drawing.Size(84, 20);
            this.materialLabel.TabIndex = 4;
            this.materialLabel.Text = "Матеріал:";
            this.materialLabel.Click += new System.EventHandler(this.materialLabel_Click);
            // 
            // glazedWindowLabel
            // 
            this.glazedWindowLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.glazedWindowLabel.AutoSize = true;
            this.glazedWindowLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.glazedWindowLabel.Location = new System.Drawing.Point(308, 11);
            this.glazedWindowLabel.Name = "glazedWindowLabel";
            this.glazedWindowLabel.Size = new System.Drawing.Size(105, 20);
            this.glazedWindowLabel.TabIndex = 5;
            this.glazedWindowLabel.Text = "Склопакет:";
            // 
            // heightTextBox
            // 
            this.heightTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.heightTextBox.Location = new System.Drawing.Point(133, 80);
            this.heightTextBox.Name = "heightTextBox";
            this.heightTextBox.Size = new System.Drawing.Size(122, 20);
            this.heightTextBox.TabIndex = 6;
            this.heightTextBox.Text = "0";
            // 
            // widthTextBox
            // 
            this.widthTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.widthTextBox.Location = new System.Drawing.Point(133, 42);
            this.widthTextBox.Name = "widthTextBox";
            this.widthTextBox.Size = new System.Drawing.Size(122, 20);
            this.widthTextBox.TabIndex = 7;
            this.widthTextBox.Text = "0";
            this.widthTextBox.TextChanged += new System.EventHandler(this.widthTextBox_TextChanged);
            // 
            // comboBoxMaterial
            // 
            this.comboBoxMaterial.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMaterial.Cursor = System.Windows.Forms.Cursors.Hand;
            this.comboBoxMaterial.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMaterial.FormattingEnabled = true;
            this.comboBoxMaterial.Items.AddRange(new object[] {
            "Дерево",
            "Метал",
            "Металопластик"});
            this.comboBoxMaterial.Location = new System.Drawing.Point(133, 116);
            this.comboBoxMaterial.Name = "comboBoxMaterial";
            this.comboBoxMaterial.Size = new System.Drawing.Size(122, 21);
            this.comboBoxMaterial.TabIndex = 8;
            // 
            // costTextBox
            // 
            this.costTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.costTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.costTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.costTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.costTextBox.Location = new System.Drawing.Point(133, 180);
            this.costTextBox.Name = "costTextBox";
            this.costTextBox.ReadOnly = true;
            this.costTextBox.Size = new System.Drawing.Size(122, 19);
            this.costTextBox.TabIndex = 9;
            this.costTextBox.TextChanged += new System.EventHandler(this.costTextBox_TextChanged);
            // 
            // radioOneChamber
            // 
            this.radioOneChamber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioOneChamber.AutoSize = true;
            this.radioOneChamber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.radioOneChamber.Location = new System.Drawing.Point(312, 40);
            this.radioOneChamber.Name = "radioOneChamber";
            this.radioOneChamber.Size = new System.Drawing.Size(141, 24);
            this.radioOneChamber.TabIndex = 10;
            this.radioOneChamber.TabStop = true;
            this.radioOneChamber.Text = "Однокамерний";
            this.radioOneChamber.UseVisualStyleBackColor = true;
            // 
            // radioTwoChamber
            // 
            this.radioTwoChamber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioTwoChamber.AutoSize = true;
            this.radioTwoChamber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.radioTwoChamber.Location = new System.Drawing.Point(312, 76);
            this.radioTwoChamber.Name = "radioTwoChamber";
            this.radioTwoChamber.Size = new System.Drawing.Size(130, 24);
            this.radioTwoChamber.TabIndex = 11;
            this.radioTwoChamber.TabStop = true;
            this.radioTwoChamber.Text = "Двокамерний";
            this.radioTwoChamber.UseVisualStyleBackColor = true;
            this.radioTwoChamber.CheckedChanged += new System.EventHandler(this.radioTwoChamber_CheckedChanged);
            // 
            // checkWindowsill
            // 
            this.checkWindowsill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkWindowsill.AutoSize = true;
            this.checkWindowsill.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkWindowsill.Location = new System.Drawing.Point(312, 116);
            this.checkWindowsill.Name = "checkWindowsill";
            this.checkWindowsill.Size = new System.Drawing.Size(110, 24);
            this.checkWindowsill.TabIndex = 12;
            this.checkWindowsill.Text = "Підвіконня";
            this.checkWindowsill.UseVisualStyleBackColor = true;
            // 
            // buttonCalculate
            // 
            this.buttonCalculate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCalculate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonCalculate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonCalculate.Location = new System.Drawing.Point(312, 170);
            this.buttonCalculate.Name = "buttonCalculate";
            this.buttonCalculate.Size = new System.Drawing.Size(141, 36);
            this.buttonCalculate.TabIndex = 13;
            this.buttonCalculate.Text = "Розрахувати";
            this.buttonCalculate.UseVisualStyleBackColor = true;
            this.buttonCalculate.Click += new System.EventHandler(this.buttonCalculate_Click);
            // 
            // hrnLabel
            // 
            this.hrnLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.hrnLabel.AutoSize = true;
            this.hrnLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.hrnLabel.Location = new System.Drawing.Point(261, 183);
            this.hrnLabel.Name = "hrnLabel";
            this.hrnLabel.Size = new System.Drawing.Size(27, 13);
            this.hrnLabel.TabIndex = 14;
            this.hrnLabel.Text = "грн";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 221);
            this.Controls.Add(this.hrnLabel);
            this.Controls.Add(this.buttonCalculate);
            this.Controls.Add(this.checkWindowsill);
            this.Controls.Add(this.radioTwoChamber);
            this.Controls.Add(this.radioOneChamber);
            this.Controls.Add(this.costTextBox);
            this.Controls.Add(this.comboBoxMaterial);
            this.Controls.Add(this.widthTextBox);
            this.Controls.Add(this.heightTextBox);
            this.Controls.Add(this.glazedWindowLabel);
            this.Controls.Add(this.materialLabel);
            this.Controls.Add(this.costlabel);
            this.Controls.Add(this.heightLabel);
            this.Controls.Add(this.hidthLabel);
            this.Controls.Add(this.sizeOfWindowLabel);
            this.MaximumSize = new System.Drawing.Size(600, 260);
            this.MinimumSize = new System.Drawing.Size(500, 260);
            this.Name = "Form1";
            this.Text = "Лабораторна робота №6";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label sizeOfWindowLabel;
        private System.Windows.Forms.Label hidthLabel;
        private System.Windows.Forms.Label heightLabel;
        private System.Windows.Forms.Label costlabel;
        private System.Windows.Forms.Label materialLabel;
        private System.Windows.Forms.Label glazedWindowLabel;
        private System.Windows.Forms.TextBox heightTextBox;
        private System.Windows.Forms.TextBox widthTextBox;
        private System.Windows.Forms.ComboBox comboBoxMaterial;
        private System.Windows.Forms.TextBox costTextBox;
        private System.Windows.Forms.RadioButton radioOneChamber;
        private System.Windows.Forms.RadioButton radioTwoChamber;
        private System.Windows.Forms.CheckBox checkWindowsill;
        private System.Windows.Forms.Button buttonCalculate;
        private System.Windows.Forms.Label hrnLabel;
    }
}

