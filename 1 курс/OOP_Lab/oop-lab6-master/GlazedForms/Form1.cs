﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GlazedForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            comboBoxMaterial.SelectedIndex = 0;
            radioOneChamber.Checked = true;
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void materialLabel_Click(object sender, EventArgs e)
        {

        }

        private void radioTwoChamber_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            double width = 0.0, height = 0.0; int check = 1;
            double costWoodOne = 0.25, costWoodTwo = 0.30;
            double costMetalOne = 0.05, costMetalTwo = 0.10;
            double costGlazedOne = 0.15, costGlazedTwo = 0.20;
            double costWindowSill = 35;
            bool isOkW = double.TryParse(widthTextBox.Text, out width);
            bool isOkH = double.TryParse(heightTextBox.Text, out height);
            if (!isOkW || width <= 0.0)
            {
                MessageBox.Show("Помилка. Неправильно введена ширина.");
                widthTextBox.Text = "0";
                costTextBox.Text = "0";
                check = 0;
            }
            if (!isOkH || height <= 0.0)
            {
                MessageBox.Show("Помилка. Неправильно введена висота.");
                heightTextBox.Text = "0";
                costTextBox.Text = "0";
                check = 0;
            }
            if (check == 1)
            {
                double area = width * height;
                if (radioOneChamber.Checked)
                {
                    switch (comboBoxMaterial.SelectedIndex)
                    {
                        case 0:
                            {
                                costTextBox.Text = Convert.ToString(area * costWoodOne);
                                break;
                            }
                        case 1:
                            {
                                costTextBox.Text = Convert.ToString(area * costMetalOne);
                                break;
                            }
                        case 2:
                            {
                                costTextBox.Text = Convert.ToString(area * costGlazedOne);
                                break;
                            }
                        default:
                            {
                                break;
                            }
                    }
                }
                if (radioTwoChamber.Checked)
                {
                    switch (comboBoxMaterial.SelectedIndex)
                    {
                        case 0:
                            {
                                costTextBox.Text = Convert.ToString(area * costWoodTwo);
                                break;
                            }
                        case 1:
                            {
                                costTextBox.Text = Convert.ToString(area * costMetalTwo);
                                break;
                            }
                        case 2:
                            {
                                costTextBox.Text = Convert.ToString(area * costGlazedTwo);
                                break;
                            }
                        default:
                            {
                                break;
                            }
                    }
                }
                if (checkWindowsill.Checked && area != 0)
                {
                    costTextBox.Text = Convert.ToString(Convert.ToDouble(costTextBox.Text) + costWindowSill);
                }
            }
        }
        private void costTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void widthTextBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}