﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicPlotter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            one();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            one();
        }
        private void one()
        {
            Graphics g = this.CreateGraphics();
            g.Clear(Color.White);
            Pen a = new Pen(Color.Blue, 1);
            Pen b = new Pen(Color.Green, 2);
            Font drawFont = new Font("Arial", 12);
            Font signatureFont = new Font("Arial", 7);
            SolidBrush drawBrush = new SolidBrush(Color.Blue);
            StringFormat drawFormat = new StringFormat();
            drawFormat.FormatFlags = StringFormatFlags.DirectionRightToLeft;
            int sizeWidth = Form1.ActiveForm.Width;
            int sizeHeight = Form1.ActiveForm.Height;
            Point center = new Point(((int)(sizeWidth / 2) - 8), (int)((sizeHeight / 2) - 19));
            //осі
            g.DrawLine(a, 10, center.Y, center.X, center.Y);
            g.DrawLine(a, center.X, center.Y, 2 * center.X - 10, center.Y);
            g.DrawLine(a, center.X, 10, center.X, center.Y);
            g.DrawLine(a, center.X, center.Y, center.X, 2 * center.Y - 10);
            //підписи осей
            g.DrawString("x", drawFont, drawBrush, new Point(2 * center.X - 5, center.Y + 10), drawFormat);
            g.DrawString("y", drawFont, drawBrush, new Point(center.X + 30, 5), drawFormat);
            g.DrawString("0", drawFont, drawBrush, new Point(center.X - 5, center.Y - 20), drawFormat);
            //стрілки
            g.DrawLine(a, center.X, 10, center.X + 5, 20);
            g.DrawLine(a, center.X, 10, center.X - 5, 20);
            g.DrawLine(a, center.X*2 - 20, center.Y+5, center.X*2-10, center.Y);
            g.DrawLine(a, center.X * 2 - 20, center.Y - 5, center.X * 2 - 10, center.Y);
            //осі
            int stepForAxes = 25;
            int lenghtShtrih = 3;
            int maxValueForAxesX = 10;
            int maxValueForAxesY = 10;
            float oneDivX = (float)maxValueForAxesX / ((float)center.X / (float)stepForAxes);
            float oneDivY = (float)maxValueForAxesY / ((float)center.Y / (float)stepForAxes);
            for (int i=center.X, j = center.X, k=1;i<2*center.X-15;j-=stepForAxes, i+=stepForAxes, k++)
            {
                g.DrawLine(a, i, center.Y - lenghtShtrih, i, center.Y + lenghtShtrih);
                g.DrawLine(a, j, center.Y - lenghtShtrih, j, center.Y + lenghtShtrih);
                if(i<2*center.X-40)
                {
                    g.DrawString((k * oneDivX).ToString("0.0"), signatureFont, drawBrush,
                        new Point(i + stepForAxes + 9, center.Y + 6), drawFormat);
                    g.DrawString(((k * oneDivX).ToString("0.0").ToString()+"-"), signatureFont, drawBrush,
                       new Point(j - stepForAxes + 9, center.Y + 6), drawFormat);
                }
            }
            for(int i = center.Y, j = center.Y, k=1;i<center.Y*2-15;j-=stepForAxes, i+=stepForAxes,k++)
            {
                g.DrawLine(a, center.X -lenghtShtrih, i, center.X+lenghtShtrih, i);
                g.DrawLine(a, center.X - lenghtShtrih, j, center.X + lenghtShtrih, j);
                 if(i<2*center.Y-40)
                {
                        g.DrawString((k * oneDivY).ToString("0.0").ToString()+"-", signatureFont, drawBrush,
                            new Point(center.X-6, i+stepForAxes-6), drawFormat);
                    g.DrawString((k * oneDivY).ToString("0.0"), signatureFont, drawBrush,
                            new Point(center.X - 6, j - stepForAxes - 6), drawFormat);
                }
            }
            int numOfPoint = 1000;
            float[] first = new float[numOfPoint]; float []thirt = new float[numOfPoint];
            for(float i=0, j=0;i<numOfPoint;i++, j+=(float)0.01)
            {
                first[(int)i] = j;
            }
            float[] second = new float[numOfPoint];
            for(float i=0, j=0;i<numOfPoint;i++,j+=(float)0.01)
            {
                second[(int)i] = (float)(Math.Pow(Math.Cos(10 * j), 2) + j);
            }
            for (float i = 0, j = 0; i < numOfPoint; i++, j -= (float)0.01)
            {
                thirt[(int)i] = (float)(Math.Pow(Math.Cos(10 * j), 2) + j);
            }
            Point[] pointOne = new Point[numOfPoint], pointTwo = new Point[numOfPoint];
            float tempX = 1 / oneDivX * stepForAxes;
            float tempY = 1 / oneDivY * stepForAxes;
            for(int i=0;i<numOfPoint;i++)
            {
                pointOne[i].X = center.X + (int)(first[i] * tempX);
                pointOne[i].Y = center.Y - (int)(second[i] * tempY);
                pointTwo[i].X = center.X - (int)(first[i] * tempX);
                pointTwo[i].Y = center.Y - (int)(thirt[i] * tempY);
            }
            g.DrawCurve(b, pointOne);
            g.DrawCurve(b, pointTwo);
        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void butCallForm_Click(object sender, EventArgs e)
        {
            MyForm kk = new MyForm();
            kk.Show();
        }
    }
}