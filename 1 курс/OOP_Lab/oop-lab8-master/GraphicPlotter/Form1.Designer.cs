﻿namespace GraphicPlotter
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.graphicPicture = new System.Windows.Forms.PictureBox();
            this.butCallForm = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.graphicPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // graphicPicture
            // 
            this.graphicPicture.Image = ((System.Drawing.Image)(resources.GetObject("graphicPicture.Image")));
            this.graphicPicture.Location = new System.Drawing.Point(12, 12);
            this.graphicPicture.Name = "graphicPicture";
            this.graphicPicture.Size = new System.Drawing.Size(154, 34);
            this.graphicPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.graphicPicture.TabIndex = 0;
            this.graphicPicture.TabStop = false;
            // 
            // butCallForm
            // 
            this.butCallForm.Location = new System.Drawing.Point(12, 52);
            this.butCallForm.Name = "butCallForm";
            this.butCallForm.Size = new System.Drawing.Size(154, 36);
            this.butCallForm.TabIndex = 2;
            this.butCallForm.Text = "Показати зразок з Chart";
            this.butCallForm.UseVisualStyleBackColor = true;
            this.butCallForm.Click += new System.EventHandler(this.butCallForm_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 461);
            this.Controls.Add(this.butCallForm);
            this.Controls.Add(this.graphicPicture);
            this.MinimumSize = new System.Drawing.Size(500, 500);
            this.Name = "Form1";
            this.Text = "Лабораторна робота №8";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.graphicPicture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox graphicPicture;
        private System.Windows.Forms.Button butCallForm;
    }
}

