﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicPlotter
{
    public partial class MyForm : Form
    {
        public MyForm()
        {
            InitializeComponent();
        }

        private void MyForm_Load(object sender, EventArgs e)
        {
            int stepForAxes = 25;
            int maxValueForAxesX = 10;
            int maxValueForAxesY = 10;
            int sizeWidth = MyForm.ActiveForm.Width;
            int sizeHeight = MyForm.ActiveForm.Height;

            Point center = new Point(((int)(sizeWidth / 2) - 8), (int)((sizeHeight / 2) - 19));
            float oneDivX = (float)maxValueForAxesX / ((float)center.X / (float)stepForAxes);
            float oneDivY = (float)maxValueForAxesY / ((float)center.Y / (float)stepForAxes);
            int numOfPoint = 1000;
            float[] first = new float[numOfPoint]; float[] thirt = new float[numOfPoint];
            for (float i = 0, j = 0; i < numOfPoint; i++, j += (float)0.01)
            {
                first[(int)i] = (float)Math.Round(j,2,MidpointRounding.AwayFromZero);
            }
            float[] second = new float[numOfPoint];
            for (float i = 0, j = 0; i < numOfPoint; i++, j += (float)0.01)
            {
                second[(int)i] = (float)(Math.Pow(Math.Cos(10 * j), 2) + j);
            }
            for (float i = 0, j = 0; i < numOfPoint; i++, j -= (float)0.01)
            {
                thirt[(int)i] = (float)(Math.Pow(Math.Cos(10 * j), 2) + j);
            }
            chart1.Series["Series1"].Points.DataBindXY(first, second);
            for (int i = 0; i < numOfPoint; i++) first[i] = first[i] * (-1);
            chart1.Series["Series2"].Points.DataBindXY(first, thirt);
        }
    }
}
