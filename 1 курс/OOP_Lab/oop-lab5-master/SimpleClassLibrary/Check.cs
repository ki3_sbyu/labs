﻿using System;

namespace SimpleClassLibrary
{
    public static class Check
    {
        public static string WriteWord(string value)
        {
            for (int j = 0; j != 1;)
            {
                for (int i = 0; i < value.Length; i++)
                {
                    if (!Char.IsLetter(value.ToCharArray()[i]))
                    {
                        Console.WriteLine("Неверно указано поле. Попробуйте еще раз.");
                        value = Console.ReadLine();
                        j = 0;
                        break;
                    }
                    else j = 1;
                }
            }
            return value;
        }
        public static string WriteDigit(string value)
        {
            for (int j = 0; j != 1;)
            {
                for (int i = 0; i < value.Length; i++)
                {
                    if (!Char.IsDigit(value.ToCharArray()[i]))
                    {
                        Console.WriteLine("Неверно указано поле. Попробуйте еще раз.");
                        value = Console.ReadLine();
                        j = 0;
                        break;
                    }
                    else j = 1;
                }
            }
            return value;
        }
        public static string WriteDoubleDigit(string value)
        {
            double num = 0.0;
            bool isOk = Double.TryParse(value,out num);
            for (int j = 0; j != 1;)
            {
                    if (isOk)
                    {
                        Console.WriteLine("Неверно указано поле. Попробуйте еще раз.");
                        value = Console.ReadLine();
                        j = 0;
                        break;
                    }
                    else j = 1;
            }
            return value;
        }
        public static string WritePersonName(string value)
        {
            char[] alphabet = "*$#%@!&^:;<>?/|1234567890-=+_~`".ToCharArray();
            int k = 0;
            do
            {
                for (int i = 0; i < value.Length; i++)
                {
                    for (int j = 0; j < alphabet.Length; j++)
                    {
                        if (value.ToCharArray()[i] == alphabet[j])
                        {
                            Console.WriteLine("Неверно указано поле. Попробуйте еще раз.");
                            value = Console.ReadLine();
                            k = 0;
                            break;
                        }
                        else k = 1;
                    }
                }
            } while (k != 1);
            return value;
        }
        public static string WriteMonth(string value)
        {
            int k = 0;
            do
            {
                value = Check.WriteDigit(value);
                if (Convert.ToInt32(value) > 12 || Convert.ToInt32(value) <= 0)
                {
                    Console.WriteLine("Неверно указано поле. Попробуйте еще раз.");
                    value = Console.ReadLine();
                    k = 0;
                }
                else
                {
                    value = Convert.ToString(Convert.ToInt32(value));
                    k = 1;
                }
            } while (k != 1);
            return value;
        }
        public static string WriteYear(string value)
        {
            int checkThis, k = 0;
            do
            {
                checkThis = Convert.ToInt32(Check.WriteDigit(value));
                if (checkThis > 2200 || checkThis <= 1900)
                {
                    Console.WriteLine("Неверно указано поле. Попробуйте еще раз.");
                    value = Console.ReadLine();
                    k = 0;
                }
                else
                {
                    value = Convert.ToString(checkThis);
                    k = 1;
                }
            } while (k != 1);
            return value;
        }
    }
}