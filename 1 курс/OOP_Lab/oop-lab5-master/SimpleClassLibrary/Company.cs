﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleClassLibrary
{
    public class Company
    {
        #region Захищені поля класу Company
        protected string compName;
        protected string position;
        protected string salary;

        #endregion
        #region Акцессори класу Company
        public string CompName
        {
            get { return compName; }
            set { this.compName = Check.WriteWord(value); }
        }
        public string Position
        {
            get { return position; }
            set { this.position = Check.WriteWord(value); }
        }
        public string Salary
        {
            get { return salary; }
            set { this.salary = Check.WriteDigit(value); }
        }
        #endregion
        #region Конструктори класу Company
        public Company(string name, string position, string salary)
        {
            this.compName = Check.WriteWord(name);
            this.position = Check.WriteWord(position);
            this.salary = Check.WriteDigit(salary);
        }
        public Company() : this("company") { }
        public Company(string name, string position) : this(name, position, "3200") { }
        public Company(string name) : this(name, "worker") { }

        #endregion
    }
}
