﻿using System;
using System.Collections.Generic;
using System.Text;
namespace SimpleClassLibrary
{
    public class Worker : Company
    {
        public static double currencyDol = 26.0;
        public static double currencyEuro = 32.07;
        #region Захищені поля класу Worker
        protected string name = "name";
        protected string year = "2018";
        protected string month = "04";
        protected string premium = "0";
        #endregion
        #region Акцессори класу Worker
        public string Name
        {
            get { return this.name; }
            set { this.name = Check.WritePersonName(value); }
        }
        public string Year
        {
            get { return year; }
            set
            {
                if (Convert.ToInt32(Check.WriteDigit(value)) > 1900 && Convert.ToInt32(Check.WriteDigit(value)) < 9999)
                {
                    this.year = value;
                }
            }
        }
        public string Month
        {
            get { return month; }
            set
            {
                if (Convert.ToInt32(Check.WriteDigit(value)) >= 1 && Convert.ToInt32(Check.WriteDigit(value)) <= 12)
                {
                    this.month = value;
                }
            }
        }
        public string Premium
        {
            set
            {
                Console.WriteLine("Выберите валюту:\n"+"1.Гривна\n"+"2.Доллар\n"+"3.Евро");
                int currency = Convert.ToInt32(Check.WriteDigit(Console.ReadLine()));
                value = Check.WriteDigit(Console.ReadLine());
                do {
                    switch (currency)
                    {
                        case 1:
                            {
                                premium = Check.WriteDigit(value);
                                break;
                            }
                        case 2:
                            {
                                premium = Convert.ToString(Convert.ToDouble(Check.WriteDigit(value)) * currencyDol);
                                break;
                            }
                        case 3:
                            {
                                premium = Convert.ToString(Convert.ToDouble(Check.WriteDigit(value)) * currencyEuro);
                                break;
                            }
                        default:
                            {
                                Console.WriteLine("Неверный ввод.Повторите попытку.");
                                currency = Convert.ToInt32(Check.WriteDigit(Console.ReadLine()));
                                break;
                            }
                    }

                } while (currency != 1 && currency != 2 && currency != 3);
            }
            get { return premium; }
        }
        #endregion
        #region Конструктори класу Worker
        public Worker() : this("name") { }
        public Worker(string name) : this(name, "2018") { }
        public Worker(string name, string year) : this(name, year, "04") { }
        public Worker(string name, string year, string month)
        {
            this.name = Check.WriteWord(name);
            if (Convert.ToInt32(Check.WriteDigit(year)) <= 9999 && (Convert.ToInt32(Check.WriteDigit(year)) >= 1950))
                this.year = Check.WriteDigit(year);
            if (Convert.ToInt32(Check.WriteDigit(month)) <= 12 && (Convert.ToInt32(Check.WriteDigit(month)) >= 0))
                this.month = Check.WriteDigit(month);
        }
        public Worker(Worker obj)
        {
            this.name = obj.name;
            this.year = obj.year;
            this.month = obj.month;
            this.position = obj.position;
            this.salary = obj.salary;
            this.compName = obj.compName;

        }
        #endregion
        #region Методи
        public int GetWorkExperience()
        {
            if (Convert.ToInt32(month) > DateTime.Now.Month)
            {
                return (DateTime.Now.Year - Convert.ToInt32(year) - 1) * 12 + 12 - Convert.ToInt32(month) - DateTime.Now.Month;
            }
            else
                return (DateTime.Now.Year - Convert.ToInt32(year)) * 12 + DateTime.Now.Month - Convert.ToInt32(month);
        }
        public int GetTotalMoney()
        {
            return GetWorkExperience() * Convert.ToInt32(salary);
        }
        

        #endregion
    }
}
