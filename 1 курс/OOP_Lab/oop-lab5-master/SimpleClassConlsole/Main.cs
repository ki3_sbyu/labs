﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleClassLibrary;
/*Лабораторна робота №5
 *Виконав Стецюк Б.Ю.
  гр.КІ-3*/
namespace SimpleClassConsole
{
    class Program
    {
        #region Методи класу Program
        public static Worker ReadWorker()
        {
            Worker array = new Worker();
            int ch = 0;
            Console.WriteLine
                (
                "1.Вводить все данные\n" +
                "2.Вводить только имя работника, год и месяц начала работы\n" +
                "3.Вводить только имя работника\n"
                );
            do
            {
                switch (Convert.ToInt32(Check.WriteDigit(Console.ReadLine())))
                {
                    case 1:
                        {
                            Console.WriteLine("Имя работника:");
                            array.Name = Check.WritePersonName(Console.ReadLine());
                            Console.WriteLine("Название компании:");
                            array.CompName = Check.WriteWord(Console.ReadLine());
                            Console.WriteLine("Должность:");
                            array.Position = Check.WriteWord(Console.ReadLine());
                            Console.WriteLine("Зарплата:");
                            array.Salary = Check.WriteDigit(Console.ReadLine());
                            Console.WriteLine("Месяц начала работы:");
                            array.Month = Check.WriteMonth(Console.ReadLine());
                            Console.WriteLine("Год начала работы:");
                            array.Year = Check.WriteYear(Console.ReadLine());
                            ch = 1;
                            break;
                        }
                    case 2:
                        {
                            Console.WriteLine("Имя работника:");
                            array.Name = Check.WritePersonName(Console.ReadLine());
                            Console.WriteLine("Месяц начала работы:");
                            array.Month = Check.WriteMonth(Console.ReadLine());
                            Console.WriteLine("Год начала работы:");
                            array.Year = Check.WriteYear(Console.ReadLine());
                            ch = 1;
                            break;
                        }
                    case 3:
                        {
                            Console.WriteLine("Имя работника:");
                            array.Name = Check.WritePersonName(Console.ReadLine());
                            ch = 1;
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Ошибка ввода.");
                            ch = 0;
                            break;
                        }
                }
            } while (ch != 1);
            return array;
        }
        public static Worker[] ReadWorkersArray(ref Worker[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = ReadWorker();
            }
            return array;
        }
        public static void PrintHeadWorker()
        {
            Console.WriteLine($"{"ID",-4} {"Имя работника",-25} {"Компания",-20} {"Должность",-10} {"ММ",2}/{"ГГГГ",-4} {"Зарплата(грн/ам.дол/евро)",26} {"Премиальные", -10}");
        }
        public static void PrintWorker(Worker Man, int id)
        {
            Console.WriteLine($"{id,-4} {Man.Name,-25} {Man.CompName,-20} {Man.Position,-10} {Man.Month,2}/{Man.Year,-4} {Man.Salary,8}/"+
                $"{Convert.ToDouble(Man.Salary) / Worker.currencyDol, 8:###.##}/{Convert.ToDouble(Man.Salary)/ Worker.currencyEuro, 8:###.##}  {Man.Premium, 10}");
        }
        public static void PrintWorkers(Worker[] array)
        {
            PrintHeadWorker();
            for (int i = 0; i < array.Length; i++)
            {
                PrintWorker(array[i], i);
            }
        }
        public static void GetWorkersInfo(Worker []database, out int min, out int max)
        {
            min = 0; max = 0;
            for(int i=0;i<database.Length-1;i++)
            {
                if(Convert.ToInt32(database[i].Salary)>Convert.ToInt32(database[i+1].Salary) && Convert.ToInt32(database[i+1].Salary) < min)
                {
                    min = Convert.ToInt32(database[i + 1].Salary);
                }
                if(Convert.ToInt32(database[i].Salary) < Convert.ToInt32(database[i + 1].Salary) && Convert.ToInt32(database[i + 1].Salary) > max)
                {
                    max = Convert.ToInt32(database[i + 1].Salary);
                }
            }
        }
        public static int CompareWorkerBySalary (Worker a, Worker b)
        {
            if (Convert.ToInt32(a.Salary) > Convert.ToInt32(b.Salary))
                return 1;
            if (Convert.ToInt32(a.Salary) < Convert.ToInt32(b.Salary))
                return -1;
            return 0;
        }
        public static int CompareWorkerByWorkExperience(Worker a, Worker b)
        {
            if (a.GetWorkExperience() > b.GetWorkExperience())
                return -1;
            if (a.GetWorkExperience() < b.GetWorkExperience())
                return 1;
            return 0;
        }
        public static void SortWorkerBySalary(ref Worker [] database)
        {
            Array.Sort(database, CompareWorkerBySalary);
        }
        public static void SortWorkerByWorkExperience(ref Worker []database)
        {
            Array.Sort(database, CompareWorkerByWorkExperience);
        }
        #endregion
        static void Main(string[] args)
        {
            #region Каcтомізація вікна
            Console.Title = "Лабораторна робота №5";
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            #endregion
            Worker[] database= new Worker[1];
            int menu=0;
            do
            {
                Console.WriteLine
                    (
                    "Меню\n" +
                    "1. Добавить данные о работниках в базу\n" +
                    "2. Вывести данные о всех работниках\n" +
                    "3. Вывести данные об работнике по ID\n" +
                    "4. Отсортировать работников по зарплате\n" +
                    "5. Отсортировать работников по стажу\n"+
                    "6. Узнать общее количество выданых денег работнику\n"+
                    "7. Узнать стаж работника\n"+
                    "8. Добавить премию работнику\n"+
                    "0. Выход"
                    );
                menu = Convert.ToInt32(Check.WriteDigit(Console.ReadLine()));
                switch (menu)
                {
                    case 1:
                        {
                            Console.WriteLine("Введите количество работников");
                            database = new Worker[Convert.ToInt32(Check.WriteDigit(Console.ReadLine()))];
                            ReadWorkersArray(ref database);
                            break;
                        }
                    case 2:
                        {
                            PrintWorkers(database);
                            break;
                        }
                    case 3:
                        {
                            Console.WriteLine("Введите ID работника");
                            int ID = Convert.ToInt32(Check.WriteDigit(Console.ReadLine()));
                            if (ID > database.Length || ID < 0)
                            {
                                Console.WriteLine("Такого работника не существует");
                            }
                            else
                            {
                                PrintHeadWorker();
                                PrintWorker(database[ID], ID);
                            }
                            break;
                        }
                    case 4:
                        {
                            SortWorkerBySalary(ref database);
                            break;
                        }
                    case 5:
                        {
                            SortWorkerByWorkExperience(ref database);
                            break;
                        }
                    case 6:
                        {
                            Console.WriteLine("Введите ID работника");
                            int ID = Convert.ToInt32(Check.WriteDigit(Console.ReadLine()));
                            
                            if (ID >= database.Length || ID < 0)
                            {
                                Console.WriteLine("Такого работника не существует");
                            }
                            else
                            {
                                Console.WriteLine($"Общая заработанная сумма: {database[ID].GetTotalMoney()} грн");
                            }
                            break;
                        }
                    case 7:
                        {
                            Console.WriteLine("Введите ID работника");
                            int ID = Convert.ToInt32(Check.WriteDigit(Console.ReadLine()));
                            if (ID >= database.Length || ID < 0)
                            {
                                Console.WriteLine("Такого работника не существует");
                            }
                            else
                            {
                                Console.WriteLine($"Общий стаж: {database[ID].GetWorkExperience()} месяц(цa,цeв)");
                            }
                            break;
                        }
                    case 8:
                        {
                            Console.WriteLine("Введите ID работника");
                            int ID = Convert.ToInt32(Check.WriteDigit(Console.ReadLine()));
                            if (ID >= database.Length || ID < 0)
                            {
                                Console.WriteLine("Такого работника не существует");
                            }
                            else
                            {
                                database[ID].Premium = "0";
                                Console.WriteLine("Успешно!");
                            }
                            break;
                        }
                    case 0:
                        {
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Ошибка ввода");
                            break;
                        }

                }
            } while (menu != 0);
        }
    }
}