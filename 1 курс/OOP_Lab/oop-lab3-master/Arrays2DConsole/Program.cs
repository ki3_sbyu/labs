﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays2DConsole
{
    class Program
    {
        static double ReadNum()
        {
            double number;
            bool check;
            step1:
            check = double.TryParse(Console.ReadLine(), out number);
            if (check)
            {
                return number;
            }
            else
            {
                Console.WriteLine("Помилка. Спробуйте ще раз.");
                goto step1;
            }
        }
        static void randomize(double [,]array, int n, int m)
        {
            Random rmd = new Random();
            int min = -33531, max = 33110;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    array[i,j] = rmd.Next(min, max)/1000.0;
                }
            }
        }
        static void SumRows(double [,]array, double []sumArr, int n, int m)
        {
            for (int i=0;i<n;i++)
            {
                for(int j=0;j<m;j++)
                {
                    sumArr[i] += Math.Abs(array[i,j]);
                }
            }
        }
        static void SortElem(double[,] array, int n, int m)
        {
            double temp;
            int i, j, k;
            for(int sort=0;sort<n;sort++)
            {
                for (k = m / 2; k > 0; k /= 2)
                    for (i = k; i < m; i++)
                    {
                        temp = array[sort, i];
                        for (j = i; j >= k; j -= k)
                        {
                            if (Math.Abs(temp) < Math.Abs(array[sort, j - k]))
                                array[sort, j] = array[sort,j - k];
                            else
                                break;
                        }
                        array[sort, j] = temp;
                    }
            }
        }
        static void PrintMatrixAndSumm(double [,] array, double [] arrSum, int n, int m)
        {
            for(int i=0;i<n;i++)
            {
                for(int j=0;j<m;j++)
                {
                    Console.Write($"{array[i, j]} ");
                }
                Console.Write($" Сумма елементів: {arrSum[i]}");
                Console.Write("\n");
            }
        }
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            //підтримка укр.мови
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Console.WriteLine("Введіть кількість рядків матриці: ");
            int n = Convert.ToInt32(ReadNum());
            Console.WriteLine("Введіть кількість ствопців матриці: ");
            int m = Convert.ToInt32(ReadNum());
            double[,] matrix = new double[n,m];
            double[] sumArr = new double[n];
            randomize(matrix, n, m);
            SortElem(matrix, n, m);
            SumRows(matrix, sumArr, n, m);
            PrintMatrixAndSumm(matrix, sumArr, n, m);

        }
    }
}
