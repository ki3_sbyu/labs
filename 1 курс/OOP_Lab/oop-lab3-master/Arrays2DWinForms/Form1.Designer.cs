﻿namespace Arrays2DWinForms
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.labCol = new System.Windows.Forms.Label();
            this.labRows = new System.Windows.Forms.Label();
            this.numRows = new System.Windows.Forms.NumericUpDown();
            this.numCol = new System.Windows.Forms.NumericUpDown();
            this.genRand = new System.Windows.Forms.Button();
            this.CalcSum = new System.Windows.Forms.Button();
            this.SortRows = new System.Windows.Forms.Button();
            this.dataGridViewMatrix = new System.Windows.Forms.DataGridView();
            this.labsrow1 = new System.Windows.Forms.Label();
            this.dataSum = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.numRows)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMatrix)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSum)).BeginInit();
            this.SuspendLayout();
            // 
            // labCol
            // 
            this.labCol.AutoSize = true;
            this.labCol.BackColor = System.Drawing.Color.Transparent;
            this.labCol.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labCol.Location = new System.Drawing.Point(9, 45);
            this.labCol.Name = "labCol";
            this.labCol.Size = new System.Drawing.Size(113, 13);
            this.labCol.TabIndex = 0;
            this.labCol.Text = "Кількість стовпчиків:";
            // 
            // labRows
            // 
            this.labRows.AutoSize = true;
            this.labRows.BackColor = System.Drawing.Color.Transparent;
            this.labRows.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labRows.Location = new System.Drawing.Point(9, 19);
            this.labRows.Name = "labRows";
            this.labRows.Size = new System.Drawing.Size(91, 13);
            this.labRows.TabIndex = 1;
            this.labRows.Text = "Кількість рядків:";
            // 
            // numRows
            // 
            this.numRows.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numRows.Location = new System.Drawing.Point(131, 17);
            this.numRows.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numRows.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numRows.Name = "numRows";
            this.numRows.Size = new System.Drawing.Size(285, 20);
            this.numRows.TabIndex = 2;
            this.numRows.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numCol
            // 
            this.numCol.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numCol.Location = new System.Drawing.Point(131, 43);
            this.numCol.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numCol.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numCol.Name = "numCol";
            this.numCol.Size = new System.Drawing.Size(285, 20);
            this.numCol.TabIndex = 3;
            this.numCol.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // genRand
            // 
            this.genRand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.genRand.Location = new System.Drawing.Point(423, 17);
            this.genRand.Name = "genRand";
            this.genRand.Size = new System.Drawing.Size(76, 21);
            this.genRand.TabIndex = 4;
            this.genRand.Text = "Генерувати";
            this.genRand.UseVisualStyleBackColor = true;
            this.genRand.Click += new System.EventHandler(this.genRand_Click);
            // 
            // CalcSum
            // 
            this.CalcSum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CalcSum.Location = new System.Drawing.Point(423, 43);
            this.CalcSum.Name = "CalcSum";
            this.CalcSum.Size = new System.Drawing.Size(76, 21);
            this.CalcSum.TabIndex = 5;
            this.CalcSum.Text = "Обрахувати сумму";
            this.CalcSum.UseVisualStyleBackColor = true;
            this.CalcSum.Click += new System.EventHandler(this.CalcSum_Click);
            // 
            // SortRows
            // 
            this.SortRows.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SortRows.Location = new System.Drawing.Point(505, 15);
            this.SortRows.Name = "SortRows";
            this.SortRows.Size = new System.Drawing.Size(167, 25);
            this.SortRows.TabIndex = 6;
            this.SortRows.Text = "Сортувати рядки";
            this.SortRows.UseVisualStyleBackColor = true;
            this.SortRows.Click += new System.EventHandler(this.SortRows_Click);
            // 
            // dataGridViewMatrix
            // 
            this.dataGridViewMatrix.AllowUserToAddRows = false;
            this.dataGridViewMatrix.AllowUserToDeleteRows = false;
            this.dataGridViewMatrix.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewMatrix.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewMatrix.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewMatrix.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMatrix.Location = new System.Drawing.Point(16, 67);
            this.dataGridViewMatrix.MaximumSize = new System.Drawing.Size(483, 226);
            this.dataGridViewMatrix.Name = "dataGridViewMatrix";
            this.dataGridViewMatrix.ReadOnly = true;
            this.dataGridViewMatrix.Size = new System.Drawing.Size(483, 226);
            this.dataGridViewMatrix.TabIndex = 7;
            // 
            // labsrow1
            // 
            this.labsrow1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labsrow1.AutoSize = true;
            this.labsrow1.BackColor = System.Drawing.Color.Transparent;
            this.labsrow1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labsrow1.Location = new System.Drawing.Point(505, 43);
            this.labsrow1.Name = "labsrow1";
            this.labsrow1.Size = new System.Drawing.Size(134, 13);
            this.labsrow1.TabIndex = 8;
            this.labsrow1.Text = "Сума елементів N рядка:";
            this.labsrow1.Click += new System.EventHandler(this.labsrow1_Click);
            // 
            // dataSum
            // 
            this.dataSum.AllowUserToAddRows = false;
            this.dataSum.AllowUserToDeleteRows = false;
            this.dataSum.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataSum.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataSum.Location = new System.Drawing.Point(505, 59);
            this.dataSum.Name = "dataSum";
            this.dataSum.ReadOnly = true;
            this.dataSum.Size = new System.Drawing.Size(167, 234);
            this.dataSum.TabIndex = 17;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(684, 301);
            this.Controls.Add(this.dataSum);
            this.Controls.Add(this.labsrow1);
            this.Controls.Add(this.dataGridViewMatrix);
            this.Controls.Add(this.SortRows);
            this.Controls.Add(this.CalcSum);
            this.Controls.Add(this.genRand);
            this.Controls.Add(this.numCol);
            this.Controls.Add(this.numRows);
            this.Controls.Add(this.labRows);
            this.Controls.Add(this.labCol);
            this.MaximumSize = new System.Drawing.Size(700, 340);
            this.MinimumSize = new System.Drawing.Size(450, 250);
            this.Name = "Form1";
            this.Text = "Лабораторна робота №3";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numRows)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMatrix)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSum)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labCol;
        private System.Windows.Forms.Label labRows;
        private System.Windows.Forms.NumericUpDown numRows;
        private System.Windows.Forms.NumericUpDown numCol;
        private System.Windows.Forms.Button genRand;
        private System.Windows.Forms.Button CalcSum;
        private System.Windows.Forms.Button SortRows;
        private System.Windows.Forms.DataGridView dataGridViewMatrix;
        private System.Windows.Forms.Label labsrow1;
        private System.Windows.Forms.DataGridView dataSum;
    }
}

