﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Arrays2DWinForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        double[,] matrix;
        private void Form1_Load(object sender, EventArgs e)
        {

        }
        static void randomize(double[,] array, int n, int m)
        {
            Random rmd = new Random();
            int min = -33531, max = 33110;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    array[i, j] = rmd.Next(min, max) / 1000.0;
                }
            }
        }
        static void SumRows(double[,] array, double[] sumArr, int n, int m)
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    sumArr[i] += Math.Abs(array[i, j]);
                }
            }
        }
        static void SortElem(double[,] array, int n, int m)
        {
            double temp;
            int i, j, k;
            for (int sort = 0; sort < n; sort++)
            {
                for (k = m / 2; k > 0; k /= 2)
                    for (i = k; i < m; i++)
                    {
                        temp = array[sort, i];
                        for (j = i; j >= k; j -= k)
                        {
                            if (Math.Abs(temp) < Math.Abs(array[sort, j - k]))
                                array[sort, j] = array[sort, j - k];
                            else
                                break;
                        }
                        array[sort, j] = temp;
                    }
            }
        }
        private void dataGridViewMatrix_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex == -1 && e.RowIndex > -1)
            {
                e.PaintBackground(e.CellBounds, true);
                using (SolidBrush br = new SolidBrush(Color.Black))
                {
                    StringFormat sf = new StringFormat();
                    sf.Alignment = StringAlignment.Center;
                    sf.LineAlignment = StringAlignment.Center;
                    e.Graphics.DrawString(e.RowIndex.ToString(),
            e.CellStyle.Font, br, e.CellBounds, sf);
                }
                e.Handled = true;
            }
        }

        private void genRand_Click(object sender, EventArgs e)
        {
            int n, m;
            n = Convert.ToInt32(numRows.Value);
            m = Convert.ToInt32(numCol.Value);
            matrix = new double[n,m];
            randomize(matrix, n, m);
            dataGridViewMatrix.RowCount = n;
            dataGridViewMatrix.ColumnCount = m;
            for (int j = 0; j < n; j++)
                dataGridViewMatrix.Rows[j].HeaderCell.Value = j.ToString();
            for (int i = 0; i < m; i++)
                dataGridViewMatrix.Columns[i].HeaderText = i.ToString();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    dataGridViewMatrix[j, i].Value = matrix[i, j];
                    dataGridViewMatrix.Columns[j].HeaderText = j.ToString();
                    dataGridViewMatrix.Rows[i].HeaderCell.Value = i.ToString();
                }
            }
            //

        }

        private void CalcSum_Click(object sender, EventArgs e) 
        {
            int n, m;
            n = Convert.ToInt32(numRows.Value);
            m = Convert.ToInt32(numCol.Value);
            double[] sumRow = new double[n];
            dataSum.RowCount = n;
            dataSum.ColumnCount = 1;
            SumRows(matrix, sumRow, n, m);
            for (int i = 0; i < n; i++)
            {
                dataSum[0, i].Value = sumRow[i];
                dataSum.Rows[i].HeaderCell.Value = i.ToString();
            }
        }

        private void labsrow1_Click(object sender, EventArgs e)
        {

        }

        private void SortRows_Click(object sender, EventArgs e)
        {
            int n, m;
            n = Convert.ToInt32(numRows.Value);
            m = Convert.ToInt32(numCol.Value);
            SortElem(matrix, n, m);
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    dataGridViewMatrix[j, i].Value = matrix[i, j];
                    dataGridViewMatrix.Columns[j].HeaderText = j.ToString();
                    dataGridViewMatrix.Rows[i].HeaderCell.Value = i.ToString();
                }
            }
        }
        }
    }