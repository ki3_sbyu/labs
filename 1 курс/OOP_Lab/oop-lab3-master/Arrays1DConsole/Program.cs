﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays1DConsole
{
    class Program
    {
        static void SortFirstElem(double[] array, int k)
        {
            double temp;
            int i, j, m;
            for (m = k / 2; m > 0; m /= 2)
                for (i = m; i < k; i++)
                {
                    temp = array[i];
                    for (j = i; j >= m; j -= m)
                    {
                        if (temp < array[j - m])
                            array[j] = array[j - m];
                        else
                            break;
                    }
                    array[j] = temp;
                }
        }
        static void PrintElemArray(double[] array)
        {
            for(int i=0;i<array.Length;i++)
            {
                Console.Write($" {array[i]} ");
                if ((i+1) % 10 == 0) Console.Write("\n");
            }
            Console.Write("\n");
        }
        static double ReadNum()
        {
            double number;
            bool check;
            step1:
            check = double.TryParse(Console.ReadLine(), out number);
            if (check)
            {
                return number;
            }
            else
            {
                Console.WriteLine("Помилка. Спробуйте ще раз.");
                goto step1;
            }
        }
        static int MulIndexNegElem(double []array)
        {
            int mul = 1, count = 0;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] < 0)
                {
                    count++;
                    mul *= i;
                }
            }
            if (count == 0) mul = -1;
            return mul;
        }
        static void Main(string[] args)
        {
            int n, k, min = -28305, max = 12951, menu, result;
            string error = "Від'ємних елементів не знайдено";
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            //підтримка укр.мови
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Random rnd = new Random();
            do
            {
                Console.WriteLine("Введіть кількість елементів в масиві: ");
                n = Convert.ToInt32(ReadNum());
                Console.WriteLine("Введіть кількість перших елементів в масиві, які потрібно відсортувати: ");
                k = Convert.ToInt32(ReadNum());
                double[] array = new double[n];
                //рандом
                for (int num = 0; num < array.Length; num++)
                {
                    array[num] = rnd.Next(min, max);
                    array[num] /= 1000;
                    array[num] = Math.Round(array[num], 3);
                }
                SortFirstElem(array, k);
                result = MulIndexNegElem(array);
                PrintElemArray(array);
                if (result != -1)
                {
                    error = Convert.ToString(result);
                }
                Console.WriteLine($"Добуток індексів усіх від'ємних значень - {error}\nДля повтору введіть 1, інше число - для виходу");
                menu = Convert.ToInt32(ReadNum());
            } while (menu == 1);
        }
    }
}
