﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Arrays1DWinForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }
        static void SortFirstElem(double[] array, int k)
        {
            double temp;
            int i, j, m;
            for (m = k / 2; m > 0; m /= 2)
                for (i = m; i < k; i++)
                {
                    temp = array[i];
                    for (j = i; j >= m; j -= m)
                    {
                        if (temp < array[j - m])
                            array[j] = array[j - m];
                        else
                            break;
                    }
                    array[j] = temp;
                }
        }
        static int MulIndexNegElem(double[] array)
        {
            int mul = 1, check = 0;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] < 0)
                {
                    mul *= i;
                    check++;
                }
            }
            if (check == 0) mul = -1;
            return mul;
        }
        double[] array;
        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void generate_Click(object sender, EventArgs e)
        {
            int n, k, min = -28305, max = 12951;
            n = Convert.ToInt16(numArray.Value);
            k = Convert.ToInt32(numSortArr.Value);
            if (k>n)
            {
                MessageBox.Show("Кількість елементів для сортування перевищує кількість елементів масиву.", "Помилка");
                goto end1;
            }
            Random rnd = new Random();
            dataGridViewArray.RowCount = 1;
            dataGridViewArray.ColumnCount = n;
            array = new double[n];
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(min, max) / 1000.0;
            }
            SortFirstElem(array, k);
            for(int i=0;i<array.Length;i++)
            {
                dataGridViewArray[i, 0].Value = array[i];
                dataGridViewArray.Columns[i].HeaderText = i.ToString();
            }
            end1:;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double result = MulIndexNegElem(array);
            if (result != -1)
                textMul.Text = Convert.ToString(result);
            else textMul.Text = "Від. елем. не знайдено.";

        }
    }
}
