﻿namespace Arrays1DWinForms
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.labN = new System.Windows.Forms.Label();
            this.numArray = new System.Windows.Forms.NumericUpDown();
            this.generate = new System.Windows.Forms.Button();
            this.labMul = new System.Windows.Forms.Label();
            this.textMul = new System.Windows.Forms.TextBox();
            this.labK = new System.Windows.Forms.Label();
            this.numSortArr = new System.Windows.Forms.NumericUpDown();
            this.dataGridViewArray = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numArray)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSortArr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewArray)).BeginInit();
            this.SuspendLayout();
            // 
            // labN
            // 
            this.labN.AutoSize = true;
            this.labN.BackColor = System.Drawing.Color.Transparent;
            this.labN.ForeColor = System.Drawing.Color.White;
            this.labN.Location = new System.Drawing.Point(20, 45);
            this.labN.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labN.Name = "labN";
            this.labN.Size = new System.Drawing.Size(250, 26);
            this.labN.TabIndex = 0;
            this.labN.Text = "Кількість елементів масиву: ";
            // 
            // numArray
            // 
            this.numArray.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numArray.Location = new System.Drawing.Point(265, 43);
            this.numArray.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.numArray.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numArray.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numArray.Name = "numArray";
            this.numArray.Size = new System.Drawing.Size(119, 33);
            this.numArray.TabIndex = 1;
            this.numArray.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // generate
            // 
            this.generate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.generate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.generate.Location = new System.Drawing.Point(398, 15);
            this.generate.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.generate.Name = "generate";
            this.generate.Size = new System.Drawing.Size(167, 33);
            this.generate.TabIndex = 2;
            this.generate.Text = "Генерувати";
            this.generate.UseVisualStyleBackColor = true;
            this.generate.Click += new System.EventHandler(this.generate_Click);
            // 
            // labMul
            // 
            this.labMul.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labMul.AutoSize = true;
            this.labMul.BackColor = System.Drawing.Color.Transparent;
            this.labMul.ForeColor = System.Drawing.Color.White;
            this.labMul.Location = new System.Drawing.Point(20, 221);
            this.labMul.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labMul.Name = "labMul";
            this.labMul.Size = new System.Drawing.Size(401, 26);
            this.labMul.TabIndex = 3;
            this.labMul.Text = "Добуток індексів від\'ємних елементів масиву:";
            // 
            // textMul
            // 
            this.textMul.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textMul.Location = new System.Drawing.Point(429, 218);
            this.textMul.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.textMul.Name = "textMul";
            this.textMul.ReadOnly = true;
            this.textMul.Size = new System.Drawing.Size(130, 33);
            this.textMul.TabIndex = 4;
            // 
            // labK
            // 
            this.labK.AutoSize = true;
            this.labK.BackColor = System.Drawing.Color.Transparent;
            this.labK.ForeColor = System.Drawing.Color.White;
            this.labK.Location = new System.Drawing.Point(20, 95);
            this.labK.Name = "labK";
            this.labK.Size = new System.Drawing.Size(444, 26);
            this.labK.TabIndex = 5;
            this.labK.Text = "Кількість перших відсортованих елементів масиву:";
            // 
            // numSortArr
            // 
            this.numSortArr.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numSortArr.Location = new System.Drawing.Point(470, 93);
            this.numSortArr.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numSortArr.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numSortArr.Name = "numSortArr";
            this.numSortArr.Size = new System.Drawing.Size(89, 33);
            this.numSortArr.TabIndex = 6;
            this.numSortArr.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // dataGridViewArray
            // 
            this.dataGridViewArray.AllowUserToAddRows = false;
            this.dataGridViewArray.AllowUserToDeleteRows = false;
            this.dataGridViewArray.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewArray.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewArray.Location = new System.Drawing.Point(19, 148);
            this.dataGridViewArray.Name = "dataGridViewArray";
            this.dataGridViewArray.ReadOnly = true;
            this.dataGridViewArray.Size = new System.Drawing.Size(546, 61);
            this.dataGridViewArray.TabIndex = 7;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(398, 53);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(167, 33);
            this.button1.TabIndex = 8;
            this.button1.Text = "Розрахувати";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 26F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(584, 301);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridViewArray);
            this.Controls.Add(this.numSortArr);
            this.Controls.Add(this.labK);
            this.Controls.Add(this.textMul);
            this.Controls.Add(this.labMul);
            this.Controls.Add(this.generate);
            this.Controls.Add(this.numArray);
            this.Controls.Add(this.labN);
            this.Font = new System.Drawing.Font("Microsoft MHei", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ForeColor = System.Drawing.Color.Black;
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.MaximumSize = new System.Drawing.Size(700, 700);
            this.MinimumSize = new System.Drawing.Size(600, 340);
            this.Name = "Form1";
            this.Text = "Лабораторна робота №3";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numArray)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSortArr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewArray)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labN;
        private System.Windows.Forms.NumericUpDown numArray;
        private System.Windows.Forms.Button generate;
        private System.Windows.Forms.Label labMul;
        private System.Windows.Forms.TextBox textMul;
        private System.Windows.Forms.Label labK;
        private System.Windows.Forms.NumericUpDown numSortArr;
        private System.Windows.Forms.DataGridView dataGridViewArray;
        private System.Windows.Forms.Button button1;
    }
}

