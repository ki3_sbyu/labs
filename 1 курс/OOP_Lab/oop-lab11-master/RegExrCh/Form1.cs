﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace RegExrCh
{
    public partial class LabTask11 : Form
    {
        public LabTask11()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void butChPhone_Click(object sender, EventArgs e)
        {
            
        }

        private void textPhone_TextChanged(object sender, EventArgs e)
        {
            Regex _check = new Regex(@"^\+380\d{9}$");
            MatchCollection _match = _check.Matches(textPhone.Text);
            if (_match.Count > 0)
            {
                textPhone.BackColor = Color.LightGreen;
            }
            else textPhone.BackColor = Color.LightCoral;
        }

        private void textPass_TextChanged(object sender, EventArgs e)
        {
            Regex _check = new Regex(@"^([A-Z]){2}\d{6}$");
            MatchCollection _match = _check.Matches(textPass.Text);
            if (_match.Count > 0)
            {
                textPass.BackColor = Color.LightGreen;
            }
            else textPass.BackColor = Color.LightCoral;
        }

        private void textNumInt_TextChanged(object sender, EventArgs e)
        {
            Regex _check = new Regex(@"^((10[3-9][1-9]{2}|1[1-9]\d{3})|[2-7]\d{4}|8[0-8]\d{3}|89[0-6][0-4][0-5])$");
            MatchCollection _match = _check.Matches(textNumInt.Text);
            if (_match.Count > 0)
            {
                textNumInt.BackColor = Color.LightGreen;
            }
            else textNumInt.BackColor = Color.LightCoral;
        }

        private void textUkrName_TextChanged(object sender, EventArgs e)
        {
            Regex _check = new Regex(@"^[A-ЩЯЮЄЇ\'І]([а-щяюєї\'і]*)$");
            MatchCollection _match = _check.Matches(textUkrName.Text);
            if (_match.Count > 0)
            {
                textUkrName.BackColor = Color.LightGreen;
            }
            else textUkrName.BackColor = Color.LightCoral;
        }

        private void textDate_TextChanged(object sender, EventArgs e)
        {
            Regex _check = new Regex(@"^([01]\d|2[0-3]):([0-5]\d)$");
            MatchCollection _match = _check.Matches(textDate.Text);
            if (_match.Count > 0)
            {
                textDate.BackColor = Color.LightGreen;
            }
            else textDate.BackColor = Color.LightCoral;
        }

        private void textEmail_TextChanged(object sender, EventArgs e)
        {
            Regex _check = new Regex(@"^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$");
            MatchCollection _match = _check.Matches(textEmail.Text);
            if (_match.Count > 0)
            {
                textEmail.BackColor = Color.LightGreen;
            }
            else textEmail.BackColor = Color.LightCoral;
        }
    }
}