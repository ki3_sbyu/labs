﻿namespace RegExrCh
{
    partial class LabTask11
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.labPhoneNumber = new System.Windows.Forms.Label();
            this.labPass = new System.Windows.Forms.Label();
            this.labNumInt = new System.Windows.Forms.Label();
            this.labDate = new System.Windows.Forms.Label();
            this.labUkrName = new System.Windows.Forms.Label();
            this.labEmail = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textPhone = new System.Windows.Forms.TextBox();
            this.textPass = new System.Windows.Forms.TextBox();
            this.textNumInt = new System.Windows.Forms.TextBox();
            this.textUkrName = new System.Windows.Forms.TextBox();
            this.textDate = new System.Windows.Forms.TextBox();
            this.textEmail = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // labPhoneNumber
            // 
            this.labPhoneNumber.AutoSize = true;
            this.labPhoneNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labPhoneNumber.Location = new System.Drawing.Point(12, 63);
            this.labPhoneNumber.Name = "labPhoneNumber";
            this.labPhoneNumber.Size = new System.Drawing.Size(132, 18);
            this.labPhoneNumber.TabIndex = 0;
            this.labPhoneNumber.Text = "Номер телефону:";
            // 
            // labPass
            // 
            this.labPass.AutoSize = true;
            this.labPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labPass.Location = new System.Drawing.Point(12, 89);
            this.labPass.Name = "labPass";
            this.labPass.Size = new System.Drawing.Size(127, 18);
            this.labPass.TabIndex = 1;
            this.labPass.Text = "Номер паспорта:";
            this.labPass.Click += new System.EventHandler(this.label2_Click);
            // 
            // labNumInt
            // 
            this.labNumInt.AutoSize = true;
            this.labNumInt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labNumInt.Location = new System.Drawing.Point(12, 115);
            this.labNumInt.Name = "labNumInt";
            this.labNumInt.Size = new System.Drawing.Size(234, 18);
            this.labNumInt.TabIndex = 2;
            this.labNumInt.Text = "Число з інтервалу [10311;89645]:";
            this.labNumInt.Click += new System.EventHandler(this.label3_Click);
            // 
            // labDate
            // 
            this.labDate.AutoSize = true;
            this.labDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labDate.Location = new System.Drawing.Point(14, 167);
            this.labDate.Name = "labDate";
            this.labDate.Size = new System.Drawing.Size(93, 18);
            this.labDate.TabIndex = 3;
            this.labDate.Text = "Введіть час:";
            // 
            // labUkrName
            // 
            this.labUkrName.AutoSize = true;
            this.labUkrName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labUkrName.Location = new System.Drawing.Point(12, 141);
            this.labUkrName.Name = "labUkrName";
            this.labUkrName.Size = new System.Drawing.Size(118, 18);
            this.labUkrName.TabIndex = 4;
            this.labUkrName.Text = "Українське ім\'я:";
            this.labUkrName.Click += new System.EventHandler(this.label5_Click);
            // 
            // labEmail
            // 
            this.labEmail.AutoSize = true;
            this.labEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labEmail.Location = new System.Drawing.Point(14, 193);
            this.labEmail.Name = "labEmail";
            this.labEmail.Size = new System.Drawing.Size(54, 18);
            this.labEmail.TabIndex = 5;
            this.labEmail.Text = "E-mail:";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(12, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(461, 25);
            this.label7.TabIndex = 6;
            this.label7.Text = "Демонстрація роботи з регулярними виразами";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // textPhone
            // 
            this.textPhone.Location = new System.Drawing.Point(245, 64);
            this.textPhone.Name = "textPhone";
            this.textPhone.Size = new System.Drawing.Size(227, 20);
            this.textPhone.TabIndex = 7;
            this.textPhone.TextChanged += new System.EventHandler(this.textPhone_TextChanged);
            // 
            // textPass
            // 
            this.textPass.Location = new System.Drawing.Point(245, 90);
            this.textPass.Name = "textPass";
            this.textPass.Size = new System.Drawing.Size(227, 20);
            this.textPass.TabIndex = 9;
            this.textPass.TextChanged += new System.EventHandler(this.textPass_TextChanged);
            // 
            // textNumInt
            // 
            this.textNumInt.Location = new System.Drawing.Point(245, 116);
            this.textNumInt.Name = "textNumInt";
            this.textNumInt.Size = new System.Drawing.Size(227, 20);
            this.textNumInt.TabIndex = 10;
            this.textNumInt.TextChanged += new System.EventHandler(this.textNumInt_TextChanged);
            // 
            // textUkrName
            // 
            this.textUkrName.Location = new System.Drawing.Point(245, 142);
            this.textUkrName.Name = "textUkrName";
            this.textUkrName.Size = new System.Drawing.Size(227, 20);
            this.textUkrName.TabIndex = 11;
            this.textUkrName.TextChanged += new System.EventHandler(this.textUkrName_TextChanged);
            // 
            // textDate
            // 
            this.textDate.Location = new System.Drawing.Point(245, 168);
            this.textDate.Name = "textDate";
            this.textDate.Size = new System.Drawing.Size(227, 20);
            this.textDate.TabIndex = 12;
            this.textDate.TextChanged += new System.EventHandler(this.textDate_TextChanged);
            // 
            // textEmail
            // 
            this.textEmail.Location = new System.Drawing.Point(245, 194);
            this.textEmail.Name = "textEmail";
            this.textEmail.Size = new System.Drawing.Size(227, 20);
            this.textEmail.TabIndex = 13;
            this.textEmail.TextChanged += new System.EventHandler(this.textEmail_TextChanged);
            // 
            // LabTask11
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 261);
            this.Controls.Add(this.textEmail);
            this.Controls.Add(this.textDate);
            this.Controls.Add(this.textUkrName);
            this.Controls.Add(this.textNumInt);
            this.Controls.Add(this.textPass);
            this.Controls.Add(this.textPhone);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.labEmail);
            this.Controls.Add(this.labUkrName);
            this.Controls.Add(this.labDate);
            this.Controls.Add(this.labNumInt);
            this.Controls.Add(this.labPass);
            this.Controls.Add(this.labPhoneNumber);
            this.MaximumSize = new System.Drawing.Size(500, 300);
            this.MinimumSize = new System.Drawing.Size(500, 300);
            this.Name = "LabTask11";
            this.Text = "Лабораторна робота №11";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labPhoneNumber;
        private System.Windows.Forms.Label labPass;
        private System.Windows.Forms.Label labNumInt;
        private System.Windows.Forms.Label labDate;
        private System.Windows.Forms.Label labUkrName;
        private System.Windows.Forms.Label labEmail;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textPhone;
        private System.Windows.Forms.TextBox textPass;
        private System.Windows.Forms.TextBox textNumInt;
        private System.Windows.Forms.TextBox textUkrName;
        private System.Windows.Forms.TextBox textDate;
        private System.Windows.Forms.TextBox textEmail;
    }
}

