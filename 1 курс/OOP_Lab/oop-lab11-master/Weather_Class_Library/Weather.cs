﻿using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace Weather_Class_Library
{
    public class Weather
    {
        protected string url;
        protected string status;
        protected string temp;
        protected string wind;
        protected string wind_vector;
        protected string humidity;
        protected string temp_water;
        protected string page;
        protected string picLocation;
        Regex _regexr; Match _match;
        Regex _regNum = new Regex(@"[+-]?\d+");
        Regex _regWord = new Regex(@"\s[а-яіїє\-\,]*");
        Regex _regStatus = new Regex(@"[А-ЯІЇЄа-яіїє\, ]+");
        #region GET
        public string GetPicLocation()
        {
            return picLocation;
        }
        public string GetStatus()
        {
            return status;
        }
        public string GetTemp()
        {
            return temp;
        }
        public string GetWind()
        {
            return wind;
        }
        public string GetWind_Vector()
        {
            return wind_vector;
        }
        public string GetHumidity()
        {
            return humidity;
        }
        public string GetTemp_Water()
        {
            return temp_water;
        }
        public string GetPage()
        {
            return page;
        }
        #endregion
        #region SET
        public void SetAll()
        {
            SetTemp();
            SetWind();
            SetWind_Vector();
            SetHumidity();
            SetStatus();
            SetWaterTemp();
            SetPicLocation();
        }
        public void SetURL(string url)
        {
            page = new WebClient { Encoding = Encoding.UTF8 }.DownloadString(url);
        }
        public void SetTemp()
        {
            _regexr = new Regex(@"c\'\>[+-]?\d+\<span");
            _match = _regexr.Match(page);
            if (_match.Groups.Count > 0)
            {
                string s = _match.Value;
                Match match = _regNum.Match(s);
                temp = match.Value;
            }
            else status="";
        }
        public void SetWind()
        {
            _regexr = new Regex(@"m_wind ms\' style=[\'\:\w]*\>\d*");
            _match = _regexr.Match(page);
            if(_match.Groups.Count>0)
            {
                string s = _match.Value;
                Match match = _regNum.Match(s);
                wind = match.Value;
            }
        }
        public void SetWind_Vector()
        {
            _regexr = new Regex(@"Вітер [а-яіїє\-]*");
            _match = _regexr.Match(page);
            if(_match.Groups.Count>0)
            {
                string s = _match.Value;
                Match match = _regWord.Match(s);
                wind_vector = match.Value;
            }
        }
        public void SetHumidity()
        {
            _regexr = new Regex(@"\SВологість\S>\d+<");
            _match = _regexr.Match(page);
            if (_match.Groups.Count > 0)
            {
                string s = _match.Value;
                Match match = _regNum.Match(s);
                humidity = match.Value;
            }
        }
        public void SetStatus()
        {
            _regexr = new Regex(@"<td>[А-ЯІЇЄа-яіїє\s\-\,]+<\/td>");
            _match = _regexr.Match(page);
            if (_match.Groups.Count > 0)
            {
                string s = _match.Value;
                Match match = _regStatus.Match(s);
                status = match.Value;
            }
        }
        public void SetWaterTemp()
        {
            _regexr = new Regex(@"c\S\>\d+\<span");
            _match = _regexr.Match(page);
            if (_match.Groups.Count > 0)
            {
                string s = _match.Value;
                Match match = _regNum.Match(s);
                temp_water = match.Value;
            }
        }
        public void SetPicLocation()
        {
            _regexr = new Regex(@"url\([\s\w\/\.]+\.png\)");
            Regex picURL = new Regex(@"[\s\w\/\.]+\.png");
            _match = _regexr.Match(page);
            if (_match.Groups.Count > 0)
            {
                Match match = picURL.Match(_match.Value);
                picLocation = "https:" + match.Value;
            }
        }
    }
    #endregion
}