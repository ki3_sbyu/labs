﻿namespace WeatherInformer
{
    partial class myForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(myForm));
            this.pictureStatus = new System.Windows.Forms.PictureBox();
            this.textStatus = new System.Windows.Forms.Label();
            this.textTemp = new System.Windows.Forms.Label();
            this.textWind = new System.Windows.Forms.Label();
            this.textWindVector = new System.Windows.Forms.Label();
            this.textWaterTemp = new System.Windows.Forms.Label();
            this.textHumidity = new System.Windows.Forms.Label();
            this.degC = new System.Windows.Forms.Label();
            this.Refresh_button = new System.Windows.Forms.Button();
            this.butOdessa = new System.Windows.Forms.Button();
            this.butKiev = new System.Windows.Forms.Button();
            this.butZhytomyr = new System.Windows.Forms.Button();
            this.labLoc = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureStatus)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureStatus
            // 
            resources.ApplyResources(this.pictureStatus, "pictureStatus");
            this.pictureStatus.Name = "pictureStatus";
            this.pictureStatus.TabStop = false;
            // 
            // textStatus
            // 
            resources.ApplyResources(this.textStatus, "textStatus");
            this.textStatus.Name = "textStatus";
            this.textStatus.Click += new System.EventHandler(this.textStatus_Click);
            // 
            // textTemp
            // 
            resources.ApplyResources(this.textTemp, "textTemp");
            this.textTemp.Name = "textTemp";
            // 
            // textWind
            // 
            resources.ApplyResources(this.textWind, "textWind");
            this.textWind.Name = "textWind";
            this.textWind.Click += new System.EventHandler(this.textWind_Click);
            // 
            // textWindVector
            // 
            resources.ApplyResources(this.textWindVector, "textWindVector");
            this.textWindVector.Name = "textWindVector";
            // 
            // textWaterTemp
            // 
            resources.ApplyResources(this.textWaterTemp, "textWaterTemp");
            this.textWaterTemp.Name = "textWaterTemp";
            // 
            // textHumidity
            // 
            resources.ApplyResources(this.textHumidity, "textHumidity");
            this.textHumidity.Name = "textHumidity";
            // 
            // degC
            // 
            resources.ApplyResources(this.degC, "degC");
            this.degC.Name = "degC";
            // 
            // Refresh_button
            // 
            resources.ApplyResources(this.Refresh_button, "Refresh_button");
            this.Refresh_button.Name = "Refresh_button";
            this.Refresh_button.UseVisualStyleBackColor = true;
            this.Refresh_button.Click += new System.EventHandler(this.Refresh_button_Click);
            // 
            // butOdessa
            // 
            resources.ApplyResources(this.butOdessa, "butOdessa");
            this.butOdessa.Name = "butOdessa";
            this.butOdessa.UseVisualStyleBackColor = true;
            this.butOdessa.Click += new System.EventHandler(this.butOdessa_Click);
            // 
            // butKiev
            // 
            resources.ApplyResources(this.butKiev, "butKiev");
            this.butKiev.Name = "butKiev";
            this.butKiev.UseVisualStyleBackColor = true;
            this.butKiev.Click += new System.EventHandler(this.butKiev_Click);
            // 
            // butZhytomyr
            // 
            resources.ApplyResources(this.butZhytomyr, "butZhytomyr");
            this.butZhytomyr.Name = "butZhytomyr";
            this.butZhytomyr.UseVisualStyleBackColor = true;
            this.butZhytomyr.Click += new System.EventHandler(this.button3_Click);
            // 
            // labLoc
            // 
            resources.ApplyResources(this.labLoc, "labLoc");
            this.labLoc.Name = "labLoc";
            // 
            // myForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labLoc);
            this.Controls.Add(this.butZhytomyr);
            this.Controls.Add(this.butKiev);
            this.Controls.Add(this.butOdessa);
            this.Controls.Add(this.Refresh_button);
            this.Controls.Add(this.degC);
            this.Controls.Add(this.textHumidity);
            this.Controls.Add(this.textWaterTemp);
            this.Controls.Add(this.textWindVector);
            this.Controls.Add(this.textWind);
            this.Controls.Add(this.textTemp);
            this.Controls.Add(this.textStatus);
            this.Controls.Add(this.pictureStatus);
            this.Name = "myForm";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureStatus)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureStatus;
        private System.Windows.Forms.Label textStatus;
        private System.Windows.Forms.Label textTemp;
        private System.Windows.Forms.Label textWind;
        private System.Windows.Forms.Label textWindVector;
        private System.Windows.Forms.Label textWaterTemp;
        private System.Windows.Forms.Label textHumidity;
        private System.Windows.Forms.Label degC;
        private System.Windows.Forms.Button Refresh_button;
        private System.Windows.Forms.Button butOdessa;
        private System.Windows.Forms.Button butKiev;
        private System.Windows.Forms.Button butZhytomyr;
        private System.Windows.Forms.Label labLoc;
    }
}

