﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Weather_Class_Library;

namespace WeatherInformer
{
    public partial class myForm : Form
    {
        public string URL = "";
        public myForm()
        {
            InitializeComponent();
        }

        private void textWindVector_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Weather pogoda = new Weather();
            button3_Click(sender,e);
        }

        private void textStatus_Click(object sender, EventArgs e)
        {

        }

        private void textHumidity_TextChanged(object sender, EventArgs e)
        {

        }

        private void textWind_Click(object sender, EventArgs e)
        {

        }
        private void Refresh(Weather pogoda)
        {
            pogoda.SetAll();
            textTemp.Text = pogoda.GetTemp();
            textWind.Text ="Шв. вітру "+ pogoda.GetWind()+ " м/с";
            textWindVector.Text ="Вітер"+ pogoda.GetWind_Vector();
            textHumidity.Text ="Вологість "+ pogoda.GetHumidity()+" %";
            textStatus.Text = pogoda.GetStatus();
            textWaterTemp.Text = "Темп. води "+pogoda.GetTemp_Water()+ "°C";
            pictureStatus.ImageLocation = pogoda.GetPicLocation();
            textStatus.Text = pogoda.GetStatus();
        }

        private void Refresh_button_Click(object sender, EventArgs e)
        {
            Weather pogoda = new Weather();
            pogoda.SetURL(URL);
            Refresh(pogoda);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Weather pogoda = new Weather();
            URL = "https://www.gismeteo.ua/ua/weather-zhytomyr-4943/";
            labLoc.Text = "Погода в Житомирі";
            pogoda.SetURL(URL);
            Refresh(pogoda);
        }

        private void butKiev_Click(object sender, EventArgs e)
        {
            Weather pogoda = new Weather();
            URL = "https://www.gismeteo.ua/ua/weather-kyiv-4944/";
            labLoc.Text = "Погода в Києві";
            pogoda.SetURL(URL);
            Refresh(pogoda);
        }

        private void butOdessa_Click(object sender, EventArgs e)
        {
            Weather pogoda = new Weather();
            labLoc.Text = "Погода в Одесі";
            URL = "https://www.gismeteo.ua/ua/weather-odessa-4982/";
            pogoda.SetURL(URL);
            Refresh(pogoda);
        }
    }
}
