﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;
using System.Text;

namespace FilesForms
{
    public partial class FormHeadFile : Form
    {
        List<FileInfo> files = new List<FileInfo>();
        List<FileInfo> gzar = new List<FileInfo>();
        List<FileInfo> crypt = new List<FileInfo>();
        int CountOfZipFile = 0, CountOfZipGzar = 0, CountOfZipCrypt = 0;
        List<string> str = new List<string>();
        public FormHeadFile()
        {
            InitializeComponent();

        }

        //Добавление элемента File в список
        private void AddButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog opfd = new OpenFileDialog();
            if (opfd.ShowDialog(this) == DialogResult.OK)
            {
                string s = opfd.FileName;
                string sc = "";
                FileInfo f = new FileInfo(s);
                for (int j = opfd.FileName.Length - 1; j != 0; j--)
                {
                    if (s[j] == '\\')
                    {
                        sc = @"C:\Users\Yuriy Dazhuk\Desktop\Test\DecompressDecrypt";
                        s = s.Remove(0, j);
                        s = s.Insert(0, sc);
                        break;
                    }
                }
                File.Copy(opfd.FileName,s);
                files.Add(f);
            }
            Refresher(CountFilesLabel, BasicListBox, files);
        }

        //Обновление списка
        private void Refresher(Label label, ListBox listBox, List<FileInfo> listFileInfo)
        {
            listBox.Items.Clear();
            foreach (FileInfo f in listFileInfo)
                if (f != null)
                {
                    listBox.Items.Add(f);
                }
            label.Text = listBox.Items.Count.ToString();
        }

        //Очистка списка file
        private void ClearFileButton_Click(object sender, EventArgs e)
        {
            GeneralClear(CountFilesLabel, BasicListBox, files, 0);
        }

        //Effects of File listbox
        private void BasicListBox_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
        }

        //Drag&Drop of File listbox
        private void BasicListBox_DragDrop(object sender, DragEventArgs e)
        {
            string[] Files = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            foreach (string file in Files)
            {
                BasicListBox.Items.Add(file);
                FileInfo f = new FileInfo(file);
                files.Add(f);
            }
            Refresher(CountFilesLabel, BasicListBox, files);
        }

        //Сжатие списка
        private void ZipButton_Click(object sender, EventArgs e)
        {
            string s = "";
            if (files.Count == 0) { MessageBox.Show("Please add file before zip!"); return; }
            for (int i = 0; i < files.Count; i++)
            {
                str.Add(files[i].ToString());
                for (int j = str[i].Length - 1; j != 0; j--)
                {
                    if (str[i][j] == '\\')
                    {
                        s = @"C:\Users\Yuriy Dazhuk\Desktop\Test\Compress";
                        str[i] = str[i].Remove(0, j);
                        str[i] = str[i].Insert(0, s);
                        break;
                    }
                }
                str[i] += ".gzar";
                Compress(files[i].FullName, str[i]);
                FileInfo f = new FileInfo(str[i]);
                gzar.Add(f);
            }
            str.Clear();
            MessageBox.Show("File was compress!");
            Refresher(CountFilesLabel, BasicListBox, files);
            Refresher(CountGzarLabel, CompressListBox, gzar);
        }

        //Сжатие файла
        private void Compress(string sourceFile, string compressedFile)
        {
            // поток для чтения исходного файла
            using (FileStream sourceStream = new FileStream(sourceFile, FileMode.OpenOrCreate))
            {
                // поток для записи сжатого файла
                using (FileStream targetStream = File.Create(compressedFile))
                {
                    // поток архивации
                    using (GZipStream compressionStream = new GZipStream(targetStream, CompressionMode.Compress))
                    {
                        sourceStream.CopyTo(compressionStream); // копируем байты из одного потока в другой
                    }
                }
            }
        }

        //Чтение сжатого файла
        private void Decompress(string compressedFile, string targetFile)
        {
            // поток для чтения из сжатого файла
            using (FileStream sourceStream = new FileStream(compressedFile, FileMode.OpenOrCreate))
            {
                // поток для записи восстановленного файла
                using (FileStream targetStream = File.Create(targetFile))
                {
                    // поток разархивации
                    using (GZipStream decompressionStream = new GZipStream(sourceStream, CompressionMode.Decompress))
                    {
                        decompressionStream.CopyTo(targetStream);
                    }
                }
            }
        }

        //Удаление элемента списка File
        private void DeleteFileButton_Click_1(object sender, EventArgs e)
        {
            GeneraDelete(CountFilesLabel, BasicListBox, files);
        }

        //Удаление элемента списка Gzar
        private void DeleteZipButton_Click(object sender, EventArgs e)
        {
            GeneraDelete(CountGzarLabel, CompressListBox, gzar);
        }

        //Распаковка архива File через OpenDialog в папку Unzip
        private void UnZipFileButton_Click(object sender, EventArgs e)
        {
            GeneralUnZip(CountOfZipFile);
        }

        //Создание архива списка gzars с расширением *.zip*
        private void ZipGzarButton_Click(object sender, EventArgs e)
        {
            if (gzar.Count == 0) { MessageBox.Show("You must to use function -> (Compress) before!"); return; }
            string start = @"C:\Users\Yuriy Dazhuk\Desktop\Test\Compress";
            string finish = start.Replace(@"\Compress", "") + @"\Zip\Gzar" + ".zip";
            FileInfo check = new FileInfo(finish);
            if (check.Exists)
            {
                for (int i = finish.Length - 1; i != 0; i--)
                {
                    if (finish[i] == '.')
                    {
                        finish = finish.Insert(i, $"{CountOfZipGzar}");
                    }
                }
            }
            ZipFile.CreateFromDirectory(start, finish);
            MessageBox.Show("Zip file was created!");
            CountOfZipGzar++;
        }

        //Drag&Drop of Gzar listbox
        private void CompressListBox_DragDrop(object sender, DragEventArgs e)
        {
            int count = 0;
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            for (int i = 0; i < files.Length; i++)
            {
                if (files[i].EndsWith(".gzar"))
                {
                    CompressListBox.Items.Add(files[i]);
                    FileInfo f = new FileInfo(files[i]);
                    gzar.Add(f);
                    count++;
                }
            }
            if (count == 0) MessageBox.Show("Please put files only with extension *.gzar* in the middle listbox!");
            Refresher(CountGzarLabel, CompressListBox, gzar);
        }

        //Effects of Gzar listbox
        private void CompressListBox_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
        }

        //Распаковка архива Gzar через OpenDialog в папку Unzip
        private void UnZipGzarButton_Click(object sender, EventArgs e)
        {
            GeneralUnZip(CountOfZipGzar);
        }

        //Шифрование 
        private void EncryptButton_Click(object sender, EventArgs e)
        {
            string s = "";
            string g = "";
            if (files.Count == 0) { MessageBox.Show("Please add file before encrypt!"); return; }
            for (int i = 0; i < files.Count; i++)
            {
                str.Add(files[i].ToString());
                g = str[i];
                for (int j = str[i].Length - 1; j != 0; j--)
                {
                    if (str[i][j] == '\\')
                    {
                        s = @"C:\Users\Yuriy Dazhuk\Desktop\Test\Encrypt";
                        str[i] = str[i].Remove(0, j);
                        str[i] = str[i].Insert(0, s);
                        str[i] += ".crypt";
                        s = str[i];
                        break;
                    }

                }
                EncryptFile(g, s);
                FileInfo f = new FileInfo(s);
                crypt.Add(f);
            }
            str.Clear();
            MessageBox.Show("File was encrypt!");
            Refresher(CountFilesLabel, BasicListBox, files);
            Refresher(CountCryptLabel, CryptListBox, crypt);
        }

        //Дешифрование
        private void DecryptButton_Click(object sender, EventArgs e)
        {
            string s = "";
            string g = "";
            if (crypt.Count == 0) { MessageBox.Show("Please add file before encrypt!"); return; }
            for (int i = 0; i < crypt.Count; i++)
            {
                str.Add(crypt[i].ToString());
                g = str[i];
                for (int j = str[i].Length - 1; j != 0; j--)
                {
                    if (str[i][j] == '\\')
                    {
                        s = @"C:\Users\Yuriy Dazhuk\Desktop\Test\DecompressDecrypt";
                        str[i] = str[i].Remove(0, j);
                        str[i] = str[i].Insert(0, s);
                        str[i] = str[i].Remove(str[i].Length - 6);
                        s = str[i];
                        break;
                    }

                }
                DecryptFile(g,s);
                FileInfo f = new FileInfo(str[i]);
                files.Add(f);
            }
            str.Clear();
            MessageBox.Show("File was decrypt!");
            Refresher(CountFilesLabel, BasicListBox, files);
            Refresher(CountCryptLabel, CryptListBox, crypt);
        }

        //Удаление элемента списка Crypt
        private void DeleteCryptButton_Click(object sender, EventArgs e)
        {
            GeneraDelete(CountCryptLabel, CryptListBox, crypt);
        }

        //Безвозвратная очистка списка crypt
        private void ClearCryptButton_Click(object sender, EventArgs e)
        {
            GeneralClear(CountCryptLabel, CryptListBox, crypt, 1);
        }

        //Создание архива списка crypt с расширением *.zip*
        private void ZipEncryptButton_Click(object sender, EventArgs e)
        {
            if (crypt.Count == 0) { MessageBox.Show("You must to use function -> (Encrypt) before!"); return; }
            string start = @"C:\Users\Yuriy Dazhuk\Desktop\Test\Encrypt";
            string finish = start.Replace(@"\Encrypt", "") + @"\Zip\Crypt" + ".zip";
            FileInfo check = new FileInfo(finish);
            if (check.Exists)
            {
                for (int i = finish.Length - 1; i != 0; i--)
                {
                    if (finish[i] == '.')
                    {
                        finish = finish.Insert(i, $"{CountOfZipCrypt}");
                    }
                }
            }
            ZipFile.CreateFromDirectory(start, finish);
            MessageBox.Show("Zip file was created!");
            CountOfZipCrypt++;
        }

        //Распаковка архива Crypt через OpenDialog в папку Unzip
        private void UnzipDecryptButton_Click(object sender, EventArgs e)
        {
            GeneralUnZip(CountOfZipCrypt);
        }

        //Создание архива списка files с расширением *.zip*
        private void ZipFileButton_Click(object sender, EventArgs e)
        {
            if (files.Count == 0) { MessageBox.Show("Please add file before zip!"); return; }
            string start = @"C:\Users\Yuriy Dazhuk\Desktop\Test\DecompressDecrypt";
            string finish = start.Replace(@"\DecompressDecrypt", "") + @"\Zip\File" + ".zip";
            FileInfo check = new FileInfo(finish);
            if (check.Exists)
            {
                for(int i = finish.Length - 1; i != 0; i--)
                {
                    if(finish[i] == '.')
                    {
                        finish = finish.Insert(i, $"{CountOfZipFile}");
                    }
                }
            }
            ZipFile.CreateFromDirectory(start, finish);
            MessageBox.Show("Zip file was created!");
            CountOfZipFile++;
        }

        //Безвозвратная очистка списка zip
        private void ClearZipButton_Click(object sender, EventArgs e)
        {
            GeneralClear(CountGzarLabel, CompressListBox, gzar, 1);
        }

        //Чтение сжатого файла в списке
        private void UnZipButton_Click(object sender, EventArgs e)
        {
            if (gzar.Count == 0) { MessageBox.Show("Please add zip before unzip!"); return; }
            for (int i = 0; i < gzar.Count; i++)
            {
                str.Add(gzar[i].ToString());
                str[i] = str[i].Replace("Compress", "DecompressDecrypt");
                str[i] = str[i].Remove(str[i].Length - 5);
                Decompress(gzar[i].FullName, str[i]);
                FileInfo f = new FileInfo(str[i]);
                files.Add(f);
            }
            str.Clear();
            MessageBox.Show("File was compress!");
            Refresher(CountFilesLabel, BasicListBox, files);
            Refresher(CountGzarLabel, CompressListBox, gzar);
        }

        //Главная распаковка для сокращения кода
        private void GeneralUnZip(int count)
        {
            if (count == 0) { MessageBox.Show("You must to use function -> (Zip) before!"); return; }
            OpenFileDialog opfd = new OpenFileDialog();
            opfd.Title = "Looking for *.zip*";
            if (opfd.ShowDialog(this) == DialogResult.OK)
            {
                if (opfd.FileName.EndsWith(".zip"))
                {
                    string start = opfd.FileName;
                    string finish = @"C:\Users\oleks\source\repos\oop-lab12\FilesForms\bin\Debug\Test\Unzip";
                    ZipFile.ExtractToDirectory(start, finish);
                    MessageBox.Show("Zip file was unarchived!");
                }
                else MessageBox.Show("File must be with extension *.zip*!");
            }
        }

        //Главное удаление для сокращения кода
        private void GeneraDelete(Label l, ListBox lb, List<FileInfo> listF)
        {
            if (lb.SelectedIndex == -1) { MessageBox.Show("Please select your element before remove!"); return; }
            int[] arr = lb.SelectedIndices.OfType<int>().OrderByDescending(j => j).ToArray();
            for (int j = 0; j < arr.Length; j++)
            {
                listF.RemoveAt(arr[j]);
            }
            Refresher(l, lb, listF);
        }

        //Drag&Drop of Crypt listbox
        private void CryptListBox_DragDrop(object sender, DragEventArgs e)
        {
            int count = 0;
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            for (int i = 0; i < files.Length; i++)
            {
                if (files[i].EndsWith(".crypt"))
                {
                    CryptListBox.Items.Add(files[i]);
                    FileInfo f = new FileInfo(files[i]);
                    crypt.Add(f);
                    count++;
                }
            }
            if (count == 0) MessageBox.Show("Please put files only with extension *.crypt* in the middle listbox!");
            Refresher(CountCryptLabel, CryptListBox, crypt);
        }

        //Effects of crypt listbox
        private void CryptListBox_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
        }

        //Главное очищение списков для сокращения кода
        private void GeneralClear(Label l, ListBox lb, List<FileInfo> listF, int count)
        {
            lb.Items.Clear();
            if (count == 1)
            {
                for (int i = 0; i < listF.Count; i++)
                {
                    listF[i].Delete();
                }
            }
            listF.Clear();
            MessageBox.Show("The buffer is empty!");
            Refresher(l, lb, listF);
        }

        private void FormHeadFile_Load(object sender, EventArgs e)
        {

        }

        //Шифрование
        private void EncryptFile(string inputFile, string outputFile)
        {

            try
            {
                string password = @"myKey123"; // Your Key Here
                UnicodeEncoding UE = new UnicodeEncoding();
                byte[] key = UE.GetBytes(password);

                string cryptFile = outputFile;
                FileStream fsCrypt = new FileStream(cryptFile, FileMode.Create);

                RijndaelManaged RMCrypto = new RijndaelManaged();

                CryptoStream cs = new CryptoStream(fsCrypt,
                    RMCrypto.CreateEncryptor(key, key),
                    CryptoStreamMode.Write);

                FileStream fsIn = new FileStream(inputFile, FileMode.Open);

                int data;
                while ((data = fsIn.ReadByte()) != -1)
                    cs.WriteByte((byte)data);


                fsIn.Close();
                cs.Close();
                fsCrypt.Close();
            }
            catch
            {
                MessageBox.Show("Encryption failed!", "Error");
            }
        }

        //Дешифрование
        private void DecryptFile(string inputFile, string outputFile)
        {
            string password = @"myKey123"; // Your Key Here

            UnicodeEncoding UE = new UnicodeEncoding();
            byte[] key = UE.GetBytes(password);

            FileStream fsCrypt = new FileStream(inputFile, FileMode.Open);

            RijndaelManaged RMCrypto = new RijndaelManaged();

            CryptoStream cs = new CryptoStream(fsCrypt,
                RMCrypto.CreateDecryptor(key, key),
                CryptoStreamMode.Read);

            FileStream fsOut = new FileStream(outputFile, FileMode.Create);

            int data;
            while ((data = cs.ReadByte()) != -1)
                fsOut.WriteByte((byte)data);

            fsOut.Close();
            cs.Close();
            fsCrypt.Close();
        }
    }
}
