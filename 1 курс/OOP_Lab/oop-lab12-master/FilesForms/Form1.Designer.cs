﻿namespace FilesForms
{
    partial class FormHeadFile
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormHeadFile));
            this.BasicListBox = new System.Windows.Forms.ListBox();
            this.AddButton = new System.Windows.Forms.Button();
            this.DeleteFileButton = new System.Windows.Forms.Button();
            this.ClearFileButton = new System.Windows.Forms.Button();
            this.ZipButton = new System.Windows.Forms.Button();
            this.UnZipButton = new System.Windows.Forms.Button();
            this.EncryptButton = new System.Windows.Forms.Button();
            this.DecryptButton = new System.Windows.Forms.Button();
            this.CountFilesLabel = new System.Windows.Forms.Label();
            this.FilesLabel = new System.Windows.Forms.Label();
            this.CompressListBox = new System.Windows.Forms.ListBox();
            this.GzarsLabel = new System.Windows.Forms.Label();
            this.CountGzarLabel = new System.Windows.Forms.Label();
            this.CryptListBox = new System.Windows.Forms.ListBox();
            this.FilesssLabel = new System.Windows.Forms.Label();
            this.CountCryptLabel = new System.Windows.Forms.Label();
            this.ClearZipButton = new System.Windows.Forms.Button();
            this.DeleteZipButton = new System.Windows.Forms.Button();
            this.ClearCryptButton = new System.Windows.Forms.Button();
            this.DeleteCryptButton = new System.Windows.Forms.Button();
            this.ListNameLabel = new System.Windows.Forms.Label();
            this.GzarNameLabel = new System.Windows.Forms.Label();
            this.CryptNameLabel = new System.Windows.Forms.Label();
            this.ZipGzarButton = new System.Windows.Forms.Button();
            this.UnZipGzarButton = new System.Windows.Forms.Button();
            this.UnZipFileButton = new System.Windows.Forms.Button();
            this.ZipFileButton = new System.Windows.Forms.Button();
            this.UnzipDecryptButton = new System.Windows.Forms.Button();
            this.ZipEncryptButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BasicListBox
            // 
            this.BasicListBox.AllowDrop = true;
            this.BasicListBox.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BasicListBox.FormattingEnabled = true;
            this.BasicListBox.HorizontalScrollbar = true;
            this.BasicListBox.ItemHeight = 14;
            this.BasicListBox.Location = new System.Drawing.Point(16, 42);
            this.BasicListBox.Name = "BasicListBox";
            this.BasicListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.BasicListBox.Size = new System.Drawing.Size(250, 312);
            this.BasicListBox.TabIndex = 1;
            this.BasicListBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.BasicListBox_DragDrop);
            this.BasicListBox.DragEnter += new System.Windows.Forms.DragEventHandler(this.BasicListBox_DragEnter);
            // 
            // AddButton
            // 
            this.AddButton.BackColor = System.Drawing.Color.Ivory;
            this.AddButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddButton.Location = new System.Drawing.Point(272, 42);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(110, 35);
            this.AddButton.TabIndex = 2;
            this.AddButton.Text = "Add";
            this.AddButton.UseVisualStyleBackColor = false;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // DeleteFileButton
            // 
            this.DeleteFileButton.BackColor = System.Drawing.Color.Ivory;
            this.DeleteFileButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DeleteFileButton.Location = new System.Drawing.Point(272, 83);
            this.DeleteFileButton.Name = "DeleteFileButton";
            this.DeleteFileButton.Size = new System.Drawing.Size(110, 35);
            this.DeleteFileButton.TabIndex = 3;
            this.DeleteFileButton.Text = "Delete";
            this.DeleteFileButton.UseVisualStyleBackColor = false;
            this.DeleteFileButton.Click += new System.EventHandler(this.DeleteFileButton_Click_1);
            // 
            // ClearFileButton
            // 
            this.ClearFileButton.BackColor = System.Drawing.Color.Ivory;
            this.ClearFileButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ClearFileButton.Location = new System.Drawing.Point(272, 124);
            this.ClearFileButton.Name = "ClearFileButton";
            this.ClearFileButton.Size = new System.Drawing.Size(110, 35);
            this.ClearFileButton.TabIndex = 4;
            this.ClearFileButton.Text = "Clear";
            this.ClearFileButton.UseVisualStyleBackColor = false;
            this.ClearFileButton.Click += new System.EventHandler(this.ClearFileButton_Click);
            // 
            // ZipButton
            // 
            this.ZipButton.BackColor = System.Drawing.Color.Ivory;
            this.ZipButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ZipButton.Location = new System.Drawing.Point(644, 42);
            this.ZipButton.Name = "ZipButton";
            this.ZipButton.Size = new System.Drawing.Size(110, 35);
            this.ZipButton.TabIndex = 5;
            this.ZipButton.Text = "Compress";
            this.ZipButton.UseVisualStyleBackColor = false;
            this.ZipButton.Click += new System.EventHandler(this.ZipButton_Click);
            // 
            // UnZipButton
            // 
            this.UnZipButton.BackColor = System.Drawing.Color.Ivory;
            this.UnZipButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.UnZipButton.Location = new System.Drawing.Point(644, 83);
            this.UnZipButton.Name = "UnZipButton";
            this.UnZipButton.Size = new System.Drawing.Size(110, 35);
            this.UnZipButton.TabIndex = 6;
            this.UnZipButton.Text = "Decompress";
            this.UnZipButton.UseVisualStyleBackColor = false;
            this.UnZipButton.Click += new System.EventHandler(this.UnZipButton_Click);
            // 
            // EncryptButton
            // 
            this.EncryptButton.BackColor = System.Drawing.Color.Ivory;
            this.EncryptButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EncryptButton.Location = new System.Drawing.Point(1016, 42);
            this.EncryptButton.Name = "EncryptButton";
            this.EncryptButton.Size = new System.Drawing.Size(110, 35);
            this.EncryptButton.TabIndex = 7;
            this.EncryptButton.Text = "Encrypt";
            this.EncryptButton.UseVisualStyleBackColor = false;
            this.EncryptButton.Click += new System.EventHandler(this.EncryptButton_Click);
            // 
            // DecryptButton
            // 
            this.DecryptButton.BackColor = System.Drawing.Color.Ivory;
            this.DecryptButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DecryptButton.Location = new System.Drawing.Point(1016, 83);
            this.DecryptButton.Name = "DecryptButton";
            this.DecryptButton.Size = new System.Drawing.Size(110, 35);
            this.DecryptButton.TabIndex = 8;
            this.DecryptButton.Text = "Decrypt";
            this.DecryptButton.UseVisualStyleBackColor = false;
            this.DecryptButton.Click += new System.EventHandler(this.DecryptButton_Click);
            // 
            // CountFilesLabel
            // 
            this.CountFilesLabel.AutoSize = true;
            this.CountFilesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CountFilesLabel.Location = new System.Drawing.Point(325, 15);
            this.CountFilesLabel.Name = "CountFilesLabel";
            this.CountFilesLabel.Size = new System.Drawing.Size(20, 24);
            this.CountFilesLabel.TabIndex = 11;
            this.CountFilesLabel.Text = "0";
            // 
            // FilesLabel
            // 
            this.FilesLabel.AutoSize = true;
            this.FilesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FilesLabel.Location = new System.Drawing.Point(264, 15);
            this.FilesLabel.Name = "FilesLabel";
            this.FilesLabel.Size = new System.Drawing.Size(55, 24);
            this.FilesLabel.TabIndex = 12;
            this.FilesLabel.Text = "Files:";
            // 
            // CompressListBox
            // 
            this.CompressListBox.AllowDrop = true;
            this.CompressListBox.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CompressListBox.FormattingEnabled = true;
            this.CompressListBox.HorizontalScrollbar = true;
            this.CompressListBox.ItemHeight = 14;
            this.CompressListBox.Location = new System.Drawing.Point(388, 42);
            this.CompressListBox.Name = "CompressListBox";
            this.CompressListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.CompressListBox.Size = new System.Drawing.Size(250, 312);
            this.CompressListBox.TabIndex = 13;
            this.CompressListBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.CompressListBox_DragDrop);
            this.CompressListBox.DragEnter += new System.Windows.Forms.DragEventHandler(this.CompressListBox_DragEnter);
            // 
            // GzarsLabel
            // 
            this.GzarsLabel.AutoSize = true;
            this.GzarsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GzarsLabel.Location = new System.Drawing.Point(640, 15);
            this.GzarsLabel.Name = "GzarsLabel";
            this.GzarsLabel.Size = new System.Drawing.Size(55, 24);
            this.GzarsLabel.TabIndex = 15;
            this.GzarsLabel.Text = "Files:";
            // 
            // CountGzarLabel
            // 
            this.CountGzarLabel.AutoSize = true;
            this.CountGzarLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CountGzarLabel.Location = new System.Drawing.Point(701, 15);
            this.CountGzarLabel.Name = "CountGzarLabel";
            this.CountGzarLabel.Size = new System.Drawing.Size(20, 24);
            this.CountGzarLabel.TabIndex = 14;
            this.CountGzarLabel.Text = "0";
            // 
            // CryptListBox
            // 
            this.CryptListBox.AllowDrop = true;
            this.CryptListBox.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CryptListBox.FormattingEnabled = true;
            this.CryptListBox.HorizontalScrollbar = true;
            this.CryptListBox.ItemHeight = 14;
            this.CryptListBox.Location = new System.Drawing.Point(760, 42);
            this.CryptListBox.Name = "CryptListBox";
            this.CryptListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.CryptListBox.Size = new System.Drawing.Size(250, 312);
            this.CryptListBox.TabIndex = 16;
            this.CryptListBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.CryptListBox_DragDrop);
            this.CryptListBox.DragEnter += new System.Windows.Forms.DragEventHandler(this.CryptListBox_DragEnter);
            // 
            // FilesssLabel
            // 
            this.FilesssLabel.AutoSize = true;
            this.FilesssLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FilesssLabel.Location = new System.Drawing.Point(1012, 15);
            this.FilesssLabel.Name = "FilesssLabel";
            this.FilesssLabel.Size = new System.Drawing.Size(55, 24);
            this.FilesssLabel.TabIndex = 18;
            this.FilesssLabel.Text = "Files:";
            // 
            // CountCryptLabel
            // 
            this.CountCryptLabel.AutoSize = true;
            this.CountCryptLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CountCryptLabel.Location = new System.Drawing.Point(1073, 15);
            this.CountCryptLabel.Name = "CountCryptLabel";
            this.CountCryptLabel.Size = new System.Drawing.Size(20, 24);
            this.CountCryptLabel.TabIndex = 17;
            this.CountCryptLabel.Text = "0";
            // 
            // ClearZipButton
            // 
            this.ClearZipButton.BackColor = System.Drawing.Color.Ivory;
            this.ClearZipButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ClearZipButton.Location = new System.Drawing.Point(644, 165);
            this.ClearZipButton.Name = "ClearZipButton";
            this.ClearZipButton.Size = new System.Drawing.Size(110, 35);
            this.ClearZipButton.TabIndex = 20;
            this.ClearZipButton.Text = "Clear";
            this.ClearZipButton.UseVisualStyleBackColor = false;
            this.ClearZipButton.Click += new System.EventHandler(this.ClearZipButton_Click);
            // 
            // DeleteZipButton
            // 
            this.DeleteZipButton.BackColor = System.Drawing.Color.Ivory;
            this.DeleteZipButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DeleteZipButton.Location = new System.Drawing.Point(644, 124);
            this.DeleteZipButton.Name = "DeleteZipButton";
            this.DeleteZipButton.Size = new System.Drawing.Size(110, 35);
            this.DeleteZipButton.TabIndex = 19;
            this.DeleteZipButton.Text = "Delete";
            this.DeleteZipButton.UseVisualStyleBackColor = false;
            this.DeleteZipButton.Click += new System.EventHandler(this.DeleteZipButton_Click);
            // 
            // ClearCryptButton
            // 
            this.ClearCryptButton.BackColor = System.Drawing.Color.Ivory;
            this.ClearCryptButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ClearCryptButton.Location = new System.Drawing.Point(1016, 165);
            this.ClearCryptButton.Name = "ClearCryptButton";
            this.ClearCryptButton.Size = new System.Drawing.Size(110, 35);
            this.ClearCryptButton.TabIndex = 22;
            this.ClearCryptButton.Text = "Clear";
            this.ClearCryptButton.UseVisualStyleBackColor = false;
            this.ClearCryptButton.Click += new System.EventHandler(this.ClearCryptButton_Click);
            // 
            // DeleteCryptButton
            // 
            this.DeleteCryptButton.BackColor = System.Drawing.Color.Ivory;
            this.DeleteCryptButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DeleteCryptButton.Location = new System.Drawing.Point(1016, 124);
            this.DeleteCryptButton.Name = "DeleteCryptButton";
            this.DeleteCryptButton.Size = new System.Drawing.Size(110, 35);
            this.DeleteCryptButton.TabIndex = 21;
            this.DeleteCryptButton.Text = "Delete";
            this.DeleteCryptButton.UseVisualStyleBackColor = false;
            this.DeleteCryptButton.Click += new System.EventHandler(this.DeleteCryptButton_Click);
            // 
            // ListNameLabel
            // 
            this.ListNameLabel.AutoSize = true;
            this.ListNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ListNameLabel.Location = new System.Drawing.Point(12, 15);
            this.ListNameLabel.Name = "ListNameLabel";
            this.ListNameLabel.Size = new System.Drawing.Size(94, 24);
            this.ListNameLabel.TabIndex = 23;
            this.ListNameLabel.Text = "List of files";
            // 
            // GzarNameLabel
            // 
            this.GzarNameLabel.AutoSize = true;
            this.GzarNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GzarNameLabel.Location = new System.Drawing.Point(384, 15);
            this.GzarNameLabel.Name = "GzarNameLabel";
            this.GzarNameLabel.Size = new System.Drawing.Size(107, 24);
            this.GzarNameLabel.TabIndex = 24;
            this.GzarNameLabel.Text = "List of gzars";
            // 
            // CryptNameLabel
            // 
            this.CryptNameLabel.AutoSize = true;
            this.CryptNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CryptNameLabel.Location = new System.Drawing.Point(756, 15);
            this.CryptNameLabel.Name = "CryptNameLabel";
            this.CryptNameLabel.Size = new System.Drawing.Size(111, 24);
            this.CryptNameLabel.TabIndex = 25;
            this.CryptNameLabel.Text = "List of crypts";
            // 
            // ZipGzarButton
            // 
            this.ZipGzarButton.BackColor = System.Drawing.Color.Ivory;
            this.ZipGzarButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ZipGzarButton.Location = new System.Drawing.Point(644, 208);
            this.ZipGzarButton.Name = "ZipGzarButton";
            this.ZipGzarButton.Size = new System.Drawing.Size(110, 35);
            this.ZipGzarButton.TabIndex = 26;
            this.ZipGzarButton.Text = "Zip";
            this.ZipGzarButton.UseVisualStyleBackColor = false;
            this.ZipGzarButton.Click += new System.EventHandler(this.ZipGzarButton_Click);
            // 
            // UnZipGzarButton
            // 
            this.UnZipGzarButton.BackColor = System.Drawing.Color.Ivory;
            this.UnZipGzarButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.UnZipGzarButton.Location = new System.Drawing.Point(644, 249);
            this.UnZipGzarButton.Name = "UnZipGzarButton";
            this.UnZipGzarButton.Size = new System.Drawing.Size(110, 35);
            this.UnZipGzarButton.TabIndex = 27;
            this.UnZipGzarButton.Text = "Unzip";
            this.UnZipGzarButton.UseVisualStyleBackColor = false;
            this.UnZipGzarButton.Click += new System.EventHandler(this.UnZipGzarButton_Click);
            // 
            // UnZipFileButton
            // 
            this.UnZipFileButton.BackColor = System.Drawing.Color.Ivory;
            this.UnZipFileButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.UnZipFileButton.Location = new System.Drawing.Point(272, 206);
            this.UnZipFileButton.Name = "UnZipFileButton";
            this.UnZipFileButton.Size = new System.Drawing.Size(110, 35);
            this.UnZipFileButton.TabIndex = 29;
            this.UnZipFileButton.Text = "Unzip";
            this.UnZipFileButton.UseVisualStyleBackColor = false;
            this.UnZipFileButton.Click += new System.EventHandler(this.UnZipFileButton_Click);
            // 
            // ZipFileButton
            // 
            this.ZipFileButton.BackColor = System.Drawing.Color.Ivory;
            this.ZipFileButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ZipFileButton.Location = new System.Drawing.Point(272, 165);
            this.ZipFileButton.Name = "ZipFileButton";
            this.ZipFileButton.Size = new System.Drawing.Size(110, 35);
            this.ZipFileButton.TabIndex = 28;
            this.ZipFileButton.Text = "Zip";
            this.ZipFileButton.UseVisualStyleBackColor = false;
            this.ZipFileButton.Click += new System.EventHandler(this.ZipFileButton_Click);
            // 
            // UnzipDecryptButton
            // 
            this.UnzipDecryptButton.BackColor = System.Drawing.Color.Ivory;
            this.UnzipDecryptButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.UnzipDecryptButton.Location = new System.Drawing.Point(1016, 249);
            this.UnzipDecryptButton.Name = "UnzipDecryptButton";
            this.UnzipDecryptButton.Size = new System.Drawing.Size(110, 35);
            this.UnzipDecryptButton.TabIndex = 31;
            this.UnzipDecryptButton.Text = "Unzip";
            this.UnzipDecryptButton.UseVisualStyleBackColor = false;
            this.UnzipDecryptButton.Click += new System.EventHandler(this.UnzipDecryptButton_Click);
            // 
            // ZipEncryptButton
            // 
            this.ZipEncryptButton.BackColor = System.Drawing.Color.Ivory;
            this.ZipEncryptButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ZipEncryptButton.Location = new System.Drawing.Point(1016, 208);
            this.ZipEncryptButton.Name = "ZipEncryptButton";
            this.ZipEncryptButton.Size = new System.Drawing.Size(110, 35);
            this.ZipEncryptButton.TabIndex = 30;
            this.ZipEncryptButton.Text = "Zip";
            this.ZipEncryptButton.UseVisualStyleBackColor = false;
            this.ZipEncryptButton.Click += new System.EventHandler(this.ZipEncryptButton_Click);
            // 
            // FormHeadFile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightYellow;
            this.ClientSize = new System.Drawing.Size(1134, 361);
            this.Controls.Add(this.UnzipDecryptButton);
            this.Controls.Add(this.ZipEncryptButton);
            this.Controls.Add(this.UnZipFileButton);
            this.Controls.Add(this.ZipFileButton);
            this.Controls.Add(this.UnZipGzarButton);
            this.Controls.Add(this.ZipGzarButton);
            this.Controls.Add(this.CryptNameLabel);
            this.Controls.Add(this.GzarNameLabel);
            this.Controls.Add(this.ListNameLabel);
            this.Controls.Add(this.ClearCryptButton);
            this.Controls.Add(this.DeleteCryptButton);
            this.Controls.Add(this.ClearZipButton);
            this.Controls.Add(this.DeleteZipButton);
            this.Controls.Add(this.FilesssLabel);
            this.Controls.Add(this.CountCryptLabel);
            this.Controls.Add(this.CryptListBox);
            this.Controls.Add(this.GzarsLabel);
            this.Controls.Add(this.CountGzarLabel);
            this.Controls.Add(this.CompressListBox);
            this.Controls.Add(this.FilesLabel);
            this.Controls.Add(this.CountFilesLabel);
            this.Controls.Add(this.DecryptButton);
            this.Controls.Add(this.EncryptButton);
            this.Controls.Add(this.UnZipButton);
            this.Controls.Add(this.ZipButton);
            this.Controls.Add(this.ClearFileButton);
            this.Controls.Add(this.DeleteFileButton);
            this.Controls.Add(this.AddButton);
            this.Controls.Add(this.BasicListBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1150, 400);
            this.MinimumSize = new System.Drawing.Size(1150, 400);
            this.Name = "FormHeadFile";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Files";
            this.Load += new System.EventHandler(this.FormHeadFile_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ListBox BasicListBox;
        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.Button DeleteFileButton;
        private System.Windows.Forms.Button ClearFileButton;
        private System.Windows.Forms.Button ZipButton;
        private System.Windows.Forms.Button UnZipButton;
        private System.Windows.Forms.Button EncryptButton;
        private System.Windows.Forms.Button DecryptButton;
        private System.Windows.Forms.Label CountFilesLabel;
        private System.Windows.Forms.Label FilesLabel;
        private System.Windows.Forms.ListBox CompressListBox;
        private System.Windows.Forms.Label GzarsLabel;
        private System.Windows.Forms.Label CountGzarLabel;
        private System.Windows.Forms.ListBox CryptListBox;
        private System.Windows.Forms.Label FilesssLabel;
        private System.Windows.Forms.Label CountCryptLabel;
        private System.Windows.Forms.Button ClearZipButton;
        private System.Windows.Forms.Button DeleteZipButton;
        private System.Windows.Forms.Button ClearCryptButton;
        private System.Windows.Forms.Button DeleteCryptButton;
        private System.Windows.Forms.Label ListNameLabel;
        private System.Windows.Forms.Label GzarNameLabel;
        private System.Windows.Forms.Label CryptNameLabel;
        private System.Windows.Forms.Button ZipGzarButton;
        private System.Windows.Forms.Button UnZipGzarButton;
        private System.Windows.Forms.Button UnZipFileButton;
        private System.Windows.Forms.Button ZipFileButton;
        private System.Windows.Forms.Button UnzipDecryptButton;
        private System.Windows.Forms.Button ZipEncryptButton;
    }
}

