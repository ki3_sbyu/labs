﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static double ReadNum()
        {
            st1:
            double num;
            bool isOk = double.TryParse(Console.ReadLine(), out num);
            if(isOk)
            {
                return num;
            }
            else
            {
                Console.WriteLine("Неправильно введено число, будь ласка повторіть спробу.");
                goto st1;
            }
        }
        static void Main(string[] args)
        { //розділювач дробових чисел
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            //підтримка укр.мови
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Console.WriteLine("Лабораторна робота №2.\nВиконав: Стецюк Б.Ю., група КІ-3\nВаріант №8\nЗавдання 1.\n");
            Console.WriteLine("x = ");
            double x = ReadNum();
            Console.WriteLine("y = ");
            double y = ReadNum();
            Console.WriteLine("z = ");
            double z = ReadNum();
            double part1, part2, part3, result;
            part1 = Math.Exp(z + y) * Math.Pow(z - y, x + z);
            part2 = Math.Sin(x) + Math.Sin(y);
            part3 = Math.Pow((Math.Pow(x, 7) + Math.Log(y)), 1/4);
            result = part1 / part2 + part3;
            Console.WriteLine("Результат обчислення: s = {0:F3}", result);
        }
    }
}
