﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static double ReadNum()
        {
            st1:
            double num;
            bool isOk = double.TryParse(Console.ReadLine(), out num);
            if (isOk)
            {
                return num;
            }
            else
            {
                Console.WriteLine("Неправильно введено число, будь ласка повторіть спробу.");
                goto st1;
            }
        }
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
   System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            //підтримка укр.мови
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Console.WriteLine("Лабораторна робота №2.\nВиконав: Стецюк Б.Ю., група КІ-3\nВаріант №8\nЗавдання 3.\n");

            Console.WriteLine("Введіть число N: ");
            int n = Convert.ToInt32(ReadNum());
            double summ=0, res;
            for(int i = n;i>0;i--)
            {
                res = Math.Pow((n + 1 - i), i);
                summ += res;
            }
            Console.WriteLine("Результат: {0}", Convert.ToDouble(summ));
        }
    }
}
