﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace quadWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            labX1.Visibility = Visibility.Collapsed;
            labX2.Visibility = Visibility.Collapsed;
            labX.Visibility = Visibility.Collapsed;
            textX1.Visibility = Visibility.Collapsed;
            textX2.Visibility = Visibility.Collapsed;
        }
        static void MesErr()
        {
            MessageBox.Show("Будь ласка, перевірте введені дані.", "Помилка", MessageBoxButton.OK,MessageBoxImage.Information);
        }
        private void calcQuad_Click(object sender, RoutedEventArgs e)
        {
            double a, b, c, x1, x2, D;
            bool isOk1 = double.TryParse(textA.Text, out a);
            bool isOk2 = double.TryParse(textB.Text, out b);
            bool isOk3 = double.TryParse(textC.Text, out c);
            if (!isOk1 || !isOk2 || !isOk3)
                MesErr();
            else
            {
                D = b * b - 4 * a * c;
                if (D > 0)
                {
                    x1 = (-b + Math.Sqrt(D)) / (2 * a);
                    x2 = (-b - Math.Sqrt(D)) / (2 * a);
                    labX1.Visibility = Visibility.Visible;
                    labX2.Visibility = Visibility.Visible;
                    labX.Visibility = Visibility.Collapsed;
                    textX1.Visibility = Visibility.Visible;
                    textX2.Visibility = Visibility.Visible;
                    textX1.Text = x1.ToString("F2");
                    textX2.Text = x2.ToString("F2");
                }
                else if (D == 0)
                {
                    x1 = -b / (2 * a);
                    textX1.Text = x1.ToString();
                    labX1.Visibility = Visibility.Collapsed;
                    labX2.Visibility = Visibility.Collapsed;
                    labX.Visibility = Visibility.Visible;
                    textX1.Visibility = Visibility.Visible;
                    textX2.Visibility = Visibility.Collapsed;
                }
                else
                {
                    labX1.Visibility = Visibility.Collapsed;
                    labX2.Visibility = Visibility.Collapsed;
                    labX.Visibility = Visibility.Collapsed;
                    textX1.Visibility = Visibility.Collapsed;
                    textX2.Visibility = Visibility.Collapsed;
                    MessageBox.Show("Рівняння не має коренів.", "Результат");
                }
            }
        }
    }
}
