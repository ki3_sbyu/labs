﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFApp
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
   System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }

        private void textX_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void calc_Click(object sender, RoutedEventArgs e)
        {
            double x, y, z, s, part1, part2, part3;
            bool isOk1 = double.TryParse(textX.Text, out x);
            bool isOk2 = double.TryParse(textY.Text, out y);
            bool isOk3 = double.TryParse(textZ.Text, out z);
            if (!isOk1 || !isOk2 || !isOk3)
            {
                MessageBox.Show("Будь ласка, перевірте введені дані.", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                part1 = Math.Exp(z + y) * Math.Pow(z - y, x + z);
                part2 = Math.Sin(x) + Math.Sin(y);
                part3 = Math.Pow((Math.Pow(x, 7) + Math.Log(y)), 1 / 4);
                s = part1 / part2 + part3;
                textS.Text = s.ToString("F2");
            }
        }
    }
}
