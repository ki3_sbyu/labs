﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

    private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
               System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }
        static void MesErr()
        {
            MessageBox.Show("Будь ласка, перевірте введені дані.", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        private void Calc_Click(object sender, EventArgs e)
        {
            double x, y, z, s, part1, part2, part3;
            bool isOk1 = double.TryParse(textBoxX.Text, out x);
            bool isOk2 = double.TryParse(textBoxY.Text, out y);
            bool isOk3 = double.TryParse(textBoxZ.Text, out z);
            if (!isOk1 || !isOk2 || !isOk3)
                MesErr();
            else
            {
                part1 = Math.Exp(z + y) * Math.Pow(z - y, x + z);
                part2 = Math.Sin(x) + Math.Sin(y);
                part3 = Math.Pow((Math.Pow(x, 7) + Math.Log(y)), 1 / 4);
                s = part1 / part2 + part3;
                textBoxS.Text = s.ToString("F2");
            }
        }
    }
}
