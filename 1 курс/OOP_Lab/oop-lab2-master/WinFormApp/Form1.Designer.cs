﻿namespace WinFormApp
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.textX = new System.Windows.Forms.Label();
            this.textBoxX = new System.Windows.Forms.TextBox();
            this.textY = new System.Windows.Forms.Label();
            this.textBoxY = new System.Windows.Forms.TextBox();
            this.TextZ = new System.Windows.Forms.Label();
            this.textBoxZ = new System.Windows.Forms.TextBox();
            this.Calc = new System.Windows.Forms.Button();
            this.TextS = new System.Windows.Forms.Label();
            this.textBoxS = new System.Windows.Forms.TextBox();
            this.linkAut = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // textX
            // 
            this.textX.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textX.AutoSize = true;
            this.textX.Location = new System.Drawing.Point(13, 9);
            this.textX.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.textX.Name = "textX";
            this.textX.Size = new System.Drawing.Size(35, 23);
            this.textX.TabIndex = 0;
            this.textX.Text = "x = ";
            this.textX.Click += new System.EventHandler(this.label1_Click);
            // 
            // textBoxX
            // 
            this.textBoxX.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxX.Location = new System.Drawing.Point(73, 9);
            this.textBoxX.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBoxX.Name = "textBoxX";
            this.textBoxX.Size = new System.Drawing.Size(189, 29);
            this.textBoxX.TabIndex = 1;
            this.textBoxX.WordWrap = false;
            // 
            // textY
            // 
            this.textY.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textY.AutoSize = true;
            this.textY.Location = new System.Drawing.Point(13, 60);
            this.textY.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.textY.Name = "textY";
            this.textY.Size = new System.Drawing.Size(30, 23);
            this.textY.TabIndex = 2;
            this.textY.Text = "y =";
            // 
            // textBoxY
            // 
            this.textBoxY.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxY.Location = new System.Drawing.Point(75, 57);
            this.textBoxY.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBoxY.Name = "textBoxY";
            this.textBoxY.Size = new System.Drawing.Size(187, 29);
            this.textBoxY.TabIndex = 3;
            this.textBoxY.WordWrap = false;
            // 
            // TextZ
            // 
            this.TextZ.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.TextZ.AutoSize = true;
            this.TextZ.Location = new System.Drawing.Point(13, 112);
            this.TextZ.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.TextZ.Name = "TextZ";
            this.TextZ.Size = new System.Drawing.Size(31, 23);
            this.TextZ.TabIndex = 4;
            this.TextZ.Text = "z =";
            this.TextZ.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // textBoxZ
            // 
            this.textBoxZ.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxZ.Location = new System.Drawing.Point(75, 109);
            this.textBoxZ.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBoxZ.Name = "textBoxZ";
            this.textBoxZ.Size = new System.Drawing.Size(187, 29);
            this.textBoxZ.TabIndex = 5;
            this.textBoxZ.WordWrap = false;
            // 
            // Calc
            // 
            this.Calc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Calc.BackColor = System.Drawing.Color.LightCyan;
            this.Calc.Location = new System.Drawing.Point(75, 163);
            this.Calc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Calc.Name = "Calc";
            this.Calc.Size = new System.Drawing.Size(189, 35);
            this.Calc.TabIndex = 6;
            this.Calc.Text = "Обчислити";
            this.Calc.UseVisualStyleBackColor = false;
            this.Calc.Click += new System.EventHandler(this.Calc_Click);
            // 
            // TextS
            // 
            this.TextS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.TextS.AutoSize = true;
            this.TextS.Location = new System.Drawing.Point(13, 225);
            this.TextS.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.TextS.Name = "TextS";
            this.TextS.Size = new System.Drawing.Size(31, 23);
            this.TextS.TabIndex = 7;
            this.TextS.Text = "s =";
            // 
            // textBoxS
            // 
            this.textBoxS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxS.BackColor = System.Drawing.Color.White;
            this.textBoxS.ForeColor = System.Drawing.SystemColors.MenuText;
            this.textBoxS.HideSelection = false;
            this.textBoxS.Location = new System.Drawing.Point(75, 222);
            this.textBoxS.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBoxS.Name = "textBoxS";
            this.textBoxS.ReadOnly = true;
            this.textBoxS.Size = new System.Drawing.Size(187, 29);
            this.textBoxS.TabIndex = 8;
            this.textBoxS.Text = "0";
            this.textBoxS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxS.WordWrap = false;
            // 
            // linkAut
            // 
            this.linkAut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.linkAut.AutoSize = true;
            this.linkAut.Cursor = System.Windows.Forms.Cursors.Cross;
            this.linkAut.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this.linkAut.Location = new System.Drawing.Point(112, 279);
            this.linkAut.Name = "linkAut";
            this.linkAut.Size = new System.Drawing.Size(160, 23);
            this.linkAut.TabIndex = 10;
            this.linkAut.TabStop = true;
            this.linkAut.Text = "Стецюк Б.Ю., гр. КІ-3";
            this.linkAut.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Moccasin;
            this.ClientSize = new System.Drawing.Size(284, 311);
            this.Controls.Add(this.linkAut);
            this.Controls.Add(this.textBoxS);
            this.Controls.Add(this.TextS);
            this.Controls.Add(this.Calc);
            this.Controls.Add(this.textBoxZ);
            this.Controls.Add(this.TextZ);
            this.Controls.Add(this.textBoxY);
            this.Controls.Add(this.textY);
            this.Controls.Add(this.textBoxX);
            this.Controls.Add(this.textX);
            this.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(500, 500);
            this.MinimumSize = new System.Drawing.Size(300, 350);
            this.Name = "Form1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Лабораторна робота №2";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label textX;
        private System.Windows.Forms.TextBox textBoxX;
        private System.Windows.Forms.Label textY;
        private System.Windows.Forms.TextBox textBoxY;
        private System.Windows.Forms.Label TextZ;
        private System.Windows.Forms.TextBox textBoxZ;
        private System.Windows.Forms.Button Calc;
        private System.Windows.Forms.Label TextS;
        private System.Windows.Forms.TextBox textBoxS;
        private System.Windows.Forms.LinkLabel linkAut;
    }
}

