﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static double ReadNum()
        {
            st1:
            double num;
            bool isOk = double.TryParse(Console.ReadLine(), out num);
            if (isOk)
            {
                return num;
            }
            else
            {
                Console.WriteLine("Неправильно введено число, будь ласка повторіть спробу.");
                goto st1;
            }
        }
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
    System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            //підтримка укр.мови
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Console.WriteLine("Лабораторна робота №2.\nВиконав: Стецюк Б.Ю., група КІ-3\nВаріант №8\nЗавдання 3.\n");
            Console.WriteLine("Почергово введіть числа, для завершення введіть 0 [максимум 100 чисел]");
            int n = 100, even=0, odd=0, pos=0, neg=0;
            int[] array = new int[n];
            for(int i=0;i<n;i++)
            {
                array[i] = Convert.ToInt32(ReadNum());
                if(array[i]==0)
                {
                    i = n;
                    goto exitCycle;
                }
                if (array[i] % 2 == 0&&array[i]!=0)
                    even++;
                else odd++;
                if (array[i] > 0)
                    pos++;
                else neg++;
                exitCycle:;
            }
            Console.WriteLine("Парних - {0}\nНепарних - {1}\nДодатніх - {2}\nВід'ємних - {3}", even, odd, pos, neg);
        }
    }
}
