﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace quad
{
    class Program
    {
        static double ReadNum()
        {
            st1:
            double num;
            bool isOk = double.TryParse(Console.ReadLine(), out num);
            if (isOk)
            {
                return num;
            }
            else
            {
                Console.WriteLine("Неправильно введено число, будь ласка повторіть спробу.");
                goto st1;
            }
        }
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
    System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            //підтримка укр.мови
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Console.WriteLine("Лабораторна робота №2.\nВиконав: Стецюк Б.Ю., група КІ-3\nВаріант №8\nЗавдання 2.\n");
            double a, b, c, D, x1, x2, menu;
            do
            {
                Console.Clear();
                Console.WriteLine("Введіть числа:\na = ");
                a = ReadNum();
                Console.WriteLine("b = ");
                b = ReadNum();
                Console.WriteLine("c = ");
                c = ReadNum();
                D = b * b - 4 * a * c;
                if(D>0)
                {
                    x1 = (-b + Math.Sqrt(D)) / (2 * a);
                    x2 = (-b - Math.Sqrt(D)) / (2 * a);
                    Console.WriteLine("x1 == {0}\nx2 == {1}", x1, x2);
                }
                else if(D == 0)
                {
                    x1 = x1 = -b / (2 * a);
                    Console.WriteLine("x == {0}", x1);
                }
                else
                {
                    Console.WriteLine("Рівняння коренів немає.");
                }
                Console.WriteLine("1 - повторити ввід, інше число - вихід з програми");
                menu = ReadNum();
            } while (menu == 1);

        }
    }
}
