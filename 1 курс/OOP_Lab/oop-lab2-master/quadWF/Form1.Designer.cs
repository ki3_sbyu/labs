﻿namespace quadWF
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.BoxA = new System.Windows.Forms.TextBox();
            this.numA = new System.Windows.Forms.Label();
            this.numB = new System.Windows.Forms.Label();
            this.BoxB = new System.Windows.Forms.TextBox();
            this.numC = new System.Windows.Forms.Label();
            this.BoxC = new System.Windows.Forms.TextBox();
            this.calcQuad = new System.Windows.Forms.Button();
            this.labX1 = new System.Windows.Forms.Label();
            this.ResX1 = new System.Windows.Forms.TextBox();
            this.labX2 = new System.Windows.Forms.Label();
            this.ResX2 = new System.Windows.Forms.TextBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.labX = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BoxA
            // 
            this.BoxA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.BoxA.Location = new System.Drawing.Point(68, 8);
            this.BoxA.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BoxA.Name = "BoxA";
            this.BoxA.ShortcutsEnabled = false;
            this.BoxA.Size = new System.Drawing.Size(138, 29);
            this.BoxA.TabIndex = 0;
            this.BoxA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.BoxA.TextChanged += new System.EventHandler(this.BoxA_TextChanged);
            // 
            // numA
            // 
            this.numA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numA.AutoSize = true;
            this.numA.Location = new System.Drawing.Point(29, 11);
            this.numA.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.numA.Name = "numA";
            this.numA.Size = new System.Drawing.Size(32, 23);
            this.numA.TabIndex = 1;
            this.numA.Text = "a =";
            this.numA.Click += new System.EventHandler(this.numA_Click);
            // 
            // numB
            // 
            this.numB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numB.AutoSize = true;
            this.numB.Location = new System.Drawing.Point(28, 57);
            this.numB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.numB.Name = "numB";
            this.numB.Size = new System.Drawing.Size(32, 23);
            this.numB.TabIndex = 3;
            this.numB.Text = "b =";
            this.numB.Click += new System.EventHandler(this.numB_Click);
            // 
            // BoxB
            // 
            this.BoxB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.BoxB.Location = new System.Drawing.Point(68, 54);
            this.BoxB.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BoxB.Name = "BoxB";
            this.BoxB.Size = new System.Drawing.Size(138, 29);
            this.BoxB.TabIndex = 2;
            this.BoxB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // numC
            // 
            this.numC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numC.AutoSize = true;
            this.numC.Location = new System.Drawing.Point(29, 103);
            this.numC.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.numC.Name = "numC";
            this.numC.Size = new System.Drawing.Size(31, 23);
            this.numC.TabIndex = 5;
            this.numC.Text = "c =";
            this.numC.Click += new System.EventHandler(this.numC_Click);
            // 
            // BoxC
            // 
            this.BoxC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.BoxC.Location = new System.Drawing.Point(68, 100);
            this.BoxC.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BoxC.Name = "BoxC";
            this.BoxC.Size = new System.Drawing.Size(138, 29);
            this.BoxC.TabIndex = 4;
            this.BoxC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // calcQuad
            // 
            this.calcQuad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.calcQuad.BackColor = System.Drawing.Color.PowderBlue;
            this.calcQuad.Location = new System.Drawing.Point(68, 148);
            this.calcQuad.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.calcQuad.Name = "calcQuad";
            this.calcQuad.Size = new System.Drawing.Size(140, 41);
            this.calcQuad.TabIndex = 6;
            this.calcQuad.Text = "Обрахувати";
            this.calcQuad.UseVisualStyleBackColor = false;
            this.calcQuad.Click += new System.EventHandler(this.calcQuad_Click);
            // 
            // labX1
            // 
            this.labX1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labX1.AutoSize = true;
            this.labX1.Location = new System.Drawing.Point(20, 202);
            this.labX1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labX1.Name = "labX1";
            this.labX1.Size = new System.Drawing.Size(40, 23);
            this.labX1.TabIndex = 8;
            this.labX1.Text = "x1 =";
            this.labX1.Click += new System.EventHandler(this.labX1_Click);
            // 
            // ResX1
            // 
            this.ResX1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ResX1.Location = new System.Drawing.Point(68, 199);
            this.ResX1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ResX1.Name = "ResX1";
            this.ResX1.ReadOnly = true;
            this.ResX1.Size = new System.Drawing.Size(138, 29);
            this.ResX1.TabIndex = 7;
            this.ResX1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labX2
            // 
            this.labX2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labX2.AutoSize = true;
            this.labX2.Location = new System.Drawing.Point(20, 250);
            this.labX2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labX2.Name = "labX2";
            this.labX2.Size = new System.Drawing.Size(40, 23);
            this.labX2.TabIndex = 10;
            this.labX2.Text = "x2 =";
            this.labX2.Click += new System.EventHandler(this.labX2_Click);
            // 
            // ResX2
            // 
            this.ResX2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ResX2.Location = new System.Drawing.Point(68, 247);
            this.ResX2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ResX2.Name = "ResX2";
            this.ResX2.ReadOnly = true;
            this.ResX2.Size = new System.Drawing.Size(138, 29);
            this.ResX2.TabIndex = 9;
            this.ResX2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // linkLabel1
            // 
            this.linkLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(64, 281);
            this.linkLabel1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(160, 23);
            this.linkLabel1.TabIndex = 11;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Стецюк Б.Ю., гр. КІ-3";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // labX
            // 
            this.labX.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labX.AutoSize = true;
            this.labX.Location = new System.Drawing.Point(28, 202);
            this.labX.Name = "labX";
            this.labX.Size = new System.Drawing.Size(31, 23);
            this.labX.TabIndex = 12;
            this.labX.Text = "x =";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AntiqueWhite;
            this.ClientSize = new System.Drawing.Size(234, 311);
            this.Controls.Add(this.labX);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.labX2);
            this.Controls.Add(this.ResX2);
            this.Controls.Add(this.labX1);
            this.Controls.Add(this.ResX1);
            this.Controls.Add(this.calcQuad);
            this.Controls.Add(this.numC);
            this.Controls.Add(this.BoxC);
            this.Controls.Add(this.numB);
            this.Controls.Add(this.BoxB);
            this.Controls.Add(this.numA);
            this.Controls.Add(this.BoxA);
            this.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximumSize = new System.Drawing.Size(500, 500);
            this.MinimumSize = new System.Drawing.Size(250, 350);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Лабораторна робота №2";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox BoxA;
        private System.Windows.Forms.Label numA;
        private System.Windows.Forms.Label numB;
        private System.Windows.Forms.TextBox BoxB;
        private System.Windows.Forms.Label numC;
        private System.Windows.Forms.TextBox BoxC;
        private System.Windows.Forms.Button calcQuad;
        private System.Windows.Forms.Label labX1;
        private System.Windows.Forms.TextBox ResX1;
        private System.Windows.Forms.Label labX2;
        private System.Windows.Forms.TextBox ResX2;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label labX;
    }
}

