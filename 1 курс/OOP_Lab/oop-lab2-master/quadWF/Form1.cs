﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace quadWF
{
    public partial class Form1 : Form
    {
        static void MesErr()
        {
            MessageBox.Show("Будь ласка, перевірте введені дані.", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
   System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            ResX1.Visible = false;
            ResX2.Visible = false;
            labX.Visible = false;
            labX1.Visible = false;
            labX2.Visible = false;
        }

        private void labX2_Click(object sender, EventArgs e)
        {

        }

        private void labX1_Click(object sender, EventArgs e)
        {

        }

        private void numC_Click(object sender, EventArgs e)
        {

        }

        private void numB_Click(object sender, EventArgs e)
        {

        }

        private void numA_Click(object sender, EventArgs e)
        {

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MessageBox.Show("Стецюк Богдан Юрійович, група КІ-3 || Програма для обчислення квадратного рівняння");
        }

        private void BoxA_TextChanged(object sender, EventArgs e)
        {

        }

        private void calcQuad_Click(object sender, EventArgs e)
        {
            double a, b, c, x1, x2, D;
            bool isOk1 = double.TryParse(BoxA.Text, out a);
            bool isOk2 = double.TryParse(BoxB.Text, out b);
            bool isOk3 = double.TryParse(BoxC.Text, out c);
            if (!isOk1 || !isOk2 || !isOk3)
                MesErr();
            else
            {
                D = b * b - 4 * a * c;
                if(D > 0)
                {
                    x1 = (-b + Math.Sqrt(D)) / (2 * a);
                    x2 = (-b - Math.Sqrt(D)) / (2 * a);
                    labX.Visible = false;
                    labX1.Visible = true;
                    labX2.Visible = true;
                    ResX1.Visible = true;
                    ResX2.Visible = true;
                    ResX1.Text = x1.ToString("F2");
                    ResX2.Text = x2.ToString("F2");
                }
                else if(D == 0)
                {
                    x1 = -b / (2 * a);
                    labX.Visible = true;
                    labX1.Visible = false;
                    labX2.Visible = false;
                    ResX1.Visible = true;
                    ResX2.Visible = false;
                    ResX1.Text = x1.ToString("F2");
                }
                else
                {
                    ResX1.Visible = false;
                    ResX2.Visible = false;
                    labX.Visible = false;
                    labX1.Visible = false;
                    labX2.Visible = false;
                    MessageBox.Show("Рівняння не має коренів.","Результат", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
    }
}
