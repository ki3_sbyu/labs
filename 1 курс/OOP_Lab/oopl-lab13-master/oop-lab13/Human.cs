﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop_lab13
{
    internal class Human
    {
        string name;

        internal Event Event
        {
            get => default(Event);
            set
            {
            }
        }

        public void SetName(string name)
        {
            this.name = name;
        }
        public string GetName()
        {
            return name;
        }
    }
}
